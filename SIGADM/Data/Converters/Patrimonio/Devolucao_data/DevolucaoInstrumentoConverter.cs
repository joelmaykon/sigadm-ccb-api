using System.Collections.Generic;
using System.Linq;
using SIGADM.Data.Converter;
using SIGADM.Data.VO.Patrimonio.Devolucao_data;
using SIGADM.Model.Patrimonio.Devolucao_data;

namespace SIGADM.Data.Converters.Patrimonio.Devolucao_data {
    public class DevolucaoInstrumentoConverter : IParser<DevolucaoInstrumentoVO, DevolucaoInstrumento>, IParser<DevolucaoInstrumento, DevolucaoInstrumentoVO> {
        public DevolucaoInstrumento Parse (DevolucaoInstrumentoVO origin) {
            if (origin == null) return new DevolucaoInstrumento ();
            return new DevolucaoInstrumento {
                Id = origin.Id,
                    IdInstrumento = origin.IdInstrumento,
                    IdEmprestimo = origin.IdEmprestimo,                   
                    Tombamento = origin.Tombamento,
                    Status = origin.Status, 
                    Observacao = origin.Observacao,           
                    DevolucaoDate = origin.DevolucaoDate,        
                    LaunchDate = origin.LaunchDate

            };
        }

        public DevolucaoInstrumentoVO Parse (DevolucaoInstrumento origin) {
            if (origin == null) return new DevolucaoInstrumentoVO ();
            return new DevolucaoInstrumentoVO {
                Id = origin.Id,
                    IdInstrumento = origin.IdInstrumento,
                    IdEmprestimo = origin.IdEmprestimo,
                    Tombamento = origin.Tombamento,
                    Status = origin.Status,
                    Observacao = origin.Observacao,   
                    DevolucaoDate = origin.DevolucaoDate,                   
                    LaunchDate = origin.LaunchDate
            };
        }

        public List<DevolucaoInstrumento> ParseList (List<DevolucaoInstrumentoVO> origin) {
            if (origin == null) return new List<DevolucaoInstrumento> ();
            return origin.Select (item => Parse (item)).ToList ();
        }

        public List<DevolucaoInstrumentoVO> ParseList (List<DevolucaoInstrumento> origin) {
            if (origin == null) return new List<DevolucaoInstrumentoVO> ();
            return origin.Select (item => Parse (item)).ToList ();
        }
    }
}