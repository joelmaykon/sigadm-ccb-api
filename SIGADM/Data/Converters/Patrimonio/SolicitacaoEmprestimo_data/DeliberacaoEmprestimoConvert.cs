using System.Collections.Generic;
using System.Linq;
using SIGADM.Data.Converter;
using SIGADM.Data.VO.Patrimonio.SolicitacaoEmprestimo_data;
using SIGADM.Model.Patrimonio.SolicitacaoEmprestimo_data;

namespace SIGADM.Data.Converters.Patrimonio.SolicitacaoEmprestimo_data
{
    public class DeliberacaoEmprestimoConverter : IParser<DeliberacaoEmprestimoVO, DeliberacaoEmprestimo>, IParser<DeliberacaoEmprestimo, DeliberacaoEmprestimoVO>
{
    public DeliberacaoEmprestimo Parse(DeliberacaoEmprestimoVO origin)
    {
        if (origin == null) return new DeliberacaoEmprestimo();
        return new DeliberacaoEmprestimo
        {
            Id = origin.Id,   
            Tipo = origin.Tipo,        
            DataCadastro = origin.DataCadastro,           
            LaunchDate = origin.LaunchDate

        };
    }

    public DeliberacaoEmprestimoVO Parse(DeliberacaoEmprestimo origin)
    {
        if (origin == null) return new DeliberacaoEmprestimoVO();
        return new DeliberacaoEmprestimoVO
        {
            Id = origin.Id, 
                        Tipo = origin.Tipo,          
            DataCadastro = origin.DataCadastro,
            LaunchDate = origin.LaunchDate
        };
    }

    public List<DeliberacaoEmprestimo> ParseList(List<DeliberacaoEmprestimoVO> origin)
    {
        if (origin == null) return new List<DeliberacaoEmprestimo>();
        return origin.Select(item => Parse(item)).ToList();
    }

    public List<DeliberacaoEmprestimoVO> ParseList(List<DeliberacaoEmprestimo> origin)
    {
        if (origin == null) return new List<DeliberacaoEmprestimoVO>();
        return origin.Select(item => Parse(item)).ToList();
    }
}
}