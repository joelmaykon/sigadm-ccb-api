using System.Collections.Generic;
using System.Linq;
using SIGADM.Data.Converter;
using SIGADM.Data.VO.Patrimonio.SolicitacaoEmprestimo_data;
using SIGADM.Model.Patrimonio.SolicitacaoEmprestimo_data;

namespace SIGADM.Data.Converters.Patrimonio.SolicitacaoEmprestimo_data
{
    public class SolicitacaoEmprestimoConverter : IParser<SolicitacaoEmprestimoVO, SolicitacaoEmprestimo>, IParser<SolicitacaoEmprestimo, SolicitacaoEmprestimoVO>
{
    public SolicitacaoEmprestimo Parse(SolicitacaoEmprestimoVO origin)
    {
        if (origin == null) return new SolicitacaoEmprestimo();
        return new SolicitacaoEmprestimo
        {
            Id = origin.Id,
            Nome = origin.Nome,
            Irmao = origin.Irmao,
            PrimeiroInstrumento = origin.PrimeiroInstrumento,
            SegundoInstrumento = origin.SegundoInstrumento,
            PrimeiraCategoria = origin.PrimeiraCategoria,
            SegundaCategoria = origin.SegundaCategoria,
            ComumCongregacao = origin.ComumCongregacao,
            DataCadastro = origin.DataCadastro,
            Status = origin.Status,
            LaunchDate = origin.LaunchDate

        };
    }

    public SolicitacaoEmprestimoVO Parse(SolicitacaoEmprestimo origin)
    {
        if (origin == null) return new SolicitacaoEmprestimoVO();
        return new SolicitacaoEmprestimoVO
        {
            Id = origin.Id,
            Nome = origin.Nome,
            Irmao = origin.Irmao,
            PrimeiroInstrumento = origin.PrimeiroInstrumento,
            SegundoInstrumento = origin.SegundoInstrumento,
            PrimeiraCategoria = origin.PrimeiraCategoria,
            SegundaCategoria = origin.SegundaCategoria,
            ComumCongregacao = origin.ComumCongregacao,
            DataCadastro = origin.DataCadastro,
            Status = origin.Status,
            LaunchDate = origin.LaunchDate
        };
    }

    public List<SolicitacaoEmprestimo> ParseList(List<SolicitacaoEmprestimoVO> origin)
    {
        if (origin == null) return new List<SolicitacaoEmprestimo>();
        return origin.Select(item => Parse(item)).ToList();
    }

    public List<SolicitacaoEmprestimoVO> ParseList(List<SolicitacaoEmprestimo> origin)
    {
        if (origin == null) return new List<SolicitacaoEmprestimoVO>();
        return origin.Select(item => Parse(item)).ToList();
    }
}
}