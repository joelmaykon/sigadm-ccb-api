using System.Collections.Generic;
using System.Linq;
using SIGADM.Data.Converter;
using SIGADM.Data.VO.Patrimonio.Empresa_data;
using SIGADM.Model.Patrimonio.Empresa_data;

namespace SIGADM.Data.Converters.Patrimonio.Empresa_data
{
    public class EmpresaConverter : IParser<EmpresaVO, Empresa>, IParser<Empresa, EmpresaVO>
    {
        public Empresa Parse(EmpresaVO origin)
        {
            if (origin == null) return new Empresa();
            return new Empresa
            {
                Id = origin.Id,
                Nome = origin.Nome,
                LaunchDate = origin.LaunchDate
                
            };
        }

        public EmpresaVO Parse(Empresa origin)
        {
             if (origin == null) return new EmpresaVO();
            return new EmpresaVO
            {
                Id = origin.Id,
                Nome = origin.Nome,
                LaunchDate = origin.LaunchDate
            };
        }

        public List<Empresa> ParseList(List<EmpresaVO> origin)
        {
            if (origin == null) return new List<Empresa>();
            return origin.Select(item => Parse(item)).ToList();
        }

        public List<EmpresaVO> ParseList(List<Empresa> origin)
        {
            if (origin == null) return new List<EmpresaVO>();
            return origin.Select(item => Parse(item)).ToList();
        }
    }
}