using System.Collections.Generic;
using System.Linq;
using SIGADM.Data.Converter;
using SIGADM.Data.VO.Patrimonio.Instrumento_data;
using SIGADM.Model.Patrimonio.Instrumento_data;

namespace SIGADM.Data.Converters.Patrimonio.Instrumento_data
{
    public class CaracteristicaInstrumentoConverter : IParser<CaracteristicaInstrumentoVO, CaracteristicaInstrumento>, IParser<CaracteristicaInstrumento, CaracteristicaInstrumentoVO>
    {
        public CaracteristicaInstrumento Parse(CaracteristicaInstrumentoVO origin)
        {
            if (origin == null) return new CaracteristicaInstrumento();
            return new CaracteristicaInstrumento
            {
                Id = origin.Id,
                Caracteristica = origin.Caracteristica,
                Naipe = origin.Naipe,
                LaunchDate = origin.LaunchDate
                
            };
        }

        public CaracteristicaInstrumentoVO Parse(CaracteristicaInstrumento origin)
        {
             if (origin == null) return new CaracteristicaInstrumentoVO();
            return new CaracteristicaInstrumentoVO
            {
                Id = origin.Id,
                Caracteristica = origin.Caracteristica,
                Naipe = origin.Naipe,
                LaunchDate = origin.LaunchDate
            };
        }

        public List<CaracteristicaInstrumento> ParseList(List<CaracteristicaInstrumentoVO> origin)
        {
            if (origin == null) return new List<CaracteristicaInstrumento>();
            return origin.Select(item => Parse(item)).ToList();
        }

        public List<CaracteristicaInstrumentoVO> ParseList(List<CaracteristicaInstrumento> origin)
        {
            if (origin == null) return new List<CaracteristicaInstrumentoVO>();
            return origin.Select(item => Parse(item)).ToList();
        }
    }
}