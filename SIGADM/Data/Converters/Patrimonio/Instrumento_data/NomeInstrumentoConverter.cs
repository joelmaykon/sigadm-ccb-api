using System.Collections.Generic;
using System.Linq;
using SIGADM.Data.Converter;
using SIGADM.Data.VO.Patrimonio.Instrumento_data;
using SIGADM.Model.Patrimonio.Instrumento_data;

namespace SIGADM.Data.Converters.Patrimonio.Instrumento_data
{
    public class NomeInstrumentoConverter : IParser<NomeInstrumentoVO, NomeInstrumento>, IParser<NomeInstrumento, NomeInstrumentoVO>
    {
        public NomeInstrumento Parse(NomeInstrumentoVO origin)
        {
            if (origin == null) return new NomeInstrumento();
            return new NomeInstrumento
            {
                Id = origin.Id,
                Nome = origin.Nome,
                Naipe = origin.Naipe,
                LaunchDate = origin.LaunchDate
                
            };
        }

        public NomeInstrumentoVO Parse(NomeInstrumento origin)
        {
             if (origin == null) return new NomeInstrumentoVO();
            return new NomeInstrumentoVO
            {
                Id = origin.Id,
                Nome = origin.Nome,
                Naipe = origin.Naipe,
                LaunchDate = origin.LaunchDate
            };
        }

        public List<NomeInstrumento> ParseList(List<NomeInstrumentoVO> origin)
        {
            if (origin == null) return new List<NomeInstrumento>();
            return origin.Select(item => Parse(item)).ToList();
        }

        public List<NomeInstrumentoVO> ParseList(List<NomeInstrumento> origin)
        {
            if (origin == null) return new List<NomeInstrumentoVO>();
            return origin.Select(item => Parse(item)).ToList();
        }
    }
}