using System.Collections.Generic;
using System.Linq;
using SIGADM.Data.Converter;
using SIGADM.Data.VO.Patrimonio.Instrumento_data;
using SIGADM.Model.Patrimonio.Instrumento_data;
using SIGADM.Model.Patrimonio.Datas;

namespace SIGADM.Data.Converters.Patrimonio.Instrumento_data
{
    public class AnoConverter : IParser<AnoVO, Ano>, IParser<Ano, AnoVO>
    {
        public Ano Parse(AnoVO origin)
        {
            if (origin == null) return new Ano();
            return new Ano
            {
                Id = origin.Id,
                NumeroAno = origin.NumeroAno,
                LaunchDate = origin.LaunchDate
                
            };
        }

        public AnoVO Parse(Ano origin)
        {
             if (origin == null) return new AnoVO();
            return new AnoVO
            {
                Id = origin.Id,
                NumeroAno = origin.NumeroAno,
                LaunchDate = origin.LaunchDate
            };
        }

        public List<Ano> ParseList(List<AnoVO> origin)
        {
            if (origin == null) return new List<Ano>();
            return origin.Select(item => Parse(item)).ToList();
        }

        public List<AnoVO> ParseList(List<Ano> origin)
        {
            if (origin == null) return new List<AnoVO>();
            return origin.Select(item => Parse(item)).ToList();
        }
    }
}