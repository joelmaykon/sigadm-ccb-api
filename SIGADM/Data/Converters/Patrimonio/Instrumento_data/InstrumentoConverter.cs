using System.Collections.Generic;
using System.Linq;
using SIGADM.Data.Converter;
using SIGADM.Data.VO.Patrimonio.Instrumento_data;
using SIGADM.Model.Patrimonio.Instrumento_data;

namespace SIGADM.Data.Converters.Patrimonio.Instrumento_data {
    public class InstrumentoConverter : IParser<InstrumentoVO, Instrumento>, IParser<Instrumento, InstrumentoVO> {
        public Instrumento Parse (InstrumentoVO origin) {
            if (origin == null) return new Instrumento ();
            return new Instrumento {
                Id = origin.Id,
                    Nome = origin.Nome,
                    Status = origin.Status,
                    Aquisicao = origin.Aquisicao,
                    Ano = origin.Ano,
                    Tombamento = origin.Tombamento,
                    Componentes = origin.Componentes,
                    Caracteristica = origin.Caracteristica,
                    Marca = origin.Marca,
                    Descricao = origin.Descricao,
                    LaunchDate = origin.LaunchDate

            };
        }

        public InstrumentoVO Parse (Instrumento origin) {
            if (origin == null) return new InstrumentoVO ();
            return new InstrumentoVO {
                Id = origin.Id,
                    Nome = origin.Nome,
                    Status = origin.Status,
                    Aquisicao = origin.Aquisicao,
                    Ano = origin.Ano,
                    Tombamento = origin.Tombamento,
                    Componentes = origin.Componentes,
                    Caracteristica = origin.Caracteristica,
                    Marca = origin.Marca,
                    Descricao = origin.Descricao,
                    LaunchDate = origin.LaunchDate
            };
        }

        public List<Instrumento> ParseList (List<InstrumentoVO> origin) {
            if (origin == null) return new List<Instrumento> ();
            return origin.Select (item => Parse (item)).ToList ();
        }

        public List<InstrumentoVO> ParseList (List<Instrumento> origin) {
            if (origin == null) return new List<InstrumentoVO> ();
            return origin.Select (item => Parse (item)).ToList ();
        }
    }
}