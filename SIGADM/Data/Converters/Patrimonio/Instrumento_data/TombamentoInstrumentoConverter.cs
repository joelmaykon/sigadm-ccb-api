using System.Collections.Generic;
using System.Linq;
using SIGADM.Data.Converter;
using SIGADM.Data.VO.Patrimonio.Instrumento_data;
using SIGADM.Model.Patrimonio.Instrumento_data;

namespace SIGADM.Data.Converters.Patrimonio.Instrumento_data
{
    public class TombamentoInstrumentoConverter : IParser<TombamentoInstrumentoVO, TombamentoInstrumento>, IParser<TombamentoInstrumento, TombamentoInstrumentoVO>
    {
        public TombamentoInstrumento Parse(TombamentoInstrumentoVO origin)
        {
            if (origin == null) return new TombamentoInstrumento();
            return new TombamentoInstrumento
            {
                Id = origin.Id,
                Numero = origin.Numero,
                Status = origin.Status,
                LaunchDate = origin.LaunchDate
                
            };
        }

        public TombamentoInstrumentoVO Parse(TombamentoInstrumento origin)
        {
             if (origin == null) return new TombamentoInstrumentoVO();
            return new TombamentoInstrumentoVO
            {
                Id = origin.Id,
                Numero = origin.Numero,
                Status = origin.Status,
                LaunchDate = origin.LaunchDate
            };
        }

        public List<TombamentoInstrumento> ParseList(List<TombamentoInstrumentoVO> origin)
        {
            if (origin == null) return new List<TombamentoInstrumento>();
            return origin.Select(item => Parse(item)).ToList();
        }

        public List<TombamentoInstrumentoVO> ParseList(List<TombamentoInstrumento> origin)
        {
            if (origin == null) return new List<TombamentoInstrumentoVO>();
            return origin.Select(item => Parse(item)).ToList();
        }
    }
}