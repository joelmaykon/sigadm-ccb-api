using System.Collections.Generic;
using System.Linq;
using SIGADM.Data.Converter;
using SIGADM.Data.VO.Patrimonio.Instrumento_data;
using SIGADM.Model.Patrimonio.Instrumento_data;

namespace SIGADM.Data.Converters.Patrimonio.Instrumento_data
{
    public class ComponenteInstrumentoConverter : IParser<ComponenteInstrumentoVO, ComponenteInstrumento>, IParser<ComponenteInstrumento, ComponenteInstrumentoVO>
    {
        public ComponenteInstrumento Parse(ComponenteInstrumentoVO origin)
        {
            if (origin == null) return new ComponenteInstrumento();
            return new ComponenteInstrumento
            {
                Id = origin.Id,
                Componente = origin.Componente,
                LaunchDate = origin.LaunchDate
                
            };
        }

        public ComponenteInstrumentoVO Parse(ComponenteInstrumento origin)
        {
             if (origin == null) return new ComponenteInstrumentoVO();
            return new ComponenteInstrumentoVO
            {
                Id = origin.Id,
                Componente = origin.Componente,
                LaunchDate = origin.LaunchDate
            };
        }

        public List<ComponenteInstrumento> ParseList(List<ComponenteInstrumentoVO> origin)
        {
            if (origin == null) return new List<ComponenteInstrumento>();
            return origin.Select(item => Parse(item)).ToList();
        }

        public List<ComponenteInstrumentoVO> ParseList(List<ComponenteInstrumento> origin)
        {
            if (origin == null) return new List<ComponenteInstrumentoVO>();
            return origin.Select(item => Parse(item)).ToList();
        }
    }
}