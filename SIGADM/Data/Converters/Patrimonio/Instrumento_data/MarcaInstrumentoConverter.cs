using System.Collections.Generic;
using System.Linq;
using SIGADM.Data.Converter;
using SIGADM.Data.VO.Patrimonio.Instrumento_data;
using SIGADM.Model.Patrimonio.Instrumento_data;

namespace SIGADM.Data.Converters.Patrimonio.Instrumento_data
{
    public class MarcaInstrumentoConverter : IParser<MarcaInstrumentoVO, MarcaInstrumento>, IParser<MarcaInstrumento, MarcaInstrumentoVO>
    {
        public MarcaInstrumento Parse(MarcaInstrumentoVO origin)
        {
            if (origin == null) return new MarcaInstrumento();
            return new MarcaInstrumento
            {
                Id = origin.Id,
                Marca = origin.Marca,
                LaunchDate = origin.LaunchDate
                
            };
        }

        public MarcaInstrumentoVO Parse(MarcaInstrumento origin)
        {
             if (origin == null) return new MarcaInstrumentoVO();
            return new MarcaInstrumentoVO
            {
                Id = origin.Id,
                Marca = origin.Marca,
                LaunchDate = origin.LaunchDate
            };
        }

        public List<MarcaInstrumento> ParseList(List<MarcaInstrumentoVO> origin)
        {
            if (origin == null) return new List<MarcaInstrumento>();
            return origin.Select(item => Parse(item)).ToList();
        }

        public List<MarcaInstrumentoVO> ParseList(List<MarcaInstrumento> origin)
        {
            if (origin == null) return new List<MarcaInstrumentoVO>();
            return origin.Select(item => Parse(item)).ToList();
        }
    }
}