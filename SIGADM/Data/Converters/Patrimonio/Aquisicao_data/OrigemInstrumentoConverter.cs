using System.Collections.Generic;
using System.Linq;
using SIGADM.Data.Converter;
using SIGADM.Data.VO.Patrimonio.Aquisicao_data;
using SIGADM.Model.Patrimonio.Aquisicao_data;

namespace SIGADM.Data.Converters.Patrimonio.Aquisicao_data {
    public class OrigemInstrumentoConverter : IParser<OrigemInstrumentoVO, OrigemInstrumento>, IParser<OrigemInstrumento, OrigemInstrumentoVO> {
        public OrigemInstrumento Parse (OrigemInstrumentoVO origin) {
            if (origin == null) return new OrigemInstrumento ();
            return new OrigemInstrumento {
                Id = origin.Id,
                    Doador = origin.Doador,
                    Estado = origin.Estado,
                    Cidade = origin.Cidade,
                    LaunchDate = origin.LaunchDate

            };
        }

        public OrigemInstrumentoVO Parse (OrigemInstrumento origin) {
            if (origin == null) return new OrigemInstrumentoVO ();
            return new OrigemInstrumentoVO {
                Id = origin.Id,
                    Doador = origin.Doador,
                    Estado = origin.Estado,
                    Cidade = origin.Cidade,
                    LaunchDate = origin.LaunchDate
            };
        }

        public List<OrigemInstrumento> ParseList (List<OrigemInstrumentoVO> origin) {
            if (origin == null) return new List<OrigemInstrumento> ();
            return origin.Select (item => Parse (item)).ToList ();
        }

        public List<OrigemInstrumentoVO> ParseList (List<OrigemInstrumento> origin) {
            if (origin == null) return new List<OrigemInstrumentoVO> ();
            return origin.Select (item => Parse (item)).ToList ();
        }
    }
}