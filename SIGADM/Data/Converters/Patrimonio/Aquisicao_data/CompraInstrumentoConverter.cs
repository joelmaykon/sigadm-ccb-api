using System.Collections.Generic;
using System.Linq;
using SIGADM.Data.Converter;
using SIGADM.Data.VO.Patrimonio.Aquisicao_data;
using SIGADM.Model.Patrimonio.Aquisicao_data;

namespace SIGADM.Data.Converters.Patrimonio.Aquisicao_data {
    public class CompraInstrumentoConverter : IParser<CompraInstrumentoVO, CompraInstrumento>, IParser<CompraInstrumento, CompraInstrumentoVO> {
        public CompraInstrumento Parse (CompraInstrumentoVO origin) {
            if (origin == null) return new CompraInstrumento ();
            return new CompraInstrumento {
                    Id = origin.Id,
                    Tombamento = origin.Tombamento,
                    Valor = origin.Valor,
                    NotaFiscal = origin.Notafiscal,
                    Empresa = origin.Empresa,
                    DataCompra = origin.DataCompra,
                    LaunchDate = origin.LaunchDate

            };
        }

        public CompraInstrumentoVO Parse (CompraInstrumento origin) {
            if (origin == null) return new CompraInstrumentoVO ();
            return new CompraInstrumentoVO {
                Id = origin.Id,
                    Tombamento = origin.Tombamento,
                    Valor = origin.Valor,                   
                    Empresa = origin.Empresa,  
                    Notafiscal = origin.NotaFiscal,
                    DataCompra = origin.DataCompra,
                    LaunchDate = origin.LaunchDate
            };
        }

        public List<CompraInstrumento> ParseList (List<CompraInstrumentoVO> origin) {
            if (origin == null) return new List<CompraInstrumento> ();
            return origin.Select (item => Parse (item)).ToList ();
        }

        public List<CompraInstrumentoVO> ParseList (List<CompraInstrumento> origin) {
            if (origin == null) return new List<CompraInstrumentoVO> ();
            return origin.Select (item => Parse (item)).ToList ();
        }
    }
}