using System.Collections.Generic;
using System.Linq;
using SIGADM.Data.Converter;
using SIGADM.Data.VO.Patrimonio.Aquisicao_data;
using SIGADM.Model.Patrimonio.Aquisicao_data;

namespace SIGADM.Data.Converters.Patrimonio.Aquisicao_data {
    public class DoacaoInstrumentoConverter : IParser<DoacaoInstrumentoVO, DoacaoInstrumento>, IParser<DoacaoInstrumento, DoacaoInstrumentoVO> {
        public DoacaoInstrumento Parse (DoacaoInstrumentoVO origin) {
            if (origin == null) return new DoacaoInstrumento ();
            return new DoacaoInstrumento {
                Id = origin.Id,
                    Tombamento = origin.Tombamento,
                    Origem = origin.Origem,
                    Observacao = origin.Observacao,
                    DataDoacao = origin.DataDoacao,
                    LaunchDate = origin.LaunchDate

            };
        }

        public DoacaoInstrumentoVO Parse (DoacaoInstrumento origin) {
            if (origin == null) return new DoacaoInstrumentoVO ();
            return new DoacaoInstrumentoVO {
                Id = origin.Id,
                    Tombamento = origin.Tombamento,
                    Origem = origin.Origem,
                    Observacao = origin.Observacao,
                    DataDoacao = origin.DataDoacao,
                    LaunchDate = origin.LaunchDate
            };
        }

        public List<DoacaoInstrumento> ParseList (List<DoacaoInstrumentoVO> origin) {
            if (origin == null) return new List<DoacaoInstrumento> ();
            return origin.Select (item => Parse (item)).ToList ();
        }

        public List<DoacaoInstrumentoVO> ParseList (List<DoacaoInstrumento> origin) {
            if (origin == null) return new List<DoacaoInstrumentoVO> ();
            return origin.Select (item => Parse (item)).ToList ();
        }
    }
}