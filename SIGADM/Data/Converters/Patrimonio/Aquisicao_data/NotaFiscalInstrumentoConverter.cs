using System.Collections.Generic;
using System.Linq;
using SIGADM.Data.Converter;
using SIGADM.Data.VO.Patrimonio.Aquisicao_data;
using SIGADM.Model.Patrimonio.Aquisicao_data;

namespace SIGADM.Data.Converters.Patrimonio.Aquisicao_data {
    public class NotaFiscalInstrumentoConverter : IParser<NotaFiscalInstrumentoVO, NotaFiscalInstrumento>, IParser<NotaFiscalInstrumento, NotaFiscalInstrumentoVO> {
        public NotaFiscalInstrumento Parse (NotaFiscalInstrumentoVO origin) {
            if (origin == null) return new NotaFiscalInstrumento ();
            return new NotaFiscalInstrumento {
                Id = origin.Id,
                    Tombamento = origin.Tombamento,
                    Caminho = origin.Caminho,
                    LaunchDate = origin.LaunchDate

            };
        }

        public NotaFiscalInstrumentoVO Parse (NotaFiscalInstrumento origin) {
            if (origin == null) return new NotaFiscalInstrumentoVO ();
            return new NotaFiscalInstrumentoVO {
                Id = origin.Id,
                    Tombamento = origin.Tombamento,
                    Caminho = origin.Caminho,
                    LaunchDate = origin.LaunchDate
            };
        }

        public List<NotaFiscalInstrumento> ParseList (List<NotaFiscalInstrumentoVO> origin) {
            if (origin == null) return new List<NotaFiscalInstrumento> ();
            return origin.Select (item => Parse (item)).ToList ();
        }

        public List<NotaFiscalInstrumentoVO> ParseList (List<NotaFiscalInstrumento> origin) {
            if (origin == null) return new List<NotaFiscalInstrumentoVO> ();
            return origin.Select (item => Parse (item)).ToList ();
        }
    }
}