using System.Collections.Generic;
using System.Linq;
using SIGADM.Data.Converter;
using SIGADM.Data.VO.Patrimonio.Emprestimo_data;
using SIGADM.Model.Patrimonio.Emprestimo_data;

namespace SIGADM.Data.Converters.Patrimonio.Emprestimo_data {
    public class EmprestimoInstrumentoConverter : IParser<EmprestimoInstrumentoVO, EmprestimoInstrumento>, IParser<EmprestimoInstrumento, EmprestimoInstrumentoVO> {
        public EmprestimoInstrumento Parse (EmprestimoInstrumentoVO origin) {
            if (origin == null) return new EmprestimoInstrumento ();
            return new EmprestimoInstrumento {
                Id = origin.Id,
                    IdInstrumento = origin.IdInstrumento,
                    Nome = origin.Nome,
                    Tombamento = origin.Tombamento,
                    Status = origin.Status,
                    Congregacao = origin.Congregacao,
                    Encarregado = origin.Encarregado,
                    EncarregadoRegional = origin.EncarregadoRegional,
                    DevolucaoDate = origin.DevolucaoDate,
                    EmprestimoDate = origin.EmprestimoDate,
                    LaunchDate = origin.LaunchDate

            };
        }

        public EmprestimoInstrumentoVO Parse (EmprestimoInstrumento origin) {
            if (origin == null) return new EmprestimoInstrumentoVO ();
            return new EmprestimoInstrumentoVO {
                Id = origin.Id,
                    IdInstrumento = origin.IdInstrumento,
                    Nome = origin.Nome,
                    Congregacao = origin.Congregacao,
                    Tombamento = origin.Tombamento,
                    Status = origin.Status,
                    Encarregado = origin.Encarregado,
                    EncarregadoRegional = origin.EncarregadoRegional,

                    DevolucaoDate = origin.DevolucaoDate,
                    EmprestimoDate = origin.EmprestimoDate,
                    LaunchDate = origin.LaunchDate
            };
        }

        public List<EmprestimoInstrumento> ParseList (List<EmprestimoInstrumentoVO> origin) {
            if (origin == null) return new List<EmprestimoInstrumento> ();
            return origin.Select (item => Parse (item)).ToList ();
        }

        public List<EmprestimoInstrumentoVO> ParseList (List<EmprestimoInstrumento> origin) {
            if (origin == null) return new List<EmprestimoInstrumentoVO> ();
            return origin.Select (item => Parse (item)).ToList ();
        }
    }
}