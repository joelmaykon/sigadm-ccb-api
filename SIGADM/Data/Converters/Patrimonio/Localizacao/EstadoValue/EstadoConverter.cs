using System.Collections.Generic;
using System.Linq;
using SIGADM.Data.Converter;
using SIGADM.Data.VO.Patrimonio.Localizacao.Estado;
using SIGADM.Model.Patrimonio.Localizacao.Estado;

namespace SIGADM.Data.Converters.Patrimonio.Localizacao.EstadoValue
{
    public class EstadoConverter : IParser<EstadoVO, Estado>, IParser<Estado, EstadoVO>
    {
        public Estado Parse(EstadoVO origin)
        {
            if (origin == null) return new Estado();
            return new Estado
            {
                Id = origin.Id,
                Nome = origin.Nome,
                LaunchDate = origin.LaunchDate
                
            };
        }

        public EstadoVO Parse(Estado origin)
        {
             if (origin == null) return new EstadoVO();
            return new EstadoVO
            {
                Id = origin.Id,
                Nome = origin.Nome,
                LaunchDate = origin.LaunchDate
            };
        }

        public List<Estado> ParseList(List<EstadoVO> origin)
        {
            if (origin == null) return new List<Estado>();
            return origin.Select(item => Parse(item)).ToList();
        }

        public List<EstadoVO> ParseList(List<Estado> origin)
        {
            if (origin == null) return new List<EstadoVO>();
            return origin.Select(item => Parse(item)).ToList();
        }
    }
}