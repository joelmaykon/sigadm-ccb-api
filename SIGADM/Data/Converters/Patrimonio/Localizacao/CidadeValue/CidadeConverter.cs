using System.Collections.Generic;
using System.Linq;
using SIGADM.Data.Converter;
using SIGADM.Data.VO.Patrimonio.Localizacao.Cidade;
using SIGADM.Model.Patrimonio.Localizacao.Cidade;

namespace SIGADM.Data.Converters.Patrimonio.Localizacao.CidadeValue
{
    public class CidadeConverter : IParser<CidadeVO, Cidade>, IParser<Cidade, CidadeVO>
    {
        public Cidade Parse(CidadeVO origin)
        {
            if (origin == null) return new Cidade();
            return new Cidade
            {
                Id = origin.Id,
                Nome = origin.Nome,
                LaunchDate = origin.LaunchDate
                
            };
        }

        public CidadeVO Parse(Cidade origin)
        {
             if (origin == null) return new CidadeVO();
            return new CidadeVO
            {
                Id = origin.Id,
                Nome = origin.Nome,
                LaunchDate = origin.LaunchDate
            };
        }

        public List<Cidade> ParseList(List<CidadeVO> origin)
        {
            if (origin == null) return new List<Cidade>();
            return origin.Select(item => Parse(item)).ToList();
        }

        public List<CidadeVO> ParseList(List<Cidade> origin)
        {
            if (origin == null) return new List<CidadeVO>();
            return origin.Select(item => Parse(item)).ToList();
        }
    }
}