using System.Collections.Generic;
using System.Linq;
using SIGADM.Data.Converter;
using SIGADM.Data.VO.Patrimonio.Setorizacao.Irmaos;
using SIGADM.Model.Patrimonio.Setorizacao.Irmaos;

namespace SIGADM.Data.Converters.Patrimonio.Setorizacao.Irmaos {
    public class UsuarioIgrejaConverter : IParser<UsuarioIgrejaVO, UsuarioIgreja>, IParser<UsuarioIgreja, UsuarioIgrejaVO> {
        public UsuarioIgreja Parse (UsuarioIgrejaVO origin) {
            if (origin == null) return new UsuarioIgreja ();
            return new UsuarioIgreja {
                Id = origin.Id,
                    IdIgreja = origin.IdIgreja,
                    IdUsuario = origin.IdUsuario,
                    LaunchDate = origin.LaunchDate

            };
        }

        public UsuarioIgrejaVO Parse (UsuarioIgreja origin) {
            if (origin == null) return new UsuarioIgrejaVO ();
            return new UsuarioIgrejaVO {
                Id = origin.Id,
                    IdIgreja = origin.IdIgreja,
                    IdUsuario = origin.IdUsuario,
                    LaunchDate = origin.LaunchDate
            };
        }

        public List<UsuarioIgreja> ParseList (List<UsuarioIgrejaVO> origin) {
            if (origin == null) return new List<UsuarioIgreja> ();
            return origin.Select (item => Parse (item)).ToList ();
        }

        public List<UsuarioIgrejaVO> ParseList (List<UsuarioIgreja> origin) {
            if (origin == null) return new List<UsuarioIgrejaVO> ();
            return origin.Select (item => Parse (item)).ToList ();
        }
    }
}