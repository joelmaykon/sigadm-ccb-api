using System.Collections.Generic;
using System.Linq;
using SIGADM.Data.Converter;
using SIGADM.Data.VO.Patrimonio.Setorizacao.Irmaos;
using SIGADM.Model.Patrimonio.Setorizacao.Irmaos;

namespace SIGADM.Data.Converters.Patrimonio.Setorizacao.Irmaos {
    public class UsuarioConverter : IParser<UsuarioVO, Usuario>, IParser<Usuario, UsuarioVO> {
        public Usuario Parse (UsuarioVO origin) {
            if (origin == null) return new Usuario ();
            return new Usuario {
                Id = origin.Id,
                    CPF = origin.CPF,
                    Nascimento = origin.Nascimento,
                    Nome = origin.Nome,
                    Cep = origin.Cep,
                    Cidade = origin.Cidade,
                    Bairro = origin.Bairro,
                    Rua = origin.Rua,
                    Numero = origin.Numero,
                    Complemento = origin.Complemento,
                    DDD = origin.DDD,
                    Telefone = origin.Telefone,
                    Cargo = origin.Cargo,
                    LaunchDate = origin.LaunchDate,

            };
        }

        public UsuarioVO Parse (Usuario origin) {
            if (origin == null) return new UsuarioVO ();
            return new UsuarioVO {
                Id = origin.Id,
                    CPF = origin.CPF,
                    Nascimento = origin.Nascimento,
                    Nome = origin.Nome,
                    Cep = origin.Cep,
                    Cidade = origin.Cidade,
                    Bairro = origin.Bairro,
                    Rua = origin.Rua,
                    Numero = origin.Numero,
                    Complemento = origin.Complemento,
                    DDD = origin.DDD,
                    Telefone = origin.Telefone,
                    Cargo = origin.Cargo,
                    LaunchDate = origin.LaunchDate

            };
        }

        public List<Usuario> ParseList (List<UsuarioVO> origin) {
            if (origin == null) return new List<Usuario> ();
            return origin.Select (item => Parse (item)).ToList ();
        }

        public List<UsuarioVO> ParseList (List<Usuario> origin) {
            if (origin == null) return new List<UsuarioVO> ();
            return origin.Select (item => Parse (item)).ToList ();
        }
    }
}