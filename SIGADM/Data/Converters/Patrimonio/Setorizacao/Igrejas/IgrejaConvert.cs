using System.Collections.Generic;
using System.Linq;
using SIGADM.Data.Converter;
using SIGADM.Data.VO.Patrimonio.Setorizacao.Igrejas;
using SIGADM.Model.Patrimonio.Setorizacao.Igrejas;

namespace SIGADM.Data.Converters.Patrimonio.Setorizacao.Igrejas
{
    public class IgrejaConverter : IParser<IgrejaVO, Igreja>, IParser<Igreja, IgrejaVO>
    {
        public Igreja Parse(IgrejaVO origin)
        {
            if (origin == null) return new Igreja();
            return new Igreja
            {
                Id = origin.Id,
                Codigo = origin.Codigo,
                Cidade = origin.Cidade,
                Bairro = origin.Bairro,
                CEP = origin.CEP,
                Endereco = origin.Endereco,
                Numero = origin.Numero,
                NomeRelatorio = origin.NomeRelatorio,
                EncRegional = origin.EncRegional,
                EncLocal = origin.EncLocal,
                LaunchDate = origin.LaunchDate
                
            };
        }

        public IgrejaVO Parse(Igreja origin)
        {
             if (origin == null) return new IgrejaVO();
            return new IgrejaVO
            {
                Id = origin.Id,
                Codigo = origin.Codigo,
                Cidade = origin.Cidade,
                Bairro = origin.Bairro,
                CEP = origin.CEP,
                Endereco = origin.Endereco,
                Numero = origin.Numero,
                NomeRelatorio = origin.NomeRelatorio,
                EncRegional = origin.EncRegional,
                EncLocal = origin.EncLocal,
                LaunchDate = origin.LaunchDate
            };
        }

        public List<Igreja> ParseList(List<IgrejaVO> origin)
        {
            if (origin == null) return new List<Igreja>();
            return origin.Select(item => Parse(item)).ToList();
        }

        public List<IgrejaVO> ParseList(List<Igreja> origin)
        {
            if (origin == null) return new List<IgrejaVO>();
            return origin.Select(item => Parse(item)).ToList();
        }
    }
}