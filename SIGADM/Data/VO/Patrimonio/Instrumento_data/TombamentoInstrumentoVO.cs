using System;
using System.Collections.Generic;
using Tapioca.HATEOAS;

namespace SIGADM.Data.VO.Patrimonio.Instrumento_data
{
    public class TombamentoInstrumentoVO : ISupportsHyperMedia
    {
        public long? Id { get; set; }
        public string Numero { get; set; }
        public string Status {get; set;}
        public DateTime LaunchDate { get; set; }

        public List<HyperMediaLink> Links { get; set; } = new List<HyperMediaLink>();
    }
}