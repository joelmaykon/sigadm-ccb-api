using System;
using System.Collections.Generic;
using Tapioca.HATEOAS;

namespace SIGADM.Data.VO.Patrimonio.Instrumento_data {
    public class CaracteristicaInstrumentoVO : ISupportsHyperMedia {
        public long? Id { get; set; }
        public string Caracteristica { get; set; }
        public string Naipe { get; set; }

        public DateTime LaunchDate { get; set; }

        public List<HyperMediaLink> Links { get; set; } = new List<HyperMediaLink> ();
    }
}