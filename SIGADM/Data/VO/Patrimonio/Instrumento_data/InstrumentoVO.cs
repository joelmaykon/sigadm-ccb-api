using System;
using System.Collections.Generic;
using Tapioca.HATEOAS;

namespace SIGADM.Data.VO.Patrimonio.Instrumento_data {
    public class InstrumentoVO : ISupportsHyperMedia {
        public long? Id { get; set; }
        public string Nome { get; set; }
        public string Status { get; set; }
        public string Aquisicao {get; set;}
        public string Ano { get; set; }
        public string Tombamento { get; set; }
        public string Caracteristica { get; set; }
        public string Marca { get; set; }
        public string Descricao { get; set; }
        public string Componentes { get; set; }
        public DateTime LaunchDate { get; set; }

        public List<HyperMediaLink> Links { get; set; } = new List<HyperMediaLink> ();
    }
}