using System;
using System.Collections.Generic;
using Tapioca.HATEOAS;

namespace SIGADM.Data.VO.Patrimonio.Instrumento_data
{
    public class MarcaInstrumentoVO : ISupportsHyperMedia
    {
        public long? Id { get; set; }
        public string Marca { get; set; }
        public DateTime LaunchDate { get; set; }

        public List<HyperMediaLink> Links { get; set; } = new List<HyperMediaLink>();
    }
}