using System;
using System.Collections.Generic;
using Tapioca.HATEOAS;

namespace SIGADM.Data.VO.Patrimonio.Instrumento_data
{
    public class NomeInstrumentoVO : ISupportsHyperMedia
    {
        public long? Id { get; set; }
        public string Nome { get; set; }
        public string Naipe { get; set; }
        public DateTime LaunchDate { get; set; }

        public List<HyperMediaLink> Links { get; set; } = new List<HyperMediaLink>();
    }
}