using System;
using System.Collections.Generic;
using Tapioca.HATEOAS;

namespace SIGADM.Data.VO.Patrimonio.Aquisicao_data {
    public class NotaFiscalInstrumentoVO : ISupportsHyperMedia {
        public long? Id { get; set; }
        public string Tombamento { get; set; }
        public string Caminho {get; set;}

        public DateTime LaunchDate { get; set; }

        public List<HyperMediaLink> Links { get; set; } = new List<HyperMediaLink> ();
    }
}