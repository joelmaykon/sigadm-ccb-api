using System;
using System.Collections.Generic;
using Tapioca.HATEOAS;

namespace SIGADM.Data.VO.Patrimonio.Aquisicao_data {
    public class OrigemInstrumentoVO : ISupportsHyperMedia {
        public long? Id { get; set; }
        public string Doador { get; set; }
        public string Estado { get; set; }
        public string Cidade { get; set; }

        public DateTime LaunchDate { get; set; }

        public List<HyperMediaLink> Links { get; set; } = new List<HyperMediaLink> ();
    }
}