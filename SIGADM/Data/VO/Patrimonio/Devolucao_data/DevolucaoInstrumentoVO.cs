using System;
using System.Collections.Generic;
using Tapioca.HATEOAS;

namespace SIGADM.Data.VO.Patrimonio.Devolucao_data {
    public class DevolucaoInstrumentoVO : ISupportsHyperMedia {
        public long? Id { get; set; }
        public long? IdInstrumento { get; set; }
         public long? IdEmprestimo { get; set; }

        public string Tombamento { get; set; }

        public string Status { get; set; }
        public string Observacao { get; set; }

       
        public DateTime DevolucaoDate { get; set; }

        public DateTime LaunchDate { get; set; }

        public List<HyperMediaLink> Links { get; set; } = new List<HyperMediaLink> ();
    }
}