using System;
using System.Collections.Generic;
using Tapioca.HATEOAS;

namespace SIGADM.Data.VO.Patrimonio.Setorizacao.Igrejas {
    public class IgrejaVO : ISupportsHyperMedia
    {
        public long? Id { get; set; }
        public string Codigo { get; set; }
        public string Cidade { get; set; }
        public string Bairro { get; set; }
        public string Endereco { get; set; }
        public string Numero { get; set; }
        public string NomeRelatorio { get; set; }
        public string  CEP { get; set; }
        public string EncRegional { get; set; }
        public string EncLocal { get; set; }
        public DateTime LaunchDate { get; set; }

        public List<HyperMediaLink> Links { get; set; } = new List<HyperMediaLink>();
    }
}