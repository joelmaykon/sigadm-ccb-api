using System;
using System.Collections.Generic;
using SIGADM.Data.VO.Patrimonio.Setorizacao.Igrejas;
using SIGADM.Model.Patrimonio.Setorizacao.Igrejas;
using Tapioca.HATEOAS;

namespace SIGADM.Data.VO.Patrimonio.Setorizacao.Irmaos {
    public class UsuarioVO : ISupportsHyperMedia {
        public long? Id { get; set; }
        public string CPF { get; set; }
        public DateTime Nascimento { get; set; }
        public string Nome { get; set; }
        public string Cep { get; set; }
        public string Cidade { get; set; }
        public string Bairro {get; set;}
        public string Rua { get; set; }
        public string  Numero { get; set; }
        public string  Complemento { get; set; }
        public string DDD { get; set; }
        public string Telefone { get; set; }       
        public string Cargo { get; set; }
        public DateTime LaunchDate { get; set; }
        public List<HyperMediaLink> Links { get; set; } = new List<HyperMediaLink> ();
    }
}