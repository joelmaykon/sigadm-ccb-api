using System;
using System.Collections.Generic;
using Tapioca.HATEOAS;

namespace SIGADM.Data.VO.Patrimonio.Setorizacao.Irmaos {
    public class UsuarioIgrejaVO : ISupportsHyperMedia {
        public long? Id { get; set; }
        public int IdIgreja { get; set; }
        public int IdUsuario { get; set; }
       
        public DateTime LaunchDate { get; set; }

        public List<HyperMediaLink> Links { get; set; } = new List<HyperMediaLink> ();
    }
}