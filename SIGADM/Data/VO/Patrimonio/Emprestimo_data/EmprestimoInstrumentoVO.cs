using System;
using System.Collections.Generic;
using Tapioca.HATEOAS;

namespace SIGADM.Data.VO.Patrimonio.Emprestimo_data {
    public class EmprestimoInstrumentoVO : ISupportsHyperMedia {
        public long? Id { get; set; }
        public long? IdInstrumento { get; set; }

        public string Nome { get; set; }
        public string Congregacao { get; set; }
        public string Tombamento { get; set; }

        public string Status { get; set; }

        public string Encarregado { get; set; }
        public string EncarregadoRegional { get; set; }
        
        public DateTime DevolucaoDate { get; set; }
        public DateTime EmprestimoDate { get; set; }

        public DateTime LaunchDate { get; set; }

        public List<HyperMediaLink> Links { get; set; } = new List<HyperMediaLink> ();
    }
}