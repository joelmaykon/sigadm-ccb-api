using System;
using System.Collections.Generic;
using Tapioca.HATEOAS;

namespace SIGADM.Data.VO.Patrimonio.SolicitacaoEmprestimo_data
{
    public class SolicitacaoEmprestimoVO : ISupportsHyperMedia
    {
        public long? Id { get; set; }
        public string Nome { get; set; }
        public int Irmao {get; set;}
        public string ComumCongregacao { get; set; }
        public string PrimeiroInstrumento { get; set; }
        public string PrimeiraCategoria { get; set; }
        public string SegundoInstrumento { get; set; }
        public string SegundaCategoria { get; set; }
        public DateTime DataCadastro { get; set; }
        public string Status { get; set; }
        public DateTime LaunchDate { get; set; }

        public List<HyperMediaLink> Links { get; set; } = new List<HyperMediaLink>();
    }
}