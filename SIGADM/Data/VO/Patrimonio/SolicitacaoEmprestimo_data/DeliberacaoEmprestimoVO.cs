using System;
using System.Collections.Generic;
using Tapioca.HATEOAS;

namespace SIGADM.Data.VO.Patrimonio.SolicitacaoEmprestimo_data
{
    public class DeliberacaoEmprestimoVO : ISupportsHyperMedia
    {
        public long? Id { get; set; }
        public string Tipo { get; set; }        
        public DateTime DataCadastro { get; set; }
        public DateTime LaunchDate { get; set; }
        public List<HyperMediaLink> Links { get; set; } = new List<HyperMediaLink>();
    }
}