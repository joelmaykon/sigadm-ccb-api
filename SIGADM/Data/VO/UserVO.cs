﻿using System.Runtime.Serialization;

namespace SIGADM.Model
{
    public class UserVO
    {
        public string Login { get; set; }
        public string AccessKey { get; set; }
    }
}
