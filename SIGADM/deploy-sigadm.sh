#!/bin/bash

dotnet publish -c Release
sudo docker build  -t sigadm ./bin/Release/netcoreapp2.1/publish

sudo heroku login
sudo heroku container:login
sudo docker tag sigadm  registry.heroku.com/sigadm-patrimonio-services/web
sudo docker push registry.heroku.com/sigadm-patrimonio-services/web
sudo heroku container:release web --app sigadm-patrimonio-services
heroku  open -a sigadm-patrimonio-services