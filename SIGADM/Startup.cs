﻿using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Rewrite;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.Net.Http.Headers;
using SIGADM.Business;
using SIGADM.Business.Implementattions;
using SIGADM.Business.Implementattions.Patrimonio;
using SIGADM.Business.Implementattions.Patrimonio.Devolucao_data;
using SIGADM.Business.Implementattions.Patrimonio.Empresa_data;
using SIGADM.Business.Implementattions.Patrimonio.Emprestimo_data;
using SIGADM.Business.Implementattions.Patrimonio.Instrumento_data;
using SIGADM.Business.Implementattions.Patrimonio.Localizacao.Cidade;
using SIGADM.Business.Implementattions.Patrimonio.Localizacao.Estado;
using SIGADM.Business.Implementattions.Patrimonio.Setorizacao.Igrejas;
using SIGADM.Business.Implementattions.Patrimonio.Setorizacao.Irmaos;
using SIGADM.Business.Interfaces.Aquisicao_data;
using SIGADM.Business.Interfaces.Devolucao_data;
using SIGADM.Business.Interfaces.Empresa_data;
using SIGADM.Business.Interfaces.Emprestimo_data;
using SIGADM.Business.Interfaces.Instrumento_data;
using SIGADM.Business.Interfaces.Localizacao.Cidade;
using SIGADM.Business.Interfaces.Localizacao.Estado;
using SIGADM.Business.Interfaces.Setorizacao.Irmaos;
using SIGADM.Business.Interfaces.Setorizacao.Igrejas;
using SIGADM.HyperMedia.Patrimonio.Aquisicao_data;
using SIGADM.HyperMedia.Patrimonio.Instrumento_data;
using SIGADM.HyperMedia.Patrimonio.Patrimonio.Devolucao_data;
using SIGADM.HyperMedia.Patrimonio.Patrimonio.Emprestimo_data;
using SIGADM.HyperMedia.Patrimonio.Patrimonio.Instrumento_data;
using SIGADM.HyperMedia.Patrimonio.Setorizacao.Igrejas;
using SIGADM.HyperMedia.Patrimonio.Setorizacao.Irmaos;
using SIGADM.Model.Context;
using SIGADM.Repository.Generic;
using SIGADM.Repository.Interfaces;
using SIGADM.Repository.Interfaces.Repository_Instrumento;
using SIGADM.Repository.Interfaces.Repository_Irmaos;
using SIGADM.Repository.Repository_Localizacao.Repository_Cidade;
using SIGADM.Repository.Repository_Setorizacao.Repository_Igrejas;
using SIGADM.Security.Configuration;
using Swashbuckle.AspNetCore.Swagger;
using Tapioca.HATEOAS;
using SIGADM.HyperMedia.Patrimonio.SolicitacaoEmprestimo;
using SIGADM.Business.Interfaces.SolicitacaoEmprestimo_data;
using SIGADM.Business.Implementattions.Patrimonio.SolicitacaoEmprestimo_data;
using SIGADM.Repository.Repository_SolicitacaoEmprestimo;
using SIGADM.Repository.Implementattions;
using SIGADM.Repository.Implementattions.Setorizacao.Igrejas;
using SIGADM.Repository.Implementattions.SolicitacaoEmprestimo_data;
using SIGADM.Repository;

namespace SIGADM {
    public class Startup {
        private readonly ILogger _logger;
        public IConfiguration _configuration { get; }
        public IHostingEnvironment _environment { get; }

        public Startup (IConfiguration configuration, IHostingEnvironment environment, ILogger<Startup> logger) {
            _configuration = configuration;
            _environment = environment;
            _logger = logger;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices (IServiceCollection services) {
            //Serviço de CORS para manipular o header deo browser
            services.AddCors ();
            

            //Connection to database
            var connectionString = _configuration["MySqlConnection:MySqlConnectionString"];
            services.AddDbContext<MySQLContext> (options => options.UseMySql (connectionString));

            //Adding Migrations Support
            ExecuteMigrations (connectionString);

            var signingConfigurations = new SigningConfigurations ();
            services.AddSingleton (signingConfigurations);

            var tokenConfigurations = new TokenConfiguration ();

            new ConfigureFromConfigurationOptions<TokenConfiguration> (
                    _configuration.GetSection ("TokenConfigurations")
                )
                .Configure (tokenConfigurations);

            services.AddSingleton (tokenConfigurations);

            services.AddAuthentication (authOptions => {
                authOptions.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                authOptions.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer (bearerOptions => {
                var paramsValidation = bearerOptions.TokenValidationParameters;
                paramsValidation.IssuerSigningKey = signingConfigurations.Key;
                paramsValidation.ValidAudience = tokenConfigurations.Audience;
                paramsValidation.ValidIssuer = tokenConfigurations.Issuer;

                // Validates the signing of a received token
                paramsValidation.ValidateIssuerSigningKey = true;

                // Checks if a received token is still valid
                paramsValidation.ValidateLifetime = true;

                // Tolerance time for the expiration of a token (used in case
                // of time synchronization problems between different
                // computers involved in the communication process)
                paramsValidation.ClockSkew = TimeSpan.Zero;
            });

            // Enables the use of the token as a means of
            // authorizing access to this project's resources
            services.AddAuthorization (auth => {
                auth.AddPolicy ("Bearer", new AuthorizationPolicyBuilder ()
                    .AddAuthenticationSchemes (JwtBearerDefaults.AuthenticationScheme‌​)
                    .RequireAuthenticatedUser ().Build ());
            });

            //Content negociation - Support to XML and JSON
            services.AddMvc (options => {
                    options.RespectBrowserAcceptHeader = true;
                    options.FormatterMappings.SetMediaTypeMappingForFormat ("xml", MediaTypeHeaderValue.Parse ("text/xml"));
                    options.FormatterMappings.SetMediaTypeMappingForFormat ("json", MediaTypeHeaderValue.Parse ("application/json"));

                })
                .AddXmlSerializerFormatters ()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
                

            //HATEOAS filter definitions
            var filterOptions = new HyperMediaFilterOptions ();
            filterOptions.ObjectContentResponseEnricherList.Add (new InstrumentoEnricher ());
            filterOptions.ObjectContentResponseEnricherList.Add (new NomeInstrumentoEnricher ());
            filterOptions.ObjectContentResponseEnricherList.Add (new TombamentoInstrumentoEnricher ());
            filterOptions.ObjectContentResponseEnricherList.Add (new CaracteristicaInstrumentoEnricher ());
            filterOptions.ObjectContentResponseEnricherList.Add (new MarcaInstrumentoEnricher ());
            filterOptions.ObjectContentResponseEnricherList.Add (new ComponenteInstrumentoEnricher ());
            filterOptions.ObjectContentResponseEnricherList.Add (new DoacaoInstrumentoEnricher ());
            filterOptions.ObjectContentResponseEnricherList.Add (new CompraInstrumentoEnricher ());
            filterOptions.ObjectContentResponseEnricherList.Add (new EmprestimoInstrumentoEnricher ());
            filterOptions.ObjectContentResponseEnricherList.Add (new DevolucaoInstrumentoEnricher ());
            filterOptions.ObjectContentResponseEnricherList.Add (new OrigemInstrumentoEnricher ());
            filterOptions.ObjectContentResponseEnricherList.Add (new NotaFiscalInstrumentoEnricher ());
            filterOptions.ObjectContentResponseEnricherList.Add (new UsuarioEnricher ());
            filterOptions.ObjectContentResponseEnricherList.Add (new UsuarioIgrejaEnricher ());
            filterOptions.ObjectContentResponseEnricherList.Add (new IgrejaEnricher ());
            filterOptions.ObjectContentResponseEnricherList.Add (new SolicitacaoEmprestimoEnricher ());
            filterOptions.ObjectContentResponseEnricherList.Add (new DeliberacaoEmprestimoEnricher ());


            //Service inject
            services.AddSingleton (filterOptions);

            //Versioning
            services.AddApiVersioning (option => option.ReportApiVersions = true);

            //Add Swagger Service
            services.AddSwaggerGen (c => {
                c.SwaggerDoc ("v1",
                    new Info {
                        Title = "API SIGADM  Restful ASP.NET Core 2.0",
                            Version = "v1"
                    });

            });
            //Dependency Injection
            services.AddScoped<I_instrumentoBusiness, InstrumentoBusinessImpl> ();
            services.AddScoped<INomeInstrumentoBusiness, NomeInstrumentoBusinessImpl> ();
            services.AddScoped<IAnoBusiness, AnoBusinessImpl> ();
            services.AddScoped<ITombamentoInstrumentoBusiness, TombamentoInstrumentoBusinessImpl> ();
            services.AddScoped<ICaracteristicaInstrumentoBusiness, CaracteristicaInstrumentoBusinessImpl> ();
            services.AddScoped<IMarcaInstrumentoBusiness, MarcaInstrumentoBusinessImpl> ();
            services.AddScoped<IComponenteInstrumentoBusiness, ComponenteInstrumentoBusinessImpl> ();
            services.AddScoped<IDoacaoInstrumentoBusiness, DoacaoInstrumentoBusinessImpl> ();
            services.AddScoped<ICompraInstrumentoBusiness, CompraInstrumentoBusinessImpl> ();
            services.AddScoped<ILoginBusiness, LoginBusinessImpl> ();
            services.AddScoped<IEmprestimoInstrumentoBusiness, EmprestimoInstrumentoBusinessImpl> ();
            services.AddScoped<IDevolucaoInstrumentoBusiness, DevolucaoInstrumentoBusinessImpl> ();
            services.AddScoped<IEmpresaBusiness, EmpresaBusinessImpl> ();
            services.AddScoped<ICidadeBusiness, CidadeBusinessImpl> ();
            services.AddScoped<IEstadoBusiness, EstadoBusinessImpl> ();
            services.AddScoped<IOrigemInstrumentoBusiness, OrigemInstrumentoBusinessImpl> ();
            services.AddScoped<INotaFiscalInstrumentoBusiness, NotaFiscalInstrumentoBusinessImpl> ();
            services.AddScoped<IUsuarioBusiness, UsuarioBusinessImpl> ();       
            services.AddScoped<IUsuarioIgrejaBusiness, UsuarioIgrejaBusinessImpl> ();       
            services.AddScoped<IIgrejaBusiness, IgrejaBusinessImpl> ();
            services.AddScoped<ITermoDoacaoBusiness,TermoDoacaoBusinessImpl>();
            services.AddScoped<ISolicitacaoEmprestimoBusiness,SolicitacaoEmprestimoBusinessImpl>();
            services.AddScoped<IDeliberacaoEmprestimoBusiness,DeliberacaoEmprestimoBusinessImpl>();

            //Repositórios de dados
            services.AddScoped<IUserRepository, UserRepositoryImpl> ();
            services.AddScoped<IRepositoryDoacao, DoacaoRepositoryImpl> ();
            services.AddScoped<IRepositoryCompra, CompraRepositoryImpl> ();
            services.AddScoped<IRepositoryNomeInstrumento, NomeInstrumentoRepositoryImpl> ();
            services.AddScoped<IRepositoryAnoInstrumento, AnoInstrumentoRepositoryImpl> ();
            services.AddScoped<IRepositoryMarcaInstrumento, MarcaInstrumentoRepositoryImpl> ();
            services.AddScoped<IRepositoryCaracteristicaInstrumento, CaracteristicaRepositoryImpl> ();
            services.AddScoped<IRepositoryTombamento, TombamentoRepositoryImpl> ();
            services.AddScoped<IRepositoryEmprestimo, EmprestimoRepositoryImpl> ();
            services.AddScoped<IRepositoryDevolucao, DevolucaoRepositoryImpl> ();
            services.AddScoped<IRepositoryCidade, CidadeRepositoryImpl> ();
            services.AddScoped<IRepositoryOrigem, OrigemRepositoryImpl> ();
            services.AddScoped<IRepositoryNotaFiscal, NotaFiscalRepositoryImpl> ();
            services.AddScoped<IRepositoryUsuario, UsuarioRepositoryImpl> ();
            services.AddScoped<IRepositoryUsuarioIgreja, UsuarioIgrejaRepositoryImpl> ();
            services.AddScoped<IRepositoryIgreja, IgrejaRepositoryImpl> ();
            services.AddScoped<IRepositorySolicitacaoEmprestimo, SolicitacaoEmprestimoRepositoryImpl> ();
            services.AddScoped<IRepositoryInstrumento,InstrumentoRepositoryImpl>();
            services.AddScoped<IRepositoryDeliberacaoEmprestimo,DeliberacaoEmprestimoRepositoryImpl>();
            //Dependency Injection of GenericRepository
            services.AddScoped (typeof (IRepository<>), typeof (GenericRepository<>));
        }

        private void ExecuteMigrations (string connectionString) {
            if (_environment.IsDevelopment ()) {
                try {
                    var evolveConnection = new MySql.Data.MySqlClient.MySqlConnection (connectionString);

                    var evolve = new Evolve.Evolve (evolveConnection, msg => _logger.LogInformation (msg)) {
                        Locations = new List<string> { "db/migrations" },
                        IsEraseDisabled = true,
                    };

                    evolve.Migrate ();

                } catch (Exception ex) {
                    _logger.LogCritical ("Database migration failed.", ex);
                    throw;
                }
            }
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure (IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory) {

            loggerFactory.AddConsole (_configuration.GetSection ("Logging"));
            loggerFactory.AddDebug ();
            //Inciando o serviço de CORS
            
            app.UseCors (
                options => options
                .AllowAnyOrigin ()
                .AllowAnyMethod ()
                .AllowAnyHeader ()
            );
            //Enable Swagger
            app.UseSwagger ();

            app.UseSwaggerUI (c => {
                c.SwaggerEndpoint ("/swagger/v1/swagger.json", "SIGADM API V1");
            });

            //Starting our API in Swagger page
            var option = new RewriteOptions ();
            option.AddRedirect ("^$", "swagger");
            app.UseRewriter (option);
            app.UseStaticFiles (); // For the wwwroot folder

            app.UseStaticFiles (new StaticFileOptions {
                FileProvider = new PhysicalFileProvider (
                        Path.Combine (Directory.GetCurrentDirectory (), @"Resources")),
                    RequestPath = new PathString("/Resources")
            });
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }

            app.UseHttpsRedirection();
            //Adding map routing
            app.UseMvc (routes => {
                routes.MapRoute (
                    name: "DefaultApi",
                    template: "{controller=Values}/{id?}");
            });

        }
    }
}