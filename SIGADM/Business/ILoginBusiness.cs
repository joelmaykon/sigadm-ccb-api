﻿using SIGADM.Model;

namespace SIGADM.Business
{
    public interface ILoginBusiness
    {
         object FindByLogin(UserVO user);
    }
}
