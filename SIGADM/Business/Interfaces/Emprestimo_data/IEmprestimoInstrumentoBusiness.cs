using System.Collections.Generic;
using SIGADM.Data.VO.Patrimonio.Emprestimo_data;

namespace SIGADM.Business.Interfaces.Emprestimo_data
{
    public interface IEmprestimoInstrumentoBusiness
    {
        EmprestimoInstrumentoVO Create(EmprestimoInstrumentoVO instrumento);
        EmprestimoInstrumentoVO FindById(long id);
        List<EmprestimoInstrumentoVO> FindAll();
        List<EmprestimoInstrumentoVO> FindEmprestimo(string tombamento);

        EmprestimoInstrumentoVO Update(EmprestimoInstrumentoVO instrumento);
        void Delete(long id); 
    }
}