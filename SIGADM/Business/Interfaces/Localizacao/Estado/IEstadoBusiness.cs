using System.Collections.Generic;
using SIGADM.Data.VO.Patrimonio.Localizacao.Estado;

namespace SIGADM.Business.Interfaces.Localizacao.Estado
{
    public interface IEstadoBusiness
    {
        EstadoVO Create(EstadoVO nome);
        EstadoVO FindById(long id);
        List<EstadoVO> FindAll();
        EstadoVO Update(EstadoVO nome);
        void Delete(long id); 
    }
}