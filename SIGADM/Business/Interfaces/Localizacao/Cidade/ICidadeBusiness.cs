using System.Collections.Generic;
using SIGADM.Data.VO.Patrimonio.Localizacao.Cidade;

namespace SIGADM.Business.Interfaces.Localizacao.Cidade
{
    public interface ICidadeBusiness
    {
        CidadeVO Create(CidadeVO nome);
        CidadeVO FindById(long id);
        List<CidadeVO> FindAll();
        CidadeVO Update(CidadeVO nome);
        void Delete(long id); 
    }
}