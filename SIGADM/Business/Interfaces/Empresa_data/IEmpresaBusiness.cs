using System.Collections.Generic;
using SIGADM.Data.VO.Patrimonio.Empresa_data;

namespace SIGADM.Business.Interfaces.Empresa_data
{
    public interface IEmpresaBusiness
    {
        EmpresaVO Create(EmpresaVO nome);
        EmpresaVO FindById(long id);
        List<EmpresaVO> FindAll();
        EmpresaVO Update(EmpresaVO nome);
        void Delete(long id); 
    }
}