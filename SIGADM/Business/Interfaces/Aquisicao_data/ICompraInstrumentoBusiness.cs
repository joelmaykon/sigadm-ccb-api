using System.Collections.Generic;
using SIGADM.Data.VO.Patrimonio.Aquisicao_data;
using SIGADM.Data.VO.Patrimonio.Instrumento_data;

namespace SIGADM.Business.Interfaces.Aquisicao_data
{
    public interface ICompraInstrumentoBusiness
    {
        CompraInstrumentoVO Create(CompraInstrumentoVO nome);
        CompraInstrumentoVO FindById(long id);
        List<CompraInstrumentoVO> FindAll();
        List<CompraInstrumentoVO> FindCompra(string tombamento);
        CompraInstrumentoVO Update(CompraInstrumentoVO nome);
        void Delete(long id); 
    }
}