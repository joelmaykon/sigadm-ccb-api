using System.Collections.Generic;
using SIGADM.Data.VO.Patrimonio.Aquisicao_data;

namespace SIGADM.Business.Interfaces.Aquisicao_data
{
    public interface INotaFiscalInstrumentoBusiness
    {
        NotaFiscalInstrumentoVO Create(NotaFiscalInstrumentoVO nota);
        NotaFiscalInstrumentoVO FindById(long id);
        List<NotaFiscalInstrumentoVO> FindAll();
        NotaFiscalInstrumentoVO Update(NotaFiscalInstrumentoVO origem);
        void Delete(long id); 
    }
}