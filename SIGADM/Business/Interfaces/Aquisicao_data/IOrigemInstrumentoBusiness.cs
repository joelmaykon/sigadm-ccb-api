using System.Collections.Generic;
using SIGADM.Data.VO.Patrimonio.Aquisicao_data;

namespace SIGADM.Business.Interfaces.Aquisicao_data
{
    public interface IOrigemInstrumentoBusiness
    {
        OrigemInstrumentoVO Create(OrigemInstrumentoVO origem);
        OrigemInstrumentoVO FindById(long id);
        List<OrigemInstrumentoVO> FindAll();
        OrigemInstrumentoVO Update(OrigemInstrumentoVO origem);
        void Delete(long id); 
    }
}