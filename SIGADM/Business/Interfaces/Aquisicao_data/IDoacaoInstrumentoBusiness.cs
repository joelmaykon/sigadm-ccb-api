using System.Collections.Generic;
using SIGADM.Data.VO.Patrimonio.Aquisicao_data;
using SIGADM.Data.VO.Patrimonio.Instrumento_data;

namespace SIGADM.Business.Interfaces.Aquisicao_data
{
    public interface IDoacaoInstrumentoBusiness
    {
        DoacaoInstrumentoVO Create(DoacaoInstrumentoVO doacao);
        DoacaoInstrumentoVO FindById(long id);
        List<DoacaoInstrumentoVO> FindAll();
        List<DoacaoInstrumentoVO> FindDoacao(string tombamento);
        DoacaoInstrumentoVO Update(DoacaoInstrumentoVO doacao);
        void Delete(long id); 
    }
}