using System.Collections.Generic;
using SIGADM.Data.VO.Patrimonio.Instrumento_data;

namespace SIGADM.Business.Interfaces.Instrumento_data
{
    public interface ITombamentoInstrumentoBusiness
    {
        TombamentoInstrumentoVO Create(TombamentoInstrumentoVO nome);
        TombamentoInstrumentoVO FindById(long id);
        TombamentoInstrumentoVO FindByTombamento(string tombamento);
        List<TombamentoInstrumentoVO> FindAll();
        TombamentoInstrumentoVO Update(TombamentoInstrumentoVO nome);
        TombamentoInstrumentoVO SelecionarTombamento(string tombamento);
        void Delete(long id); 
    }
}