using System.Collections.Generic;
using SIGADM.Data.VO.Patrimonio.Instrumento_data;

namespace SIGADM.Business.Interfaces.Instrumento_data
{
    public interface IAnoBusiness
    {
        AnoVO Create(AnoVO ano);
        AnoVO FindById(long id);
        List<AnoVO> FindAll();
        AnoVO Update(AnoVO ano);
        void Delete(long id);
        void DeleteAno(string ano);
         
    }
}