using System.Collections.Generic;
using SIGADM.Data.VO.Patrimonio.Instrumento_data;

namespace SIGADM.Business.Interfaces.Instrumento_data
{
    public interface I_instrumentoBusiness
    {
        InstrumentoVO Create(InstrumentoVO instrumento);
        InstrumentoVO FindById(long id);
        List<InstrumentoVO> FindAll();
        List<InstrumentoVO> BuscarInstrumentosNomes(string nome);
        InstrumentoVO Update(InstrumentoVO instrumento);
        void Delete(long id); 
    }
}