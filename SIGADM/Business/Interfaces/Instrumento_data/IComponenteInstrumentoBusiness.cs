using System.Collections.Generic;
using SIGADM.Data.VO.Patrimonio.Instrumento_data;

namespace SIGADM.Business.Interfaces.Instrumento_data
{
    public interface IComponenteInstrumentoBusiness
    {
        ComponenteInstrumentoVO Create(ComponenteInstrumentoVO componente);
        ComponenteInstrumentoVO FindById(long id);
        List<ComponenteInstrumentoVO> FindAll();
        ComponenteInstrumentoVO Update(ComponenteInstrumentoVO componente);
        void Delete(long id); 
    }
}