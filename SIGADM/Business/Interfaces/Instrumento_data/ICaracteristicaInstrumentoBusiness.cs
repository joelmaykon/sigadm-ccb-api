using System.Collections.Generic;
using SIGADM.Data.VO.Patrimonio.Instrumento_data;

namespace SIGADM.Business.Interfaces.Instrumento_data
{
    public interface ICaracteristicaInstrumentoBusiness
    {
        CaracteristicaInstrumentoVO Create(CaracteristicaInstrumentoVO nome);
        CaracteristicaInstrumentoVO FindById(long id);
        List<CaracteristicaInstrumentoVO> FindAll();
        List<CaracteristicaInstrumentoVO> FindCaracteristicaNaipe(string naipe);
        CaracteristicaInstrumentoVO Update(CaracteristicaInstrumentoVO nome);
        void Delete(long id); 
        void DeleteCaracteristica(string caracteristica);
    }
}