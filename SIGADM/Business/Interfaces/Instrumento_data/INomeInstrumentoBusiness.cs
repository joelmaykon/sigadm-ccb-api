using System.Collections.Generic;
using SIGADM.Data.VO.Patrimonio.Instrumento_data;

namespace SIGADM.Business.Interfaces.Instrumento_data
{
    public interface INomeInstrumentoBusiness
    {
        NomeInstrumentoVO Create(NomeInstrumentoVO nome);
        NomeInstrumentoVO FindById(long id);
        List<NomeInstrumentoVO> FindAll();
        NomeInstrumentoVO Update(NomeInstrumentoVO nome);
        void Delete(long id); 
        void DeleteNome(string nome);
    }
}