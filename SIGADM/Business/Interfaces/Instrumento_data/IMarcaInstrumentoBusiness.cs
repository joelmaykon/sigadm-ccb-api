using System.Collections.Generic;
using SIGADM.Data.VO.Patrimonio.Instrumento_data;

namespace SIGADM.Business.Interfaces.Instrumento_data
{
    public interface IMarcaInstrumentoBusiness
    {
        MarcaInstrumentoVO Create(MarcaInstrumentoVO nome);
        MarcaInstrumentoVO FindById(long id);
        List<MarcaInstrumentoVO> FindAll();
        MarcaInstrumentoVO Update(MarcaInstrumentoVO nome);
        void Delete(long id); 
        void DeleteMarca(string marca);
    }
}