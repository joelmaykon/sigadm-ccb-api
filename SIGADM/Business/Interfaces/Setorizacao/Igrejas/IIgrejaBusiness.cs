using System.Collections.Generic;
using SIGADM.Data.VO.Patrimonio.Setorizacao.Igrejas;

namespace SIGADM.Business.Interfaces.Setorizacao.Igrejas
{
    public interface IIgrejaBusiness
    {
        IgrejaVO Create(IgrejaVO igreja);
        IgrejaVO FindById(long id);
        List<IgrejaVO> FindAll();
        IgrejaVO Update(IgrejaVO igreja);
        void Delete(long id); 
    }
}