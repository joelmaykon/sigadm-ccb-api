using System.Collections.Generic;
using SIGADM.Data.VO.Patrimonio.Setorizacao.Irmaos;

namespace SIGADM.Business.Interfaces.Setorizacao.Irmaos
{
    public interface IUsuarioBusiness
    {
        UsuarioVO Create(UsuarioVO instrumento);
        List<UsuarioVO> FindAll();
        UsuarioVO BuscarIrmaoCPF(string cpf);
        UsuarioVO BuscarId(long id);
        UsuarioVO Update(UsuarioVO instrumento);
        void Delete(long id); 
    }
}