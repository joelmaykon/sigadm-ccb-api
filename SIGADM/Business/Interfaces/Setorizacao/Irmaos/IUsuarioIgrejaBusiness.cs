using System.Collections.Generic;
using SIGADM.Data.VO.Patrimonio.Setorizacao.Irmaos;

namespace SIGADM.Business.Interfaces.Setorizacao.Irmaos
{
    public interface IUsuarioIgrejaBusiness
    {
        UsuarioIgrejaVO Create(UsuarioIgrejaVO igreja);
        UsuarioIgrejaVO FindById(long id);
        UsuarioIgrejaVO ProcurarIdUsuario(int idUsuario);
        List<UsuarioIgrejaVO> FindAll();

        UsuarioIgrejaVO Update(UsuarioIgrejaVO igreja);
        void Delete(long id); 
    }
}