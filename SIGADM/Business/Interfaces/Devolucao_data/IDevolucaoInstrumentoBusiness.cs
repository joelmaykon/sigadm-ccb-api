using System.Collections.Generic;
using SIGADM.Data.VO.Patrimonio.Devolucao_data;

namespace SIGADM.Business.Interfaces.Devolucao_data
{
    public interface IDevolucaoInstrumentoBusiness
    {
        DevolucaoInstrumentoVO Create(DevolucaoInstrumentoVO devolucao);
        DevolucaoInstrumentoVO FindById(long id);
        List<DevolucaoInstrumentoVO> FindAll();
        List<DevolucaoInstrumentoVO> FindDevolucao(string tombamento);

        DevolucaoInstrumentoVO Update(DevolucaoInstrumentoVO instrumento);
        void Delete(long id); 
    }
}