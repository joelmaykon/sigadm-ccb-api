using System.Collections.Generic;
using SIGADM.Data.VO.Patrimonio.SolicitacaoEmprestimo_data;

namespace SIGADM.Business.Interfaces.SolicitacaoEmprestimo_data
{
    public interface ISolicitacaoEmprestimoBusiness
    {
        SolicitacaoEmprestimoVO Create(SolicitacaoEmprestimoVO solicitacaoEmprestimo);
        SolicitacaoEmprestimoVO FindById(long id);
        List<SolicitacaoEmprestimoVO> FindAll();
        SolicitacaoEmprestimoVO Update(SolicitacaoEmprestimoVO solicitacaoEmprestimo);
        void Delete(long id);
    }
}