using System.Collections.Generic;
using SIGADM.Data.VO.Patrimonio.SolicitacaoEmprestimo_data;

namespace SIGADM.Business.Interfaces.SolicitacaoEmprestimo_data
{
    public interface IDeliberacaoEmprestimoBusiness
    {
        DeliberacaoEmprestimoVO Create(DeliberacaoEmprestimoVO deliberacaoEmprestimo);
        DeliberacaoEmprestimoVO FindById(long id);
        List<DeliberacaoEmprestimoVO> FindAll();
        DeliberacaoEmprestimoVO Update(DeliberacaoEmprestimoVO deliberacaoEmprestimo);
        void Delete(long id);
    }
}