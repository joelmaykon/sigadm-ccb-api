using System.Collections.Generic;
using SIGADM.Business.Interfaces.SolicitacaoEmprestimo_data;
using SIGADM.Data.Converters.Patrimonio.SolicitacaoEmprestimo_data;
using SIGADM.Data.VO.Patrimonio.SolicitacaoEmprestimo_data;
using SIGADM.Model.Patrimonio.SolicitacaoEmprestimo_data;
using SIGADM.Repository.Generic;

namespace SIGADM.Business.Implementattions.Patrimonio.SolicitacaoEmprestimo_data
{
    public class DeliberacaoEmprestimoBusinessImpl : IDeliberacaoEmprestimoBusiness
    {
        private IRepository<DeliberacaoEmprestimo> _repository;
        private readonly DeliberacaoEmprestimoConverter _converter;
        public DeliberacaoEmprestimoBusinessImpl(IRepository<DeliberacaoEmprestimo> repository)
        {
            _repository = repository;
            _converter = new DeliberacaoEmprestimoConverter();
        }
        public DeliberacaoEmprestimoVO Create(DeliberacaoEmprestimoVO deliberacao)
        {
            var deliberacaoEntity = _converter.Parse(deliberacao);
            deliberacaoEntity = _repository.Create(deliberacaoEntity);
            return _converter.Parse(deliberacaoEntity);
        }

        public void Delete(long id)
        {
            _repository.Delete(id);
        }

        public List<DeliberacaoEmprestimoVO> FindAll()
        {
            return _converter.ParseList(_repository.FindAll());
        }

        public DeliberacaoEmprestimoVO FindById(long id)
        {
            return _converter.Parse(_repository.FindById(id));
        }

        public DeliberacaoEmprestimoVO Update(DeliberacaoEmprestimoVO deliberacao)
        {
            var deliberacaoEntity = _converter.Parse(deliberacao);
            deliberacaoEntity = _repository.Update(deliberacaoEntity);
            return _converter.Parse(deliberacaoEntity);
        }
    }
}