using System.Collections.Generic;
using SIGADM.Business.Interfaces.SolicitacaoEmprestimo_data;
using SIGADM.Data.Converters.Patrimonio.SolicitacaoEmprestimo_data;
using SIGADM.Data.VO.Patrimonio.SolicitacaoEmprestimo_data;
using SIGADM.Model.Patrimonio.SolicitacaoEmprestimo_data;
using SIGADM.Repository.Generic;

namespace SIGADM.Business.Implementattions.Patrimonio.SolicitacaoEmprestimo_data
{
    public class SolicitacaoEmprestimoBusinessImpl : ISolicitacaoEmprestimoBusiness
    {
        private IRepository<SolicitacaoEmprestimo> _repository;
        private readonly SolicitacaoEmprestimoConverter _converter;
        public SolicitacaoEmprestimoBusinessImpl(IRepository<SolicitacaoEmprestimo> repository)
        {
            _repository = repository;
            _converter = new SolicitacaoEmprestimoConverter();
        }
        public SolicitacaoEmprestimoVO Create(SolicitacaoEmprestimoVO solicitacao)
        {
            var solicitacaoEntity = _converter.Parse(solicitacao);
            solicitacaoEntity = _repository.Create(solicitacaoEntity);
            return _converter.Parse(solicitacaoEntity);
        }

        public void Delete(long id)
        {
            _repository.Delete(id);
        }

        public List<SolicitacaoEmprestimoVO> FindAll()
        {
            return _converter.ParseList(_repository.FindAll());
        }

        public SolicitacaoEmprestimoVO FindById(long id)
        {
            return _converter.Parse(_repository.FindById(id));
        }

        public SolicitacaoEmprestimoVO Update(SolicitacaoEmprestimoVO solicitacao)
        {
            var solicitacaoEntity = _converter.Parse(solicitacao);
            solicitacaoEntity = _repository.Update(solicitacaoEntity);
            return _converter.Parse(solicitacaoEntity);
        }
    }
}