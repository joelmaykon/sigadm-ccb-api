using System.Collections.Generic;
using SIGADM.Business.Interfaces.Setorizacao.Irmaos;
using SIGADM.Data.Converters.Patrimonio.Setorizacao.Irmaos;
using SIGADM.Data.VO.Patrimonio.Setorizacao.Irmaos;
using SIGADM.Model.Patrimonio.Setorizacao.Irmaos;
using SIGADM.Repository.Generic;
using SIGADM.Repository.Interfaces.Repository_Irmaos;

namespace SIGADM.Business.Implementattions.Patrimonio.Setorizacao.Irmaos {
    public class UsuarioBusinessImpl : IUsuarioBusiness {
        private IRepositoryUsuario _repository;
        private readonly UsuarioConverter _converter;
        public UsuarioBusinessImpl (IRepositoryUsuario repository) {
            _repository = repository;
            _converter = new UsuarioConverter ();
        }
        public UsuarioVO Create (UsuarioVO instrumento) {
            var instrumentoEntity = _converter.Parse (instrumento);
            instrumentoEntity = _repository.Create (instrumentoEntity);
            return _converter.Parse (instrumentoEntity);
        }

        public void Delete (long id) {
            _repository.Delete (id);
        }

        public List<UsuarioVO> FindAll () {
            return _converter.ParseList (_repository.FindAll ());
        }

      
        public UsuarioVO BuscarIrmaoCPF(string cpf){
            return _converter.Parse (_repository.BuscarIrmaoCPF(cpf));
        }
        public UsuarioVO BuscarId(long id){
            return _converter.Parse(_repository.BuscarId(id));
        }

        public UsuarioVO Update (UsuarioVO instrumento) {
            var instrumentoEntity = _converter.Parse (instrumento);
            instrumentoEntity = _repository.Update (instrumentoEntity);
            return _converter.Parse (instrumentoEntity);
        }
    }
}