using System.Collections.Generic;
using SIGADM.Business.Interfaces.Setorizacao.Irmaos;
using SIGADM.Data.Converters.Patrimonio.Setorizacao.Irmaos;
using SIGADM.Data.VO.Patrimonio.Setorizacao.Irmaos;
using SIGADM.Model.Patrimonio.Setorizacao.Irmaos;
using SIGADM.Repository.Generic;
using SIGADM.Repository.Interfaces.Repository_Irmaos;

namespace SIGADM.Business.Implementattions.Patrimonio.Setorizacao.Irmaos {
    public class UsuarioIgrejaBusinessImpl : IUsuarioIgrejaBusiness {
        private IRepositoryUsuarioIgreja _repository;
        private readonly UsuarioIgrejaConverter _converter;
        public UsuarioIgrejaBusinessImpl (IRepositoryUsuarioIgreja repository) {
            _repository = repository;
            _converter = new UsuarioIgrejaConverter ();
        }
        public UsuarioIgrejaVO Create (UsuarioIgrejaVO igreja) {
            var instrumentoEntity = _converter.Parse (igreja);
            instrumentoEntity = _repository.Create (instrumentoEntity);
            return _converter.Parse (instrumentoEntity);
        }

        public void Delete (long id) {
            _repository.Delete (id);
        }

        public List<UsuarioIgrejaVO> FindAll () {
            return _converter.ParseList (_repository.FindAll ());
        }

        public UsuarioIgrejaVO FindById (long id) {
            return _converter.Parse (_repository.FindById (id));
        }
        public UsuarioIgrejaVO ProcurarIdUsuario (int idUsuario) {
            return _converter.Parse (_repository.ProcurarIdUsuario (idUsuario));
        }

        public UsuarioIgrejaVO Update (UsuarioIgrejaVO igreja) {
            var instrumentoEntity = _converter.Parse (igreja);
            instrumentoEntity = _repository.Update (instrumentoEntity);
            return _converter.Parse (instrumentoEntity);
        }
    }
}