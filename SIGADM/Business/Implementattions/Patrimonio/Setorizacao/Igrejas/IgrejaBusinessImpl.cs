using System.Collections.Generic;
using SIGADM.Business.Interfaces.Setorizacao.Igrejas;
using SIGADM.Data.Converters.Patrimonio.Setorizacao.Igrejas;
using SIGADM.Data.VO.Patrimonio.Setorizacao.Igrejas;
using SIGADM.Repository.Generic;
using SIGADM.Repository.Repository_Setorizacao.Repository_Igrejas;

namespace SIGADM.Business.Implementattions.Patrimonio.Setorizacao.Igrejas
{
    public class IgrejaBusinessImpl : IIgrejaBusiness
    {
        private IRepositoryIgreja _repository;
        private readonly IgrejaConverter _converter;
        public IgrejaBusinessImpl(IRepositoryIgreja repository )
        {
            _repository = repository;
            _converter = new IgrejaConverter();
        }
        public IgrejaVO Create(IgrejaVO igreja)
        {
             var igrejaEntity = _converter.Parse(igreja);
            igrejaEntity = _repository.Create(igrejaEntity);
            return _converter.Parse(igrejaEntity);
        }

        public void Delete(long id)
        {
            _repository.Delete(id);
        }
        

        public List<IgrejaVO> FindAll()
        {
            return _converter.ParseList(_repository.FindAll());
        }

        public IgrejaVO FindById(long id)
        {
            return _converter.Parse(_repository.FindById(id));
        }

        public IgrejaVO Update(IgrejaVO igreja)
        {
            var igrejaEntity = _converter.Parse(igreja);
            igrejaEntity = _repository.Update(igrejaEntity);
            return _converter.Parse(igrejaEntity);
        }
    }
}