using System.Collections.Generic;
using SIGADM.Business.Interfaces.Aquisicao_data;
using SIGADM.Data.Converters.Patrimonio.Aquisicao_data;
using SIGADM.Data.VO.Patrimonio.Aquisicao_data;
using SIGADM.Model.Patrimonio.Aquisicao_data;
using SIGADM.Repository.Generic;
using SIGADM.Repository.Interfaces;

namespace SIGADM.Business.Implementattions.Patrimonio {
    public class OrigemInstrumentoBusinessImpl : IOrigemInstrumentoBusiness {
        private IRepositoryOrigem _repository;
        private readonly OrigemInstrumentoConverter _converter;
        public OrigemInstrumentoBusinessImpl (IRepositoryOrigem repository) {
            _repository = repository;
            _converter = new OrigemInstrumentoConverter ();
        }
        public OrigemInstrumentoVO Create (OrigemInstrumentoVO origem) {
            var origemEntity = _converter.Parse (origem);
            origemEntity = _repository.Create (origemEntity);
            return _converter.Parse (origemEntity);
        }

        public void Delete (long id) {
            _repository.Delete (id);
        }

        public List<OrigemInstrumentoVO> FindAll () {
            return _converter.ParseList (_repository.FindAll ());
        }

        public OrigemInstrumentoVO FindById (long id) {
            return _converter.Parse (_repository.FindById (id));
        }

        public OrigemInstrumentoVO Update (OrigemInstrumentoVO origem) {
            var origemEntity = _converter.Parse (origem);
            origemEntity = _repository.Update (origemEntity);
            return _converter.Parse (origemEntity);
        }


    }
}