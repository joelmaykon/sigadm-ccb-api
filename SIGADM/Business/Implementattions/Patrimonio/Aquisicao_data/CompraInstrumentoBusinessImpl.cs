using System.Collections.Generic;
using SIGADM.Business.Interfaces.Aquisicao_data;
using SIGADM.Data.Converters.Patrimonio.Aquisicao_data;
using SIGADM.Data.VO.Patrimonio.Aquisicao_data;
using SIGADM.Repository.Interfaces;

namespace SIGADM.Business.Implementattions.Patrimonio
{
    public class CompraInstrumentoBusinessImpl : ICompraInstrumentoBusiness
    {
        private IRepositoryCompra  _repository;
        private readonly CompraInstrumentoConverter _converter;
        public CompraInstrumentoBusinessImpl(IRepositoryCompra repository )
        {
            _repository = repository;
            _converter = new CompraInstrumentoConverter();
        }
        public CompraInstrumentoVO Create(CompraInstrumentoVO compra)
        {
             var compraEntity = _converter.Parse(compra);
            compraEntity = _repository.Create(compraEntity);
            return _converter.Parse(compraEntity);
        }

        public void Delete(long id)
        {
            _repository.Delete(id);
        }

        public List<CompraInstrumentoVO> FindAll()
        {
            return _converter.ParseList(_repository.FindAll());
        }
        public List<CompraInstrumentoVO> FindCompra(string tombamento)
        {
            return _converter.ParseList(_repository.FindCompra(tombamento));
        }

        public CompraInstrumentoVO FindById(long id)
        {
            return _converter.Parse(_repository.FindById(id));
        }

        public CompraInstrumentoVO Update(CompraInstrumentoVO compra)
        {
            var compraEntity = _converter.Parse(compra);
            compraEntity = _repository.Update(compraEntity);
            return _converter.Parse(compraEntity);
        }
    }
}