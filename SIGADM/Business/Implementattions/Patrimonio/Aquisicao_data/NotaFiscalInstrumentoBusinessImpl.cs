using System.Collections.Generic;
using SIGADM.Business.Interfaces.Aquisicao_data;
using SIGADM.Data.Converters.Patrimonio.Aquisicao_data;
using SIGADM.Data.VO.Patrimonio.Aquisicao_data;
using SIGADM.Repository.Interfaces;

namespace SIGADM.Business.Implementattions.Patrimonio
{
    public class NotaFiscalInstrumentoBusinessImpl : INotaFiscalInstrumentoBusiness {
        private IRepositoryNotaFiscal _repository;
        private readonly NotaFiscalInstrumentoConverter _converter;
        public NotaFiscalInstrumentoBusinessImpl (IRepositoryNotaFiscal repository) {
            _repository = repository;
            _converter = new NotaFiscalInstrumentoConverter ();
        }
        public NotaFiscalInstrumentoVO Create (NotaFiscalInstrumentoVO nota) {
            var notaEntity = _converter.Parse (nota);
            notaEntity = _repository.Create (notaEntity);
            return _converter.Parse (notaEntity);
        }

        public void Delete (long id) {
            _repository.Delete (id);
        }

        public List<NotaFiscalInstrumentoVO> FindAll () {
            return _converter.ParseList (_repository.FindAll ());
        }

        public NotaFiscalInstrumentoVO FindById (long id) {
            return _converter.Parse (_repository.FindById (id));
        }

        public NotaFiscalInstrumentoVO Update (NotaFiscalInstrumentoVO nota) {
            var notaEntity = _converter.Parse (nota);
            notaEntity = _repository.Update (notaEntity);
            return _converter.Parse (notaEntity);
        }


    }
}