using System.Collections.Generic;
using SIGADM.Business.Interfaces.Aquisicao_data;
using SIGADM.Data.Converters.Patrimonio.Aquisicao_data;
using SIGADM.Data.VO.Patrimonio.Aquisicao_data;
using SIGADM.Model.Patrimonio.Aquisicao_data;
using SIGADM.Repository.Generic;
using SIGADM.Repository.Interfaces;

namespace SIGADM.Business.Implementattions.Patrimonio {
    public class DoacaoInstrumentoBusinessImpl : IDoacaoInstrumentoBusiness {
        private IRepositoryDoacao _repository;
        private readonly DoacaoInstrumentoConverter _converter;
        public DoacaoInstrumentoBusinessImpl (IRepositoryDoacao repository) {
            _repository = repository;
            _converter = new DoacaoInstrumentoConverter ();
        }
        public DoacaoInstrumentoVO Create (DoacaoInstrumentoVO doacao) {
            var doacaoEntity = _converter.Parse (doacao);
            doacaoEntity = _repository.Create (doacaoEntity);
            return _converter.Parse (doacaoEntity);
        }

        public void Delete (long id) {
            _repository.Delete (id);
        }

        public List<DoacaoInstrumentoVO> FindAll () {
            return _converter.ParseList (_repository.FindAll ());
        }

        public DoacaoInstrumentoVO FindById (long id) {
            return _converter.Parse (_repository.FindById (id));
        }

        public DoacaoInstrumentoVO Update (DoacaoInstrumentoVO doacao) {
            var doacaoEntity = _converter.Parse (doacao);
            doacaoEntity = _repository.Update (doacaoEntity);
            return _converter.Parse (doacaoEntity);
        }

        public List<DoacaoInstrumentoVO> FindDoacao (string tombamento) {
            return _converter.ParseList (_repository.FindDoacao (tombamento));
        }
    }
}