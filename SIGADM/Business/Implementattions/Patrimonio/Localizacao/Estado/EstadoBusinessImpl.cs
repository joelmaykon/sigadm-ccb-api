using System.Collections.Generic;
using SIGADM.Business.Interfaces.Localizacao.Estado;
using SIGADM.Data.Converters.Patrimonio.Localizacao.EstadoValue;
using SIGADM.Data.VO.Patrimonio.Localizacao.Estado;
using SIGADM.Repository.Generic;
using SIGADM.Repository.Repository_Localizacao.Repository_Estado;

namespace SIGADM.Business.Implementattions.Patrimonio.Localizacao.Estado
{
    public class EstadoBusinessImpl : IEstadoBusiness
    {
        private IRepositoryEstado _repository;
        private readonly EstadoConverter _converter;
        public EstadoBusinessImpl(IRepositoryEstado repository )
        {
            _repository = repository;
            _converter = new EstadoConverter();
        }
        public EstadoVO Create(EstadoVO estado)
        {
             var estadoEntity = _converter.Parse(estado);
            estadoEntity = _repository.Create(estadoEntity);
            return _converter.Parse(estadoEntity);
        }

        public void Delete(long id)
        {
            _repository.Delete(id);
        }
        

        public List<EstadoVO> FindAll()
        {
            return _converter.ParseList(_repository.FindAll());
        }

        public EstadoVO FindById(long id)
        {
            return _converter.Parse(_repository.FindById(id));
        }

        public EstadoVO Update(EstadoVO estado)
        {
            var estadoEntity = _converter.Parse(estado);
            estadoEntity = _repository.Update(estadoEntity);
            return _converter.Parse(estadoEntity);
        }
    }
}