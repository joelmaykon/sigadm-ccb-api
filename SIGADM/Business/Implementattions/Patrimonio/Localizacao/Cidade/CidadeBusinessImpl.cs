using System.Collections.Generic;
using SIGADM.Business.Interfaces.Localizacao.Cidade;
using SIGADM.Data.Converters.Patrimonio.Localizacao.CidadeValue;
using SIGADM.Data.VO.Patrimonio.Localizacao.Cidade;
using SIGADM.Repository.Generic;
using SIGADM.Repository.Repository_Localizacao.Repository_Cidade;

namespace SIGADM.Business.Implementattions.Patrimonio.Localizacao.Cidade
{
    public class CidadeBusinessImpl : ICidadeBusiness
    {
        private IRepositoryCidade _repository;
        private readonly CidadeConverter _converter;
        public CidadeBusinessImpl(IRepositoryCidade repository )
        {
            _repository = repository;
            _converter = new CidadeConverter();
        }
        public CidadeVO Create(CidadeVO cidade)
        {
             var cidadeEntity = _converter.Parse(cidade);
            cidadeEntity = _repository.Create(cidadeEntity);
            return _converter.Parse(cidadeEntity);
        }

        public void Delete(long id)
        {
            _repository.Delete(id);
        }
        

        public List<CidadeVO> FindAll()
        {
            return _converter.ParseList(_repository.FindAll());
        }

        public CidadeVO FindById(long id)
        {
            return _converter.Parse(_repository.FindById(id));
        }

        public CidadeVO Update(CidadeVO cidade)
        {
            var cidadeEntity = _converter.Parse(cidade);
            cidadeEntity = _repository.Update(cidadeEntity);
            return _converter.Parse(cidadeEntity);
        }
    }
}