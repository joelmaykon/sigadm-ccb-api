using System.Collections.Generic;
using SIGADM.Business.Interfaces.Instrumento_data;
using SIGADM.Data.Converters.Patrimonio.Instrumento_data;
using SIGADM.Data.VO.Patrimonio.Instrumento_data;
using SIGADM.Model.Patrimonio.Instrumento_data;
using SIGADM.Repository.Generic;
using SIGADM.Repository.Interfaces.Repository_Instrumento;

namespace SIGADM.Business.Implementattions.Patrimonio.Instrumento_data
{
    public class NomeInstrumentoBusinessImpl : INomeInstrumentoBusiness
    {
        private IRepositoryNomeInstrumento _repository;
        private readonly NomeInstrumentoConverter _converter;
        public NomeInstrumentoBusinessImpl(IRepositoryNomeInstrumento repository )
        {
            _repository = repository;
            _converter = new NomeInstrumentoConverter();
        }
        public NomeInstrumentoVO Create(NomeInstrumentoVO instrumento)
        {
             var instrumentoEntity = _converter.Parse(instrumento);
            instrumentoEntity = _repository.Create(instrumentoEntity);
            return _converter.Parse(instrumentoEntity);
        }

        public void Delete(long id)
        {
            _repository.Delete(id);
        }
        public void DeleteNome(string nome)
        {
            _repository.DeleteNome(nome);
        }

        public List<NomeInstrumentoVO> FindAll()
        {
            return _converter.ParseList(_repository.FindAll());
        }

        public NomeInstrumentoVO FindById(long id)
        {
            return _converter.Parse(_repository.FindById(id));
        }

        public NomeInstrumentoVO Update(NomeInstrumentoVO instrumento)
        {
            var instrumentoEntity = _converter.Parse(instrumento);
            instrumentoEntity = _repository.Update(instrumentoEntity);
            return _converter.Parse(instrumentoEntity);
        }
    }
}