using System.Collections.Generic;
using SIGADM.Business.Interfaces.Instrumento_data;
using SIGADM.Data.Converters.Patrimonio.Instrumento_data;
using SIGADM.Data.VO.Patrimonio.Instrumento_data;
using SIGADM.Model.Patrimonio.Instrumento_data;
using SIGADM.Repository.Generic;
using SIGADM.Repository.Interfaces.Repository_Instrumento;

namespace SIGADM.Business.Implementattions.Patrimonio.Instrumento_data
{
    public class InstrumentoBusinessImpl : I_instrumentoBusiness
    {
        private IRepository<Instrumento> _repository;
        private IRepositoryInstrumento _repository_instrumento;
        private readonly InstrumentoConverter _converter;
        public InstrumentoBusinessImpl(IRepository<Instrumento> repository ,IRepositoryInstrumento repository_instrumento)
        {
            _repository = repository;
            _repository_instrumento = repository_instrumento;
            _converter = new InstrumentoConverter();
        }
        public InstrumentoVO Create(InstrumentoVO instrumento)
        {
             var instrumentoEntity = _converter.Parse(instrumento);
            instrumentoEntity = _repository.Create(instrumentoEntity);
            return _converter.Parse(instrumentoEntity);
        }

        public void Delete(long id)
        {
            _repository.Delete(id);
        }

        public List<InstrumentoVO> FindAll()
        {
            return _converter.ParseList(_repository.FindAll());
        }

        public InstrumentoVO FindById(long id)
        {
            return _converter.Parse(_repository.FindById(id));
        }

        public InstrumentoVO Update(InstrumentoVO instrumento)
        {
            var instrumentoEntity = _converter.Parse(instrumento);
            instrumentoEntity = _repository.Update(instrumentoEntity);
            return _converter.Parse(instrumentoEntity);
        }

        public List<InstrumentoVO> BuscarInstrumentosNomes(string nome)
        {
           return _converter.ParseList(_repository_instrumento.FindInstrumentosNome(nome));
        }
    }
}