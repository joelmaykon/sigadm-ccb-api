using System.Collections.Generic;
using SIGADM.Business.Interfaces.Instrumento_data;
using SIGADM.Data.Converters.Patrimonio.Instrumento_data;
using SIGADM.Data.VO.Patrimonio.Instrumento_data;
using SIGADM.Model.Patrimonio.Instrumento_data;
using SIGADM.Repository.Generic;

namespace SIGADM.Business.Implementattions.Patrimonio
{
    public class ComponenteInstrumentoBusinessImpl : IComponenteInstrumentoBusiness
    {
        private IRepository<ComponenteInstrumento> _repository;
        private readonly ComponenteInstrumentoConverter _converter;
        public ComponenteInstrumentoBusinessImpl(IRepository<ComponenteInstrumento> repository )
        {
            _repository = repository;
            _converter = new ComponenteInstrumentoConverter();
        }
        public ComponenteInstrumentoVO Create(ComponenteInstrumentoVO componente)
        {
             var componenteEntity = _converter.Parse(componente);
            componenteEntity = _repository.Create(componenteEntity);
            return _converter.Parse(componenteEntity);
        }

        public void Delete(long id)
        {
            _repository.Delete(id);
        }

        public List<ComponenteInstrumentoVO> FindAll()
        {
            return _converter.ParseList(_repository.FindAll());
        }

        public ComponenteInstrumentoVO FindById(long id)
        {
            return _converter.Parse(_repository.FindById(id));
        }

        public ComponenteInstrumentoVO Update(ComponenteInstrumentoVO componente)
        {
            var componenteEntity = _converter.Parse(componente);
            componenteEntity = _repository.Update(componenteEntity);
            return _converter.Parse(componenteEntity);
        }
    }
}