using System.Collections.Generic;
using SIGADM.Business.Interfaces.Instrumento_data;
using SIGADM.Data.Converters.Patrimonio.Instrumento_data;
using SIGADM.Data.VO.Patrimonio.Instrumento_data;
using SIGADM.Model.Patrimonio.Instrumento_data;
using SIGADM.Repository.Generic;
using SIGADM.Repository.Interfaces.Repository_Instrumento;

namespace SIGADM.Business.Implementattions.Patrimonio.Instrumento_data
{
    public class CaracteristicaInstrumentoBusinessImpl : ICaracteristicaInstrumentoBusiness
    {
        private IRepositoryCaracteristicaInstrumento _repository;
        private readonly CaracteristicaInstrumentoConverter _converter;
        public CaracteristicaInstrumentoBusinessImpl(IRepositoryCaracteristicaInstrumento repository )
        {
            _repository = repository;
            _converter = new CaracteristicaInstrumentoConverter();
        }
        public CaracteristicaInstrumentoVO Create(CaracteristicaInstrumentoVO caracteristica)
        {
             var caracteristicaEntity = _converter.Parse(caracteristica);
            caracteristicaEntity = _repository.Create(caracteristicaEntity);
            return _converter.Parse(caracteristicaEntity);
        }

        public void Delete(long id)
        {
            _repository.Delete(id);
        }
        public void DeleteCaracteristica(string caracteristica)
        {
            _repository.DeleteCaracteristica(caracteristica);
        }

        public List<CaracteristicaInstrumentoVO> FindAll()
        {
            return _converter.ParseList(_repository.FindAll());
        }

        public CaracteristicaInstrumentoVO FindById(long id)
        {
            return _converter.Parse(_repository.FindById(id));
        }

        public CaracteristicaInstrumentoVO Update(CaracteristicaInstrumentoVO caracteristica)
        {
            var caracteristicaEntity = _converter.Parse(caracteristica);
            caracteristicaEntity = _repository.Update(caracteristicaEntity);
            return _converter.Parse(caracteristicaEntity);
        }
        public List<CaracteristicaInstrumentoVO> FindCaracteristicaNaipe (string naipe) {
            return _converter.ParseList (_repository.FindCaracteristicaNaipe (naipe));
        }
    }
}