using System.Collections.Generic;
using SIGADM.Business.Interfaces.Instrumento_data;
using SIGADM.Data.Converters.Patrimonio.Instrumento_data;
using SIGADM.Data.VO.Patrimonio.Instrumento_data;
using SIGADM.Model.Patrimonio.Instrumento_data;
using SIGADM.Repository.Generic;
using SIGADM.Repository.Interfaces.Repository_Instrumento;

namespace SIGADM.Business.Implementattions.Patrimonio.Instrumento_data
{
    public class MarcaInstrumentoBusinessImpl : IMarcaInstrumentoBusiness
    {
        private IRepositoryMarcaInstrumento _repository;
        private readonly MarcaInstrumentoConverter _converter;
        public MarcaInstrumentoBusinessImpl(IRepositoryMarcaInstrumento repository )
        {
            _repository = repository;
            _converter = new MarcaInstrumentoConverter();
        }
        public MarcaInstrumentoVO Create(MarcaInstrumentoVO instrumento)
        {
             var instrumentoEntity = _converter.Parse(instrumento);
            instrumentoEntity = _repository.Create(instrumentoEntity);
            return _converter.Parse(instrumentoEntity);
        }

        public void Delete(long id)
        {
            _repository.Delete(id);
        }
         public void DeleteMarca(string marca)
        {
            _repository.DeleteMarca(marca);
        }

        public List<MarcaInstrumentoVO> FindAll()
        {
            return _converter.ParseList(_repository.FindAll());
        }

        public MarcaInstrumentoVO FindById(long id)
        {
            return _converter.Parse(_repository.FindById(id));
        }

        public MarcaInstrumentoVO Update(MarcaInstrumentoVO instrumento)
        {
            var instrumentoEntity = _converter.Parse(instrumento);
            instrumentoEntity = _repository.Update(instrumentoEntity);
            return _converter.Parse(instrumentoEntity);
        }
    }
}