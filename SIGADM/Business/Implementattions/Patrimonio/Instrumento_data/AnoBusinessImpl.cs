using System.Collections.Generic;
using SIGADM.Data.VO.Patrimonio.Instrumento_data;
using SIGADM.Model.Patrimonio.Datas;
using SIGADM.Repository.Generic;
using SIGADM.Data.Converters.Patrimonio.Instrumento_data;
using SIGADM.Business.Interfaces.Instrumento_data;
using SIGADM.Repository.Interfaces;

namespace SIGADM.Business.Implementattions.Patrimonio.Instrumento_data
{
    public class AnoBusinessImpl : IAnoBusiness
    {
        private IRepositoryAnoInstrumento _repository;
        private readonly AnoConverter _converter;
        public AnoBusinessImpl(IRepositoryAnoInstrumento repository )
        {
            _repository = repository;
            _converter = new AnoConverter();
        }
        public AnoVO Create(AnoVO ano)
        {
             var anoEntity = _converter.Parse(ano);
            anoEntity = _repository.Create(anoEntity);
            return _converter.Parse(anoEntity);
        }

        public void Delete(long id)
        {
            _repository.Delete(id);
        }
        public void DeleteAno(string ano)
        {
            _repository.DeleteAno(ano);
        }

        public List<AnoVO> FindAll()
        {
            return _converter.ParseList(_repository.FindAll());
        }

        public AnoVO FindById(long id)
        {
            return _converter.Parse(_repository.FindById(id));
        }

        public AnoVO Update(AnoVO ano)
        {
            var anoEntity = _converter.Parse(ano);
            anoEntity = _repository.Update(anoEntity);
            return _converter.Parse(anoEntity);
        }
    }
}