using System.Collections.Generic;
using SIGADM.Business.Interfaces.Instrumento_data;
using SIGADM.Data.Converters.Patrimonio.Instrumento_data;
using SIGADM.Data.VO.Patrimonio.Instrumento_data;
using SIGADM.Model.Patrimonio.Instrumento_data;
using SIGADM.Repository.Generic;
using SIGADM.Repository.Interfaces.Repository_Instrumento;

namespace SIGADM.Business.Implementattions.Patrimonio {
    public class TombamentoInstrumentoBusinessImpl : ITombamentoInstrumentoBusiness {
        private IRepositoryTombamento _repository;
        private readonly TombamentoInstrumentoConverter _converter;
        public TombamentoInstrumentoBusinessImpl (IRepositoryTombamento repository) {
            _repository = repository;
            _converter = new TombamentoInstrumentoConverter ();
        }
        public TombamentoInstrumentoVO Create (TombamentoInstrumentoVO instrumento) {
            var tombamentoEntity = _converter.Parse (instrumento);
            tombamentoEntity = _repository.Create (tombamentoEntity);
            return _converter.Parse (tombamentoEntity);
        }

        public void Delete (long id) {
            _repository.Delete (id);
        }

        public List<TombamentoInstrumentoVO> FindAll () {
            return _converter.ParseList (_repository.FindAll ());
        }

        public TombamentoInstrumentoVO FindById (long id) {
            return _converter.Parse (_repository.FindById (id));
        }
        public TombamentoInstrumentoVO FindByTombamento (string tombamento) {
            return _converter.Parse (_repository.FindByTombamento (tombamento));
        }

        public TombamentoInstrumentoVO Update (TombamentoInstrumentoVO instrumento) {
            var tombamentoEntity = _converter.Parse (instrumento);
            tombamentoEntity = _repository.Update (tombamentoEntity);
            return _converter.Parse (tombamentoEntity);
        }
        public TombamentoInstrumentoVO SelecionarTombamento (string tombamento) {

            var tombamentoEntity = _repository.SelecionarTombamento (tombamento);
            return _converter.Parse (tombamentoEntity);
        }
    }
}