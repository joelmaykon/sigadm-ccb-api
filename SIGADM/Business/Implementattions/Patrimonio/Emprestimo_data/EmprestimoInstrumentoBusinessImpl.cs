using System.Collections.Generic;
using SIGADM.Business.Interfaces.Emprestimo_data;
using SIGADM.Data.Converters.Patrimonio.Emprestimo_data;
using SIGADM.Data.VO.Patrimonio.Emprestimo_data;
using SIGADM.Model.Patrimonio.Emprestimo_data;
using SIGADM.Repository.Generic;
using SIGADM.Repository.Interfaces;

namespace SIGADM.Business.Implementattions.Patrimonio.Emprestimo_data
{
    public class EmprestimoInstrumentoBusinessImpl : IEmprestimoInstrumentoBusiness
    {
        private IRepositoryEmprestimo _repository;
        private readonly EmprestimoInstrumentoConverter _converter;
        public EmprestimoInstrumentoBusinessImpl(IRepositoryEmprestimo repository )
        {
            _repository = repository;
            _converter = new EmprestimoInstrumentoConverter();
        }
        public EmprestimoInstrumentoVO Create(EmprestimoInstrumentoVO instrumento)
        {
             var instrumentoEntity = _converter.Parse(instrumento);
            instrumentoEntity = _repository.Create(instrumentoEntity);
            return _converter.Parse(instrumentoEntity);
        }

        public void Delete(long id)
        {
            _repository.Delete(id);
        }

        public List<EmprestimoInstrumentoVO> FindAll()
        {
            return _converter.ParseList(_repository.FindAll());
        }

        public EmprestimoInstrumentoVO FindById(long id)
        {
            return _converter.Parse(_repository.FindById(id));
        }

        public EmprestimoInstrumentoVO Update(EmprestimoInstrumentoVO instrumento)
        {
            var instrumentoEntity = _converter.Parse(instrumento);
            instrumentoEntity = _repository.Update(instrumentoEntity);
            return _converter.Parse(instrumentoEntity);
        }
        public List<EmprestimoInstrumentoVO> FindEmprestimo (string tombamento) {
            return _converter.ParseList (_repository.FindEmprestimo (tombamento));
        }
    }
}