using System.Collections.Generic;
using SIGADM.Business.Interfaces.Empresa_data;
using SIGADM.Data.Converters.Patrimonio.Empresa_data;
using SIGADM.Data.VO.Patrimonio.Empresa_data;
using SIGADM.Model.Patrimonio.Empresa_data;
using SIGADM.Repository.Generic;

namespace SIGADM.Business.Implementattions.Patrimonio.Empresa_data
{
    public class EmpresaBusinessImpl : IEmpresaBusiness
    {
        private IRepository<Empresa> _repository;
        private readonly EmpresaConverter _converter;
        public EmpresaBusinessImpl(IRepository<Empresa> repository )
        {
            _repository = repository;
            _converter = new EmpresaConverter();
        }
        public EmpresaVO Create(EmpresaVO empresa)
        {
             var empresaEntity = _converter.Parse(empresa);
            empresaEntity = _repository.Create(empresaEntity);
            return _converter.Parse(empresaEntity);
        }

        public void Delete(long id)
        {
            _repository.Delete(id);
        }

        public List<EmpresaVO> FindAll()
        {
            return _converter.ParseList(_repository.FindAll());
        }

        public EmpresaVO FindById(long id)
        {
            return _converter.Parse(_repository.FindById(id));
        }

        public EmpresaVO Update(EmpresaVO empresa)
        {
            var empresaEntity = _converter.Parse(empresa);
            empresaEntity = _repository.Update(empresaEntity);
            return _converter.Parse(empresaEntity);
        }
    }
}