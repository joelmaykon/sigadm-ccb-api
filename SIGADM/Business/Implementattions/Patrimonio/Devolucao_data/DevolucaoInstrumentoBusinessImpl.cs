using System.Collections.Generic;
using SIGADM.Business.Interfaces.Devolucao_data;
using SIGADM.Data.Converters.Patrimonio.Devolucao_data;
using SIGADM.Data.VO.Patrimonio.Devolucao_data;
using SIGADM.Model.Patrimonio.Devolucao_data;
using SIGADM.Repository.Generic;
using SIGADM.Repository.Interfaces;

namespace SIGADM.Business.Implementattions.Patrimonio.Devolucao_data
{
    public class DevolucaoInstrumentoBusinessImpl : IDevolucaoInstrumentoBusiness
    {
        private IRepositoryDevolucao _repository;
        private readonly DevolucaoInstrumentoConverter _converter;
        public DevolucaoInstrumentoBusinessImpl(IRepositoryDevolucao repository )
        {
            _repository = repository;
            _converter = new DevolucaoInstrumentoConverter();
        }
        public DevolucaoInstrumentoVO Create(DevolucaoInstrumentoVO devolucao)
        {
             var devolucaoEntity = _converter.Parse(devolucao);
            devolucaoEntity = _repository.Create(devolucaoEntity);
            return _converter.Parse(devolucaoEntity);
        }

        public void Delete(long id)
        {
            _repository.Delete(id);
        }

        public List<DevolucaoInstrumentoVO> FindAll()
        {
            return _converter.ParseList(_repository.FindAll());
        }

        public DevolucaoInstrumentoVO FindById(long id)
        {
            return _converter.Parse(_repository.FindById(id));
        }

        public DevolucaoInstrumentoVO Update(DevolucaoInstrumentoVO devolucao)
        {
            var devolucaoEntity = _converter.Parse(devolucao);
            devolucaoEntity = _repository.Update(devolucaoEntity);
            return _converter.Parse(devolucaoEntity);
        }
        public List<DevolucaoInstrumentoVO> FindDevolucao (string tombamento) {
            return _converter.ParseList (_repository.FindDevolucao (tombamento));
        }
    }
}