﻿using SIGADM.Model;
using SIGADM.Security.Configuration;
using System;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Principal;
using System.IO;

namespace SIGADM.Business
{
    public class TermoDoacaoBusinessImpl : ITermoDoacaoBusiness
    {
        public byte[] GetImagem()
        {
            string path = Directory.GetCurrentDirectory();
            //Local onde fica o arquivo
            var fullPath = path + "/Resources/Doacao/Termos/Autumn_in_Kanas_by_Wang_Jinyu.jpg";
            return File.ReadAllBytes(fullPath);

        }
        public byte[] GetTermoDoacao()
        {
            string path = Directory.GetCurrentDirectory();
            //Local onde fica o arquivo
            var fullPath = path + "/Resources/Doacao/Termos/0001-termo.pdf";
            return File.ReadAllBytes(fullPath);
        }
    }
}
