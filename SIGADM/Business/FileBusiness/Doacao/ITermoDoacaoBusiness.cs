﻿using SIGADM.Model;

namespace SIGADM.Business
{
    public interface ITermoDoacaoBusiness
    {
         byte[] GetTermoDoacao();
         byte[] GetImagem();
    }
}
