using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using SIGADM.Business.Interfaces.Devolucao_data;
using SIGADM.Data.VO.Patrimonio.Devolucao_data;
using Swashbuckle.AspNetCore.SwaggerGen;
using Tapioca.HATEOAS;


namespace SIGADM.Controllers.Patrimonio.Devolucao_data
{   
    [ApiVersion("1")]
    [Route("api/[controller]/v{version:apiVersion}")]
    public class DevolucaoInstrumentoController : Controller
    {
        private IDevolucaoInstrumentoBusiness _devolucaoBusiness;
        public DevolucaoInstrumentoController(IDevolucaoInstrumentoBusiness devolucaoBusiness) 
        {
            _devolucaoBusiness = devolucaoBusiness;
        }

        [HttpGet]
        [SwaggerResponse((200), Type = typeof(List<DevolucaoInstrumentoVO>))]
        [SwaggerResponse(204)]
        [SwaggerResponse(400)]
        [SwaggerResponse(401)]
        [Authorize("Bearer")]
        [TypeFilter(typeof(HyperMediaFilter))]
        public IActionResult Get()
        {
            return new OkObjectResult(_devolucaoBusiness.FindAll());
        }
        [HttpGet("buscar-devolucao")]
        [SwaggerResponse((200), Type = typeof(List<DevolucaoInstrumentoVO>))]
        [SwaggerResponse(204)]
        [SwaggerResponse(400)]
        [SwaggerResponse(401)]
        [Authorize("Bearer")]
        [TypeFilter(typeof(HyperMediaFilter))]
        public IActionResult GetDoacao([FromQuery] string tombamento)
        {
            return new OkObjectResult(_devolucaoBusiness.FindDevolucao(tombamento));
        }
        // Configura o Swagger para a operação
        // http://localhost:{porta}/api/books/v1/{id}
        // [SwaggerResponse((202), Type = typeof(Book))]
        // determina o objeto de retorno em caso de sucesso Book
        // O [SwaggerResponse(XYZ)] define os códigos de retorno 204, 400 e 401
        [HttpGet("{id}")]
        [SwaggerResponse((200), Type = typeof(DevolucaoInstrumentoVO))]
        [SwaggerResponse(204)]
        [SwaggerResponse(400)]
        [SwaggerResponse(401)]
        [Authorize("Bearer")]
        [TypeFilter(typeof(HyperMediaFilter))]
        public IActionResult Get(long id)
        {
            var devolucao = _devolucaoBusiness.FindById(id);
            if (devolucao == null) return NotFound();
            return new OkObjectResult(devolucao);
        }

        // Configura o Swagger para a operação
        // http://localhost:{porta}/api/
        // [SwaggerResponse((202), Type = typeof(Book))]
        // determina o objeto de retorno em caso de sucesso Book
        // O [SwaggerResponse(XYZ)] define os códigos de retorno 400 e 401
        [HttpPost]
        [SwaggerResponse((201), Type = typeof(DevolucaoInstrumentoVO))]
        [SwaggerResponse(400)]
        [SwaggerResponse(401)]
        [Authorize("Bearer")]
        [TypeFilter(typeof(HyperMediaFilter))]
        public IActionResult Post([FromBody]DevolucaoInstrumentoVO devolucaoVO)
        {
            if (devolucaoVO == null) return BadRequest();
            return new OkObjectResult(_devolucaoBusiness.Create(devolucaoVO));
        }

        // Configura o Swagger para a operação
        // http://localhost:{porta}/api/books/v1/
        // determina o objeto de retorno em caso de sucesso Book
        // O [SwaggerResponse(XYZ)] define os códigos de retorno 400 e 401
        [HttpPut]
        [SwaggerResponse((202), Type = typeof(DevolucaoInstrumentoVO))]
        [SwaggerResponse(400)]
        [SwaggerResponse(401)]
        [Authorize("Bearer")]
        [TypeFilter(typeof(HyperMediaFilter))]
        public IActionResult Put([FromBody]DevolucaoInstrumentoVO devolucaoVO)
        {
            if (devolucaoVO == null) return BadRequest();
            var updatedDevolucao = _devolucaoBusiness.Update(devolucaoVO);
            if (updatedDevolucao == null) return BadRequest();
            return new OkObjectResult(updatedDevolucao);
        }

        // Configura o Swagger para a operação
        // http://localhost:{porta}/api/books/v1/{id}
        // O [SwaggerResponse(XYZ)] define os códigos de retorno 400 e 401
        [HttpDelete("{id}")]
        [SwaggerResponse(204)]
        [SwaggerResponse(400)]
        [SwaggerResponse(401)]
        [Authorize("Bearer")]
        [TypeFilter(typeof(HyperMediaFilter))]
        public IActionResult Delete(int id)
        {
            _devolucaoBusiness.Delete(id);
            return NoContent();
        }
    }
}