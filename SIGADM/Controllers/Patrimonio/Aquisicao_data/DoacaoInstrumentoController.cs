using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http.Headers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using SIGADM.Business;
using SIGADM.Business.Interfaces.Aquisicao_data;
using SIGADM.Data.VO.Patrimonio.Aquisicao_data;
using Swashbuckle.AspNetCore.SwaggerGen;
using Tapioca.HATEOAS;

namespace SIGADM.Controllers.Patrimonio.Aquisicao_data {
    [ApiVersion ("1")]
    [Route ("api/[controller]/v{version:apiVersion}")]
    public class DoacaoInstrumentoController : Controller {
        private IDoacaoInstrumentoBusiness _doacaoinstrumentoBusiness;
        private ITermoDoacaoBusiness _termoDoacaoBusiness;
        public DoacaoInstrumentoController (IDoacaoInstrumentoBusiness doacaoinstrumentoBusiness, ITermoDoacaoBusiness termoDoacaoBusiness) {
            _doacaoinstrumentoBusiness = doacaoinstrumentoBusiness;
            _termoDoacaoBusiness = termoDoacaoBusiness;
        }

        [HttpGet ("termo-doacao-download"), DisableRequestSizeLimit]
        [SwaggerResponse ((200), Type = typeof (byte[]))]
        [SwaggerResponse (400)]
        [SwaggerResponse (401)]
        [Authorize ("Bearer")]
        [TypeFilter (typeof (HyperMediaFilter))]
        public IActionResult TermoDoacaoDownload () {
            byte[] buffer = _termoDoacaoBusiness.GetTermoDoacao ();
            if (buffer != null) {
                //configuração do tipo de arquivo
                HttpContext.Response.ContentType = "application/pdf";
                HttpContext.Response.Headers.Add ("content-length", buffer.Length.ToString ());
                HttpContext.Response.Body.Write (buffer, 0, buffer.Length);
            }
            return new ContentResult ();
        }
        [HttpGet ("termo-imagem-download"), DisableRequestSizeLimit]
        [SwaggerResponse ((200), Type = typeof (byte[]))]
        [SwaggerResponse (400)]
        [SwaggerResponse (401)]
        [Authorize ("Bearer")]
        [TypeFilter (typeof (HyperMediaFilter))]
        public IActionResult ImagemDownload () {
            byte[] buffer = _termoDoacaoBusiness.GetImagem ();
            if (buffer != null) {
                //configuração do tipo de arquivo
                HttpContext.Response.ContentType = "image/jpg";
                HttpContext.Response.Headers.Add ("content-length", buffer.Length.ToString ());
                HttpContext.Response.Body.Write (buffer, 0, buffer.Length);
            }
            return new ContentResult ();
        }

        [HttpPost ("salvar-termo-doacao-instrumento"), DisableRequestSizeLimit]
        [SwaggerResponse ((201))]
        [SwaggerResponse (400)]
        [SwaggerResponse (401)]
        [Authorize ("Bearer")]
        [TypeFilter (typeof (HyperMediaFilter))]
        public IActionResult DoacaoTermoUpload () {
            try {
                var file = Request.Form.Files[0];
                var folderName = Path.Combine ("Resources", "Doacao", "Termos");
                var pathToSave = Path.Combine (Directory.GetCurrentDirectory (), folderName);

                if (file.Length > 0) {
                    var fileName = ContentDispositionHeaderValue.Parse (file.ContentDisposition).FileName.Trim ('"');
                    var fullPath = Path.Combine (pathToSave, fileName);
                    var dbPath = Path.Combine (folderName, fileName);

                    using (var stream = new FileStream (fullPath, FileMode.Create)) {
                        file.CopyTo (stream);
                    }

                    return Ok (new { dbPath });
                } else {
                    return BadRequest ();
                }
            } catch (Exception) {
                return StatusCode (500, "Internal server error");
            }
        }

        [HttpPost ("salvar-nota-doacao-instrumento"), DisableRequestSizeLimit]
        [SwaggerResponse ((201))]
        [SwaggerResponse (400)]
        [SwaggerResponse (401)]
        [Authorize ("Bearer")]
        [TypeFilter (typeof (HyperMediaFilter))]
        public IActionResult DoacaoNotaUpload () {
            try {
                var file = Request.Form.Files[0];
                var folderName = Path.Combine ("Resources", "Doacao", "NotasFicais");
                var pathToSave = Path.Combine (Directory.GetCurrentDirectory (), folderName);

                if (file.Length > 0) {
                    var fileName = ContentDispositionHeaderValue.Parse (file.ContentDisposition).FileName.Trim ('"');
                    var fullPath = Path.Combine (pathToSave, fileName);
                    var dbPath = Path.Combine (folderName, fileName);

                    using (var stream = new FileStream (fullPath, FileMode.Create)) {
                        file.CopyTo (stream);
                    }

                    return Ok (new { dbPath });
                } else {
                    return BadRequest ();
                }
            } catch (Exception) {
                return StatusCode (500, "Internal server error");
            }
        }

        [HttpPost ("salvar-outros-doacao-instrumento"), DisableRequestSizeLimit]
        [SwaggerResponse ((201))]
        [SwaggerResponse (400)]
        [SwaggerResponse (401)]
        [Authorize ("Bearer")]
        [TypeFilter (typeof (HyperMediaFilter))]
        public IActionResult DoacaoOutrosUpload () {
            try {
                var file = Request.Form.Files[0];
                var folderName = Path.Combine ("Resources", "Doacao", "Outros");
                var pathToSave = Path.Combine (Directory.GetCurrentDirectory (), folderName);

                if (file.Length > 0) {
                    var fileName = ContentDispositionHeaderValue.Parse (file.ContentDisposition).FileName.Trim ('"');
                    var fullPath = Path.Combine (pathToSave, fileName);
                    var dbPath = Path.Combine (folderName, fileName);

                    using (var stream = new FileStream (fullPath, FileMode.Create)) {
                        file.CopyTo (stream);
                    }

                    return Ok (new { dbPath });
                } else {
                    return BadRequest ();
                }
            } catch (Exception) {
                return StatusCode (500, "Internal server error");
            }
        }

        [HttpGet]
        [SwaggerResponse ((200), Type = typeof (List<DoacaoInstrumentoVO>))]
        [SwaggerResponse (204)]
        [SwaggerResponse (400)]
        [SwaggerResponse (401)]
        [Authorize ("Bearer")]
        [TypeFilter (typeof (HyperMediaFilter))]
        public IActionResult Get () {
            return new OkObjectResult (_doacaoinstrumentoBusiness.FindAll ());
        }

        [HttpGet ("buscar-doacao")]
        [SwaggerResponse ((200), Type = typeof (List<DoacaoInstrumentoVO>))]
        [SwaggerResponse (204)]
        [SwaggerResponse (400)]
        [SwaggerResponse (401)]
        [Authorize ("Bearer")]
        [TypeFilter (typeof (HyperMediaFilter))]
        public IActionResult GetDoacao ([FromQuery] string tombamento) {
            return new OkObjectResult (_doacaoinstrumentoBusiness.FindDoacao (tombamento));
        }

        // Configura o Swagger para a operação
        // http://localhost:{porta}/api/books/v1/{id}
        // [SwaggerResponse((202), Type = typeof(Book))]
        // determina o objeto de retorno em caso de sucesso Book
        // O [SwaggerResponse(XYZ)] define os códigos de retorno 204, 400 e 401
        [HttpGet ("{id}")]
        [SwaggerResponse ((200), Type = typeof (DoacaoInstrumentoVO))]
        [SwaggerResponse (204)]
        [SwaggerResponse (400)]
        [SwaggerResponse (401)]
        [Authorize ("Bearer")]
        [TypeFilter (typeof (HyperMediaFilter))]
        public IActionResult Get (long id) {
            var doacao = _doacaoinstrumentoBusiness.FindById (id);
            if (doacao == null) return NotFound ();
            return new OkObjectResult (doacao);
        }

        // Configura o Swagger para a operação
        // http://localhost:{porta}/api/
        // [SwaggerResponse((202), Type = typeof(Book))]
        // determina o objeto de retorno em caso de sucesso Book
        // O [SwaggerResponse(XYZ)] define os códigos de retorno 400 e 401
        [HttpPost]
        [SwaggerResponse ((201), Type = typeof (DoacaoInstrumentoVO))]
        [SwaggerResponse (400)]
        [SwaggerResponse (401)]
        [Authorize ("Bearer")]
        [TypeFilter (typeof (HyperMediaFilter))]
        public IActionResult Post ([FromBody] DoacaoInstrumentoVO doacaoVO) {
            if (doacaoVO == null) return BadRequest ();
            return new OkObjectResult (_doacaoinstrumentoBusiness.Create (doacaoVO));
        }

        // Configura o Swagger para a operação
        // http://localhost:{porta}/api/books/v1/
        // determina o objeto de retorno em caso de sucesso Book
        // O [SwaggerResponse(XYZ)] define os códigos de retorno 400 e 401
        [HttpPut]
        [SwaggerResponse ((202), Type = typeof (DoacaoInstrumentoVO))]
        [SwaggerResponse (400)]
        [SwaggerResponse (401)]
        [Authorize ("Bearer")]
        [TypeFilter (typeof (HyperMediaFilter))]
        public IActionResult Put ([FromBody] DoacaoInstrumentoVO doacaoVO) {
            if (doacaoVO == null) return BadRequest ();
            var updatedDoacao = _doacaoinstrumentoBusiness.Update (doacaoVO);
            if (updatedDoacao == null) return BadRequest ();
            return new OkObjectResult (updatedDoacao);
        }

        // Configura o Swagger para a operação
        // http://localhost:{porta}/api/books/v1/{id}
        // O [SwaggerResponse(XYZ)] define os códigos de retorno 400 e 401
        [HttpDelete ("{id}")]
        [SwaggerResponse (204)]
        [SwaggerResponse (400)]
        [SwaggerResponse (401)]
        [Authorize ("Bearer")]
        [TypeFilter (typeof (HyperMediaFilter))]
        public IActionResult Delete (int id) {
            _doacaoinstrumentoBusiness.Delete (id);
            return NoContent ();
        }
    }
}