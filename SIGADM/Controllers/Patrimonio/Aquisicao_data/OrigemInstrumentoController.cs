using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using SIGADM.Business.Interfaces.Aquisicao_data;
using SIGADM.Data.VO.Patrimonio.Aquisicao_data;
using Swashbuckle.AspNetCore.SwaggerGen;
using Tapioca.HATEOAS;


namespace SIGADM.Controllers.Patrimonio.Aquisicao_data
{   
    [ApiVersion("1")]
    [Route("api/[controller]/v{version:apiVersion}")]
    public class OrigemInstrumentoController : Controller
    {
        private IOrigemInstrumentoBusiness _origeminstrumentoBusiness;
        public OrigemInstrumentoController(IOrigemInstrumentoBusiness origeminstrumentoBusiness) 
        {
            _origeminstrumentoBusiness = origeminstrumentoBusiness;
        }

        [HttpGet]
        [SwaggerResponse((200), Type = typeof(List<OrigemInstrumentoVO>))]
        [SwaggerResponse(204)]
        [SwaggerResponse(400)]
        [SwaggerResponse(401)]
        [Authorize("Bearer")]
        [TypeFilter(typeof(HyperMediaFilter))]
        public IActionResult Get()
        {
            return new OkObjectResult(_origeminstrumentoBusiness.FindAll());
        }
        
       

        // Configura o Swagger para a operação
        // http://localhost:{porta}/api/books/v1/{id}
        // [SwaggerResponse((202), Type = typeof(Book))]
        // determina o objeto de retorno em caso de sucesso Book
        // O [SwaggerResponse(XYZ)] define os códigos de retorno 204, 400 e 401
        [HttpGet("{id}")]
        [SwaggerResponse((200), Type = typeof(OrigemInstrumentoVO))]
        [SwaggerResponse(204)]
        [SwaggerResponse(400)]
        [SwaggerResponse(401)]
        [Authorize("Bearer")]
        [TypeFilter(typeof(HyperMediaFilter))]
        public IActionResult Get(long id)
        {
            var origem = _origeminstrumentoBusiness.FindById(id);
            if (origem == null) return NotFound();
            return new OkObjectResult(origem);
        }
        
        // Configura o Swagger para a operação
        // http://localhost:{porta}/api/
        // [SwaggerResponse((202), Type = typeof(Book))]
        // determina o objeto de retorno em caso de sucesso Book
        // O [SwaggerResponse(XYZ)] define os códigos de retorno 400 e 401
        [HttpPost]
        [SwaggerResponse((201), Type = typeof(OrigemInstrumentoVO))]
        [SwaggerResponse(400)]
        [SwaggerResponse(401)]
        [Authorize("Bearer")]
        [TypeFilter(typeof(HyperMediaFilter))]
        public IActionResult Post([FromBody]OrigemInstrumentoVO origemVO)
        {
            if (origemVO == null) return BadRequest();
            return new OkObjectResult(_origeminstrumentoBusiness.Create(origemVO));
        }

        // Configura o Swagger para a operação
        // http://localhost:{porta}/api/books/v1/
        // determina o objeto de retorno em caso de sucesso Book
        // O [SwaggerResponse(XYZ)] define os códigos de retorno 400 e 401
        [HttpPut]
        [SwaggerResponse((202), Type = typeof(OrigemInstrumentoVO))]
        [SwaggerResponse(400)]
        [SwaggerResponse(401)]
        [Authorize("Bearer")]
        [TypeFilter(typeof(HyperMediaFilter))]
        public IActionResult Put([FromBody]OrigemInstrumentoVO origemVO)
        {
            if (origemVO == null) return BadRequest();
            var updatedOrigem = _origeminstrumentoBusiness.Update(origemVO);
            if (updatedOrigem == null) return BadRequest();
            return new OkObjectResult(updatedOrigem);
        }

        // Configura o Swagger para a operação
        // http://localhost:{porta}/api/books/v1/{id}
        // O [SwaggerResponse(XYZ)] define os códigos de retorno 400 e 401
        [HttpDelete("{id}")]
        [SwaggerResponse(204)]
        [SwaggerResponse(400)]
        [SwaggerResponse(401)]
        [Authorize("Bearer")]
        [TypeFilter(typeof(HyperMediaFilter))]
        public IActionResult Delete(int id)
        {
            _origeminstrumentoBusiness.Delete(id);
            return NoContent();
        }
    }
}