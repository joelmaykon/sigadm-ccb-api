using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using SIGADM.Business.Interfaces.Aquisicao_data;
using SIGADM.Data.VO.Patrimonio.Aquisicao_data;
using Swashbuckle.AspNetCore.SwaggerGen;
using Tapioca.HATEOAS;


namespace SIGADM.Controllers.Patrimonio.Aquisicao_data
{   
    [ApiVersion("1")]
    [Route("api/[controller]/v{version:apiVersion}")]
    public class NotaFiscalInstrumentoController : Controller
    {
        private INotaFiscalInstrumentoBusiness _notainstrumentoBusiness;
        public NotaFiscalInstrumentoController(INotaFiscalInstrumentoBusiness notainstrumentoBusiness) 
        {
            _notainstrumentoBusiness = notainstrumentoBusiness;
        }

        [HttpGet]
        [SwaggerResponse((200), Type = typeof(List<NotaFiscalInstrumentoVO>))]
        [SwaggerResponse(204)]
        [SwaggerResponse(400)]
        [SwaggerResponse(401)]
        [Authorize("Bearer")]
        [TypeFilter(typeof(HyperMediaFilter))]
        public IActionResult Get()
        {
            return new OkObjectResult(_notainstrumentoBusiness.FindAll());
        }
        
       

        // Configura o Swagger para a operação
        // http://localhost:{porta}/api/books/v1/{id}
        // [SwaggerResponse((202), Type = typeof(Book))]
        // determina o objeto de retorno em caso de sucesso Book
        // O [SwaggerResponse(XYZ)] define os códigos de retorno 204, 400 e 401
        [HttpGet("{id}")]
        [SwaggerResponse((200), Type = typeof(NotaFiscalInstrumentoVO))]
        [SwaggerResponse(204)]
        [SwaggerResponse(400)]
        [SwaggerResponse(401)]
        [Authorize("Bearer")]
        [TypeFilter(typeof(HyperMediaFilter))]
        public IActionResult Get(long id)
        {
            var nota = _notainstrumentoBusiness.FindById(id);
            if (nota == null) return NotFound();
            return new OkObjectResult(nota);
        }
        
        // Configura o Swagger para a operação
        // http://localhost:{porta}/api/
        // [SwaggerResponse((202), Type = typeof(Book))]
        // determina o objeto de retorno em caso de sucesso Book
        // O [SwaggerResponse(XYZ)] define os códigos de retorno 400 e 401
        [HttpPost]
        [SwaggerResponse((201), Type = typeof(NotaFiscalInstrumentoVO))]
        [SwaggerResponse(400)]
        [SwaggerResponse(401)]
        [Authorize("Bearer")]
        [TypeFilter(typeof(HyperMediaFilter))]
        public IActionResult Post([FromBody]NotaFiscalInstrumentoVO notaVO)
        {
            if (notaVO == null) return BadRequest();
            return new OkObjectResult(_notainstrumentoBusiness.Create(notaVO));
        }

        // Configura o Swagger para a operação
        // http://localhost:{porta}/api/books/v1/
        // determina o objeto de retorno em caso de sucesso Book
        // O [SwaggerResponse(XYZ)] define os códigos de retorno 400 e 401
        [HttpPut]
        [SwaggerResponse((202), Type = typeof(NotaFiscalInstrumentoVO))]
        [SwaggerResponse(400)]
        [SwaggerResponse(401)]
        [Authorize("Bearer")]
        [TypeFilter(typeof(HyperMediaFilter))]
        public IActionResult Put([FromBody]NotaFiscalInstrumentoVO notaVO)
        {
            if (notaVO == null) return BadRequest();
            var updatedNota = _notainstrumentoBusiness.Update(notaVO);
            if (updatedNota == null) return BadRequest();
            return new OkObjectResult(updatedNota);
        }

        // Configura o Swagger para a operação
        // http://localhost:{porta}/api/books/v1/{id}
        // O [SwaggerResponse(XYZ)] define os códigos de retorno 400 e 401
        [HttpDelete("{id}")]
        [SwaggerResponse(204)]
        [SwaggerResponse(400)]
        [SwaggerResponse(401)]
        [Authorize("Bearer")]
        [TypeFilter(typeof(HyperMediaFilter))]
        public IActionResult Delete(int id)
        {
            _notainstrumentoBusiness.Delete(id);
            return NoContent();
        }
    }
}