using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http.Headers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using SIGADM.Business.Interfaces.Aquisicao_data;
using SIGADM.Data.VO.Patrimonio.Aquisicao_data;
using Swashbuckle.AspNetCore.SwaggerGen;
using Tapioca.HATEOAS;


namespace SIGADM.Controllers.Patrimonio.Aquisicao_data
{   
    [ApiVersion("1")]
    [Route("api/[controller]/v{version:apiVersion}")]
    public class CompraInstrumentoController : Controller
    {
        private ICompraInstrumentoBusiness _comprainstrumentoBusiness;
        public CompraInstrumentoController(ICompraInstrumentoBusiness comprainstrumentoBusiness) 
        {
            _comprainstrumentoBusiness = comprainstrumentoBusiness;
        }
        [HttpPost ("salvar-notafiscal-instrumento"), DisableRequestSizeLimit]
        [SwaggerResponse ((201))]
        [SwaggerResponse (400)]
        [SwaggerResponse (401)]
        [Authorize ("Bearer")]
        [TypeFilter (typeof (HyperMediaFilter))]
        public IActionResult NotaFiscalUpload () {
            try {
                var file = Request.Form.Files[0];
                var folderName = Path.Combine ("Resources","Compra","NotasFiscais");
                var pathToSave = Path.Combine (Directory.GetCurrentDirectory (), folderName);

                if (file.Length > 0) {
                    var fileName = ContentDispositionHeaderValue.Parse (file.ContentDisposition).FileName.Trim ('"');
                    var fullPath = Path.Combine (pathToSave, fileName);
                    var dbPath = Path.Combine (folderName, fileName);

                    using (var stream = new FileStream (fullPath, FileMode.Create)) {
                        file.CopyTo (stream);
                    }

                    return Ok (new { dbPath });
                } else {
                    return BadRequest ();
                }
            } catch (Exception) {
                return StatusCode (500, "Internal server error");
            }
        }
        [HttpGet]
        [SwaggerResponse((200), Type = typeof(List<CompraInstrumentoVO>))]
        [SwaggerResponse(204)]
        [SwaggerResponse(400)]
        [SwaggerResponse(401)]
        [Authorize("Bearer")]
        [TypeFilter(typeof(HyperMediaFilter))]
        public IActionResult Get()
        {
            return new OkObjectResult(_comprainstrumentoBusiness.FindAll());
        }
        [HttpGet("buscar-compra")]
        [SwaggerResponse((200), Type = typeof(List<CompraInstrumentoVO>))]
        [SwaggerResponse(204)]
        [SwaggerResponse(400)]
        [SwaggerResponse(401)]
        [Authorize("Bearer")]
        [TypeFilter(typeof(HyperMediaFilter))]
        public IActionResult GetCompra([FromQuery] string tombamento)
        {
            return new OkObjectResult(_comprainstrumentoBusiness.FindCompra(tombamento));
        }


        // Configura o Swagger para a operação
        // http://localhost:{porta}/api/books/v1/{id}
        // [SwaggerResponse((202), Type = typeof(Book))]
        // determina o objeto de retorno em caso de sucesso Book
        // O [SwaggerResponse(XYZ)] define os códigos de retorno 204, 400 e 401
        [HttpGet("{id}")]
        [SwaggerResponse((200), Type = typeof(CompraInstrumentoVO))]
        [SwaggerResponse(204)]
        [SwaggerResponse(400)]
        [SwaggerResponse(401)]
        [Authorize("Bearer")]
        [TypeFilter(typeof(HyperMediaFilter))]
        public IActionResult Get(long id)
        {
            var tombo = _comprainstrumentoBusiness.FindById(id);
            if (tombo == null) return NotFound();
            return new OkObjectResult(tombo);
        }

        // Configura o Swagger para a operação
        // http://localhost:{porta}/api/
        // [SwaggerResponse((202), Type = typeof(Book))]
        // determina o objeto de retorno em caso de sucesso Book
        // O [SwaggerResponse(XYZ)] define os códigos de retorno 400 e 401
        [HttpPost]
        [SwaggerResponse((201), Type = typeof(CompraInstrumentoVO))]
        [SwaggerResponse(400)]
        [SwaggerResponse(401)]
        [Authorize("Bearer")]
        [TypeFilter(typeof(HyperMediaFilter))]
        public IActionResult Post([FromBody]CompraInstrumentoVO compraVO)
        {
            if (compraVO == null) return BadRequest();
            return new OkObjectResult(_comprainstrumentoBusiness.Create(compraVO));
        }

        // Configura o Swagger para a operação
        // http://localhost:{porta}/api/books/v1/
        // determina o objeto de retorno em caso de sucesso Book
        // O [SwaggerResponse(XYZ)] define os códigos de retorno 400 e 401
        [HttpPut]
        [SwaggerResponse((202), Type = typeof(CompraInstrumentoVO))]
        [SwaggerResponse(400)]
        [SwaggerResponse(401)]
        [Authorize("Bearer")]
        [TypeFilter(typeof(HyperMediaFilter))]
        public IActionResult Put([FromBody]CompraInstrumentoVO compraVO)
        {
            if (compraVO == null) return BadRequest();
            var updatedCompra = _comprainstrumentoBusiness.Update(compraVO);
            if (updatedCompra == null) return BadRequest();
            return new OkObjectResult(updatedCompra);
        }

        // Configura o Swagger para a operação
        // http://localhost:{porta}/api/books/v1/{id}
        // O [SwaggerResponse(XYZ)] define os códigos de retorno 400 e 401
        [HttpDelete("{id}")]
        [SwaggerResponse(204)]
        [SwaggerResponse(400)]
        [SwaggerResponse(401)]
        [Authorize("Bearer")]
        [TypeFilter(typeof(HyperMediaFilter))]
        public IActionResult Delete(int id)
        {
            _comprainstrumentoBusiness.Delete(id);
            return NoContent();
        }
    }
}