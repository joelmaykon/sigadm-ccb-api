using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http.Headers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using SIGADM.Business.Interfaces.Setorizacao.Irmaos;
using SIGADM.Data.VO.Patrimonio.Setorizacao.Irmaos;
using Swashbuckle.AspNetCore.SwaggerGen;
using Tapioca.HATEOAS;


namespace SIGADM.Controllers.Patrimonio.Setorizacao.Irmaos
{   
    [ApiVersion("1")]
    [Route("api/[controller]/v{version:apiVersion}")]
    public class UsuarioIgrejaController : Controller
    {
        private IUsuarioIgrejaBusiness _usuarioBusiness;
        public UsuarioIgrejaController(IUsuarioIgrejaBusiness usuarioBusiness) 
        {
            _usuarioBusiness = usuarioBusiness;
        }
        
        [HttpGet]
        [SwaggerResponse((200), Type = typeof(List<UsuarioIgrejaVO>))]
        [SwaggerResponse(204)]
        [SwaggerResponse(400)]
        [SwaggerResponse(401)]
        [Authorize("Bearer")]
        [TypeFilter(typeof(HyperMediaFilter))]
        public IActionResult Get()
        {
            return new OkObjectResult(_usuarioBusiness.FindAll());
        }
        
        // Configura o Swagger para a operação
        // http://localhost:{porta}/api/books/v1/{id}
        // [SwaggerResponse((202), Type = typeof(Book))]
        // determina o objeto de retorno em caso de sucesso Book
        // O [SwaggerResponse(XYZ)] define os códigos de retorno 204, 400 e 401
        [HttpGet("{id}")]
        [SwaggerResponse((200), Type = typeof(UsuarioIgrejaVO))]
        [SwaggerResponse(204)]
        [SwaggerResponse(400)]
        [SwaggerResponse(401)]
        [Authorize("Bearer")]
        [TypeFilter(typeof(HyperMediaFilter))]
        public IActionResult Get(long id)
        {
            var usuarioIgreja = _usuarioBusiness.FindById(id);
            if (usuarioIgreja == null) return NotFound();
            return new OkObjectResult(usuarioIgreja);
        }

        // Configura o Swagger para a operação
        // http://localhost:{porta}/api/books/v1/{id}
        // [SwaggerResponse((202), Type = typeof(Book))]
        // determina o objeto de retorno em caso de sucesso Book
        // O [SwaggerResponse(XYZ)] define os códigos de retorno 204, 400 e 401
        [HttpGet("usuario-por-id")]
        [SwaggerResponse((200), Type = typeof(UsuarioIgrejaVO))]
        [SwaggerResponse(204)]
        [SwaggerResponse(400)]
        [SwaggerResponse(401)]
        [Authorize("Bearer")]
        [TypeFilter(typeof(HyperMediaFilter))]
        public IActionResult GetIdUsuario(int id)
        {
            var usuarioIgreja = _usuarioBusiness.ProcurarIdUsuario(id);
            if (usuarioIgreja == null) return NotFound();
            return new OkObjectResult(usuarioIgreja);
        }

        // Configura o Swagger para a operação
        // http://localhost:{porta}/api/
        // [SwaggerResponse((202), Type = typeof(Book))]
        // determina o objeto de retorno em caso de sucesso Book
        // O [SwaggerResponse(XYZ)] define os códigos de retorno 400 e 401
        [HttpPost]
        [SwaggerResponse((201), Type = typeof(UsuarioIgrejaVO))]
        [SwaggerResponse(400)]
        [SwaggerResponse(401)]
        [Authorize("Bearer")]
        [TypeFilter(typeof(HyperMediaFilter))]
        public IActionResult Post([FromBody]UsuarioIgrejaVO usuarioIgrejaVO)
        {
            if (usuarioIgrejaVO == null) return BadRequest();
            return new OkObjectResult(_usuarioBusiness.Create(usuarioIgrejaVO));
        }

        // Configura o Swagger para a operação
        // http://localhost:{porta}/api/books/v1/
        // determina o objeto de retorno em caso de sucesso Book
        // O [SwaggerResponse(XYZ)] define os códigos de retorno 400 e 401
        [HttpPut]
        [SwaggerResponse((202), Type = typeof(UsuarioIgrejaVO))]
        [SwaggerResponse(400)]
        [SwaggerResponse(401)]
        [Authorize("Bearer")]
        [TypeFilter(typeof(HyperMediaFilter))]
        public IActionResult Put([FromBody]UsuarioIgrejaVO usuarioIgrejaVO)
        {
            if (usuarioIgrejaVO == null) return BadRequest();
            var updatedIgrejaUsuario = _usuarioBusiness.Update(usuarioIgrejaVO);
            if (updatedIgrejaUsuario == null) return BadRequest();
            return new OkObjectResult(updatedIgrejaUsuario);
        }

        // Configura o Swagger para a operação
        // http://localhost:{porta}/api/books/v1/{id}
        // O [SwaggerResponse(XYZ)] define os códigos de retorno 400 e 401
        [HttpDelete("{id}")]
        [SwaggerResponse(204)]
        [SwaggerResponse(400)]
        [SwaggerResponse(401)]
        [Authorize("Bearer")]
        [TypeFilter(typeof(HyperMediaFilter))]
        public IActionResult Delete(int id)
        {
            _usuarioBusiness.Delete(id);
            return NoContent();
        }
    }
}