using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http.Headers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using SIGADM.Business.Interfaces.Setorizacao.Irmaos;
using SIGADM.Data.VO.Patrimonio.Setorizacao.Irmaos;
using Swashbuckle.AspNetCore.SwaggerGen;
using Tapioca.HATEOAS;


namespace SIGADM.Controllers.Patrimonio.Setorizacao.Irmaos
{   
    [ApiVersion("1")]
    [Route("api/[controller]/v{version:apiVersion}")]
    public class UsuarioController : Controller
    {
        private IUsuarioBusiness _usuarioBusiness;
        public UsuarioController(IUsuarioBusiness usuarioBusiness) 
        {
            _usuarioBusiness = usuarioBusiness;
        }
        
        [HttpGet]
        [SwaggerResponse((200), Type = typeof(List<UsuarioVO>))]
        [SwaggerResponse(204)]
        [SwaggerResponse(400)]
        [SwaggerResponse(401)]
        [Authorize("Bearer")]
        [TypeFilter(typeof(HyperMediaFilter))]
        public IActionResult Get()
        {
            return new OkObjectResult(_usuarioBusiness.FindAll());
        }
        
        
        [HttpGet("{cpf}")]
        [SwaggerResponse((200), Type = typeof(UsuarioVO))]
        [SwaggerResponse(204)]
        [SwaggerResponse(400)]
        [SwaggerResponse(401)]
        [Authorize("Bearer")]
        [TypeFilter(typeof(HyperMediaFilter))]
        public IActionResult Get(string cpf)
        {
            var instrumento = _usuarioBusiness.BuscarIrmaoCPF(cpf);
            if (instrumento == null) return NotFound();
            return new OkObjectResult(instrumento);
        }[HttpGet("id")]
        [SwaggerResponse((200), Type = typeof(UsuarioVO))]
        [SwaggerResponse(204)]
        [SwaggerResponse(400)]
        [SwaggerResponse(401)]
        [Authorize("Bearer")]
        [TypeFilter(typeof(HyperMediaFilter))]
        public IActionResult GetId([FromQuery] long id)
        {
            var instrumento = _usuarioBusiness.BuscarId(id);
            if (instrumento == null) return NotFound();
            return new OkObjectResult(instrumento);
        }

        // Configura o Swagger para a operação
        // http://localhost:{porta}/api/
        // [SwaggerResponse((202), Type = typeof(Book))]
        // determina o objeto de retorno em caso de sucesso Book
        // O [SwaggerResponse(XYZ)] define os códigos de retorno 400 e 401
        [HttpPost]
        [SwaggerResponse((201), Type = typeof(UsuarioVO))]
        [SwaggerResponse(400)]
        [SwaggerResponse(401)]
        [Authorize("Bearer")]
        [TypeFilter(typeof(HyperMediaFilter))]
        public IActionResult Post([FromBody]UsuarioVO instrumentoVO)
        {
            if (instrumentoVO == null) return BadRequest();
            return new OkObjectResult(_usuarioBusiness.Create(instrumentoVO));
        }

        // Configura o Swagger para a operação
        // http://localhost:{porta}/api/books/v1/
        // determina o objeto de retorno em caso de sucesso Book
        // O [SwaggerResponse(XYZ)] define os códigos de retorno 400 e 401
        [HttpPut]
        [SwaggerResponse((202), Type = typeof(UsuarioVO))]
        [SwaggerResponse(400)]
        [SwaggerResponse(401)]
        [Authorize("Bearer")]
        [TypeFilter(typeof(HyperMediaFilter))]
        public IActionResult Put([FromBody]UsuarioVO instrumentoVO)
        {
            if (instrumentoVO == null) return BadRequest();
            var updatedInstrumento = _usuarioBusiness.Update(instrumentoVO);
            if (updatedInstrumento == null) return BadRequest();
            return new OkObjectResult(updatedInstrumento);
        }

        // Configura o Swagger para a operação
        // http://localhost:{porta}/api/books/v1/{id}
        // O [SwaggerResponse(XYZ)] define os códigos de retorno 400 e 401
        [HttpDelete("{id}")]
        [SwaggerResponse(204)]
        [SwaggerResponse(400)]
        [SwaggerResponse(401)]
        [Authorize("Bearer")]
        [TypeFilter(typeof(HyperMediaFilter))]
        public IActionResult Delete(int id)
        {
            _usuarioBusiness.Delete(id);
            return NoContent();
        }
    }
}