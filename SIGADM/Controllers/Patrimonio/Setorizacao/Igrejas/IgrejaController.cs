using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using SIGADM.Business.Interfaces.Setorizacao.Igrejas;
using SIGADM.Data.VO.Patrimonio.Setorizacao.Igrejas;
using Swashbuckle.AspNetCore.SwaggerGen;
using Tapioca.HATEOAS;

namespace SIGADM.Controllers.Patrimonio.Setorizacao.Igrejas {
    [ApiVersion ("1")]
    [Route ("api/[controller]/v{version:apiVersion}")]
    public class IgrejaController : Controller {
        private IIgrejaBusiness _igrejaBusiness;
        public IgrejaController (IIgrejaBusiness igrejaBusiness) {
            _igrejaBusiness = igrejaBusiness;
        }

        [HttpGet]
        [SwaggerResponse ((200), Type = typeof (List<IgrejaVO>))]
        [SwaggerResponse (204)]
        [SwaggerResponse (400)]
        [SwaggerResponse (401)]
        [Authorize ("Bearer")]
        [TypeFilter (typeof (HyperMediaFilter))]
        public IActionResult Get () {
            return new OkObjectResult (_igrejaBusiness.FindAll());
        }

        // Configura o Swagger para a operação
        // http://localhost:{porta}/api/books/v1/{id}
        // [SwaggerResponse((202), Type = typeof(Book))]
        // determina o objeto de retorno em caso de sucesso Book
        // O [SwaggerResponse(XYZ)] define os códigos de retorno 204, 400 e 401
        [HttpGet ("{id}")]
        [SwaggerResponse ((200), Type = typeof (IgrejaVO))]
        [SwaggerResponse (204)]
        [SwaggerResponse (400)]
        [SwaggerResponse (401)]
        [Authorize ("Bearer")]
        [TypeFilter (typeof (HyperMediaFilter))]
        public IActionResult Get (long id) {
            var igreja = _igrejaBusiness.FindById (id);
            if (igreja == null) return NotFound ();
            return new OkObjectResult (igreja);
        }

        // Configura o Swagger para a operação
        // http://localhost:{porta}/api/
        // [SwaggerResponse((202), Type = typeof(Book))]
        // determina o objeto de retorno em caso de sucesso Book
        // O [SwaggerResponse(XYZ)] define os códigos de retorno 400 e 401
        [HttpPost]
        [SwaggerResponse ((201), Type = typeof (IgrejaVO))]
        [SwaggerResponse (400)]
        [SwaggerResponse (401)]
        [Authorize ("Bearer")]
        [TypeFilter (typeof (HyperMediaFilter))]
        public IActionResult Post ([FromBody] IgrejaVO igrejaVO) {
            if (igrejaVO == null) return BadRequest ();
            return new OkObjectResult (_igrejaBusiness.Create (igrejaVO));
        }

        // Configura o Swagger para a operação
        // http://localhost:{porta}/api/books/v1/
        // determina o objeto de retorno em caso de sucesso Book
        // O [SwaggerResponse(XYZ)] define os códigos de retorno 400 e 401
        [HttpPut]
        [SwaggerResponse ((202), Type = typeof (IgrejaVO))]
        [SwaggerResponse (400)]
        [SwaggerResponse (401)]
        [Authorize ("Bearer")]
        [TypeFilter (typeof (HyperMediaFilter))]
        public IActionResult Put ([FromBody] IgrejaVO igrejaVO) {
            if (igrejaVO == null) return BadRequest ();
            var updatedIgreja = _igrejaBusiness.Update (igrejaVO);
            if (updatedIgreja == null) return BadRequest ();
            return new OkObjectResult (updatedIgreja);
        }

        // Configura o Swagger para a operação
        // http://localhost:{porta}/api/books/v1/{id}
        // O [SwaggerResponse(XYZ)] define os códigos de retorno 400 e 401
        [HttpDelete ("{id}")]
        [SwaggerResponse (204)]
        [SwaggerResponse (400)]
        [SwaggerResponse (401)]
        [Authorize ("Bearer")]
        [TypeFilter (typeof (HyperMediaFilter))]
        public IActionResult Delete (int id) {
            _igrejaBusiness.Delete (id);
            return NoContent ();
        }

    }
}