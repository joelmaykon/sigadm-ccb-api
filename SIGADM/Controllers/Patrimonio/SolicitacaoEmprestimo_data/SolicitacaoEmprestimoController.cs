using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using SIGADM.Business.Interfaces.Devolucao_data;
using SIGADM.Business.Interfaces.SolicitacaoEmprestimo_data;
using SIGADM.Data.VO.Patrimonio.Devolucao_data;
using SIGADM.Data.VO.Patrimonio.SolicitacaoEmprestimo_data;
using Swashbuckle.AspNetCore.SwaggerGen;
using Tapioca.HATEOAS;


namespace SIGADM.Controllers.Patrimonio.SolicitacaoEmprestimo_data
{   
    [ApiVersion("1")]
    [Route("api/[controller]/v{version:apiVersion}")]
    public class SolicitacaoEmprestimoController : Controller
    {
        private ISolicitacaoEmprestimoBusiness _solicitacaoEmprestimoBusiness;
        public SolicitacaoEmprestimoController(ISolicitacaoEmprestimoBusiness solicitacaoEmprestimoBusiness) 
        {
            _solicitacaoEmprestimoBusiness = solicitacaoEmprestimoBusiness;
        }

        [HttpGet]
        [SwaggerResponse(204)]
        [SwaggerResponse(400)]
        [SwaggerResponse(401)]
        [Authorize("Bearer")]
        [TypeFilter(typeof(HyperMediaFilter))]
        public IActionResult Get()
        {
            return new OkObjectResult(_solicitacaoEmprestimoBusiness.FindAll());
        }        
        // Configura o Swagger para a operação
        // http://localhost:{porta}/api/books/v1/{id}
        // [SwaggerResponse((202), Type = typeof(Book))]
        // determina o objeto de retorno em caso de sucesso Book
        // O [SwaggerResponse(XYZ)] define os códigos de retorno 204, 400 e 401
        [HttpGet("{id}")]
        [SwaggerResponse((200), Type = typeof(SolicitacaoEmprestimoVO))]
        [SwaggerResponse(204)]
        [SwaggerResponse(400)]
        [SwaggerResponse(401)]
        [Authorize("Bearer")]
        [TypeFilter(typeof(HyperMediaFilter))]
        public IActionResult Get(long id)
        {
            var solicitacaoEmprestimo = _solicitacaoEmprestimoBusiness.FindById(id);
            if (solicitacaoEmprestimo == null) return NotFound();
            return new OkObjectResult(solicitacaoEmprestimo);
        }

        // Configura o Swagger para a operação
        // http://localhost:{porta}/api/
        // [SwaggerResponse((202), Type = typeof(Book))]
        // determina o objeto de retorno em caso de sucesso Book
        // O [SwaggerResponse(XYZ)] define os códigos de retorno 400 e 401
        [HttpPost]
        [SwaggerResponse((201), Type = typeof(SolicitacaoEmprestimoVO))]
        [SwaggerResponse(400)]
        [SwaggerResponse(401)]
        [Authorize("Bearer")]
        [TypeFilter(typeof(HyperMediaFilter))]
        public IActionResult Post([FromBody]SolicitacaoEmprestimoVO solicitacaoEmprestimoVO)
        {
            if (solicitacaoEmprestimoVO == null) return BadRequest();
            return new OkObjectResult(_solicitacaoEmprestimoBusiness.Create(solicitacaoEmprestimoVO));
        }

        // Configura o Swagger para a operação
        // http://localhost:{porta}/api/books/v1/
        // determina o objeto de retorno em caso de sucesso Book
        // O [SwaggerResponse(XYZ)] define os códigos de retorno 400 e 401
        [HttpPut]
        [SwaggerResponse((202), Type = typeof(SolicitacaoEmprestimoVO))]
        [SwaggerResponse(400)]
        [SwaggerResponse(401)]
        [Authorize("Bearer")]
        [TypeFilter(typeof(HyperMediaFilter))]
        public IActionResult Put([FromBody]SolicitacaoEmprestimoVO solicitacaoEmprestimoVO)
        {
            if (solicitacaoEmprestimoVO == null) return BadRequest();
            var updateSolicitacaoEmprestimo = _solicitacaoEmprestimoBusiness.Update(solicitacaoEmprestimoVO);
            if (updateSolicitacaoEmprestimo == null) return BadRequest();
            return new OkObjectResult(updateSolicitacaoEmprestimo);
        }

        // Configura o Swagger para a operação
        // http://localhost:{porta}/api/books/v1/{id}
        // O [SwaggerResponse(XYZ)] define os códigos de retorno 400 e 401
        [HttpDelete("{id}")]
        [SwaggerResponse(204)]
        [SwaggerResponse(400)]
        [SwaggerResponse(401)]
        [Authorize("Bearer")]
        [TypeFilter(typeof(HyperMediaFilter))]
        public IActionResult Delete(int id)
        {
            _solicitacaoEmprestimoBusiness.Delete(id);
            return NoContent();
        }
    }
}