using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http.Headers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using SIGADM.Business.Interfaces.Instrumento_data;
using SIGADM.Data.VO.Patrimonio.Instrumento_data;
using Swashbuckle.AspNetCore.SwaggerGen;
using Tapioca.HATEOAS;

namespace SIGADM.Controllers.Patrimonio.Instrumento_data {
    [ApiVersion ("1")]
    [Route ("api/[controller]/v{version:apiVersion}")]
    public class InstrumentoController : Controller {
        private I_instrumentoBusiness _instrumentoBusiness;
        public InstrumentoController (I_instrumentoBusiness instrumentoBusiness) {
            _instrumentoBusiness = instrumentoBusiness;
        }

        

        

        [HttpGet]
        [SwaggerResponse ((200), Type = typeof (List<InstrumentoVO>))]
        [SwaggerResponse (204)]
        [SwaggerResponse (400)]
        [SwaggerResponse (401)]
        [Authorize ("Bearer")]
        [TypeFilter (typeof (HyperMediaFilter))]
        public IActionResult Get () {
            return new OkObjectResult (_instrumentoBusiness.FindAll ());
        }
        [HttpGet("instrumentos")]
        [SwaggerResponse ((200), Type = typeof (List<InstrumentoVO>))]
        [SwaggerResponse (204)]
        [SwaggerResponse (400)]
        [SwaggerResponse (401)]
        [Authorize ("Bearer")]                  
        [TypeFilter (typeof (HyperMediaFilter))]
        public IActionResult GetInstrumentosNomes ([FromQuery] string nome) {
            return new OkObjectResult (_instrumentoBusiness.BuscarInstrumentosNomes (nome));
        }

        // Configura o Swagger para a operação
        // http://localhost:{porta}/api/books/v1/{id}
        // [SwaggerResponse((202), Type = typeof(Book))]
        // determina o objeto de retorno em caso de sucesso Book
        // O [SwaggerResponse(XYZ)] define os códigos de retorno 204, 400 e 401
        [HttpGet ("{id}")]
        [SwaggerResponse ((200), Type = typeof (InstrumentoVO))]
        [SwaggerResponse (204)]
        [SwaggerResponse (400)]
        [SwaggerResponse (401)]
        [Authorize ("Bearer")]
        [TypeFilter (typeof (HyperMediaFilter))]
        public IActionResult Get (long id) {
            var instrumento = _instrumentoBusiness.FindById (id);
            if (instrumento == null) return NotFound ();
            return new OkObjectResult (instrumento);
        }

        // Configura o Swagger para a operação
        // http://localhost:{porta}/api/
        // [SwaggerResponse((202), Type = typeof(Book))]
        // determina o objeto de retorno em caso de sucesso Book
        // O [SwaggerResponse(XYZ)] define os códigos de retorno 400 e 401
        [HttpPost]
        [SwaggerResponse ((201), Type = typeof (InstrumentoVO))]
        [SwaggerResponse (400)]
        [SwaggerResponse (401)]
        [Authorize ("Bearer")]
        [TypeFilter (typeof (HyperMediaFilter))]
        public IActionResult Post ([FromBody] InstrumentoVO instrumentoVO) {
            if (instrumentoVO == null) return BadRequest ();
            return new OkObjectResult (_instrumentoBusiness.Create (instrumentoVO));
        }

        // Configura o Swagger para a operação
        // http://localhost:{porta}/api/books/v1/
        // determina o objeto de retorno em caso de sucesso Book
        // O [SwaggerResponse(XYZ)] define os códigos de retorno 400 e 401
        [HttpPut]
        [SwaggerResponse ((202), Type = typeof (InstrumentoVO))]
        [SwaggerResponse (400)]
        [SwaggerResponse (401)]
        [Authorize ("Bearer")]
        [TypeFilter (typeof (HyperMediaFilter))]
        public IActionResult Put ([FromBody] InstrumentoVO instrumentoVO) {
            if (instrumentoVO == null) return BadRequest ();
            var updatedInstrumento = _instrumentoBusiness.Update (instrumentoVO);
            if (updatedInstrumento == null) return BadRequest ();
            return new OkObjectResult (updatedInstrumento);
        }

        // Configura o Swagger para a operação
        // http://localhost:{porta}/api/books/v1/{id}
        // O [SwaggerResponse(XYZ)] define os códigos de retorno 400 e 401
        [HttpDelete ("{id}")]
        [SwaggerResponse (204)]
        [SwaggerResponse (400)]
        [SwaggerResponse (401)]
        [Authorize ("Bearer")]
        [TypeFilter (typeof (HyperMediaFilter))]
        public IActionResult Delete (int id) {
            _instrumentoBusiness.Delete (id);
            return NoContent ();
        }
    }
}