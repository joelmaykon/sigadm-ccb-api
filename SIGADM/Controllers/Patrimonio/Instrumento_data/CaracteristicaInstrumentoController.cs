using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using SIGADM.Business.Interfaces.Instrumento_data;
using SIGADM.Data.Converters.Patrimonio.Instrumento_data;
using SIGADM.Data.VO.Patrimonio.Instrumento_data;
using SIGADM.Repository.Generic;
using Swashbuckle.AspNetCore.SwaggerGen;
using Tapioca.HATEOAS;


namespace SIGADM.Controllers.Patrimonio.Instrumento_data
{   
    [ApiVersion("1")]
    [Route("api/[controller]/v{version:apiVersion}")]
    public class CaracteristicaInstrumentoController : Controller
    {
       private ICaracteristicaInstrumentoBusiness _caracteristicainstrumentoBusiness;
        public CaracteristicaInstrumentoController(ICaracteristicaInstrumentoBusiness caracteristicainstrumentoBusiness) 
        {
            _caracteristicainstrumentoBusiness = caracteristicainstrumentoBusiness;
        }

        [HttpGet]
        [SwaggerResponse((200), Type = typeof(List<CaracteristicaInstrumentoVO>))]
        [SwaggerResponse(204)]
        [SwaggerResponse(400)]
        [SwaggerResponse(401)]
        [Authorize("Bearer")]
        [TypeFilter(typeof(HyperMediaFilter))]
        public IActionResult Get()
        {
            return new OkObjectResult(_caracteristicainstrumentoBusiness.FindAll());
        }

        // Configura o Swagger para a operação
        // http://localhost:{porta}/api/books/v1/{id}
        // [SwaggerResponse((202), Type = typeof(Book))]
        // determina o objeto de retorno em caso de sucesso Book
        // O [SwaggerResponse(XYZ)] define os códigos de retorno 204, 400 e 401
        [HttpGet("{id}")]
        [SwaggerResponse((200), Type = typeof(CaracteristicaInstrumentoVO))]
        [SwaggerResponse(204)]
        [SwaggerResponse(400)]
        [SwaggerResponse(401)]
        [Authorize("Bearer")]
        [TypeFilter(typeof(HyperMediaFilter))]
        public IActionResult Get(long id)
        {
            var nome = _caracteristicainstrumentoBusiness.FindById(id);
            if (nome == null) return NotFound();
            return new OkObjectResult(nome);
        }
        [HttpGet("buscar-caracteristica-por-naipe")]
        [SwaggerResponse((200), Type = typeof(List<CaracteristicaInstrumentoVO>))]
        [SwaggerResponse(204)]
        [SwaggerResponse(400)]
        [SwaggerResponse(401)]
        [Authorize("Bearer")]
        [TypeFilter(typeof(HyperMediaFilter))]
        public IActionResult GetCaracteristicaNaipe([FromQuery] string naipe)
        {
            return new OkObjectResult(_caracteristicainstrumentoBusiness.FindCaracteristicaNaipe(naipe));
        }
        // Configura o Swagger para a operação
        // http://localhost:{porta}/api/
        // [SwaggerResponse((202), Type = typeof(Book))]
        // determina o objeto de retorno em caso de sucesso Book
        // O [SwaggerResponse(XYZ)] define os códigos de retorno 400 e 401
        [HttpPost]
        [SwaggerResponse((201), Type = typeof(CaracteristicaInstrumentoVO))]
        [SwaggerResponse(400)]
        [SwaggerResponse(401)]
        [Authorize("Bearer")]
        [TypeFilter(typeof(HyperMediaFilter))]
        public IActionResult Post([FromBody]CaracteristicaInstrumentoVO instrumentoVO)
        {
            if (instrumentoVO == null) return BadRequest();
            return new OkObjectResult(_caracteristicainstrumentoBusiness.Create(instrumentoVO));
        }

        // Configura o Swagger para a operação
        // http://localhost:{porta}/api/books/v1/
        // determina o objeto de retorno em caso de sucesso Book
        // O [SwaggerResponse(XYZ)] define os códigos de retorno 400 e 401
        [HttpPut]
        [SwaggerResponse((202), Type = typeof(CaracteristicaInstrumentoVO))]
        [SwaggerResponse(400)]
        [SwaggerResponse(401)]
        [Authorize("Bearer")]
        [TypeFilter(typeof(HyperMediaFilter))]
        public IActionResult Put([FromBody]CaracteristicaInstrumentoVO instrumentoVO)
        {
            if (instrumentoVO == null) return BadRequest();
            var updatedNome = _caracteristicainstrumentoBusiness.Update(instrumentoVO);
            if (updatedNome == null) return BadRequest();
            return new OkObjectResult(updatedNome);
        }

        // Configura o Swagger para a operação
        // http://localhost:{porta}/api/books/v1/{id}
        // O [SwaggerResponse(XYZ)] define os códigos de retorno 400 e 401
        [HttpDelete("{id}")]
        [SwaggerResponse(204)]
        [SwaggerResponse(400)]
        [SwaggerResponse(401)]
        [Authorize("Bearer")]
        [TypeFilter(typeof(HyperMediaFilter))]
        public IActionResult Delete(int id)
        {
            _caracteristicainstrumentoBusiness.Delete(id);
            return NoContent();
        }
         [HttpDelete ("deletar-por-caracteristica")]
        [SwaggerResponse ((200), Type = typeof (List<CaracteristicaInstrumentoVO>))]
        [SwaggerResponse (204)]
        [SwaggerResponse (400)]
        [SwaggerResponse (401)]
        [Authorize ("Bearer")]
        [TypeFilter (typeof (HyperMediaFilter))]
        public IActionResult DeleteNome ([FromQuery] string caracteristica) {
            _caracteristicainstrumentoBusiness.DeleteCaracteristica(caracteristica);
            return NoContent ();
        }
       
    }
}