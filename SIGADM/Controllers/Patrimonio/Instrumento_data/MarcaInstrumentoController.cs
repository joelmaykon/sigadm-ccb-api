using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using SIGADM.Business.Interfaces.Instrumento_data;
using SIGADM.Data.VO.Patrimonio.Instrumento_data;
using Swashbuckle.AspNetCore.SwaggerGen;
using Tapioca.HATEOAS;


namespace SIGADM.Controllers.Patrimonio.Instrumento_data
{   
    [ApiVersion("1")]
    [Route("api/[controller]/v{version:apiVersion}")]
    public class MarcaInstrumentoController : Controller
    {
        private IMarcaInstrumentoBusiness _marcainstrumentoBusiness;
        public MarcaInstrumentoController(IMarcaInstrumentoBusiness marcainstrumentoBusiness) 
        {
            _marcainstrumentoBusiness = marcainstrumentoBusiness;
        }

        [HttpGet]
        [SwaggerResponse((200), Type = typeof(List<MarcaInstrumentoVO>))]
        [SwaggerResponse(204)]
        [SwaggerResponse(400)]
        [SwaggerResponse(401)]
        [Authorize("Bearer")]
        [TypeFilter(typeof(HyperMediaFilter))]
        public IActionResult Get()
        {
            return new OkObjectResult(_marcainstrumentoBusiness.FindAll());
        }

        // Configura o Swagger para a operação
        // http://localhost:{porta}/api/books/v1/{id}
        // [SwaggerResponse((202), Type = typeof(Book))]
        // determina o objeto de retorno em caso de sucesso Book
        // O [SwaggerResponse(XYZ)] define os códigos de retorno 204, 400 e 401
        [HttpGet("{id}")]
        [SwaggerResponse((200), Type = typeof(MarcaInstrumentoVO))]
        [SwaggerResponse(204)]
        [SwaggerResponse(400)]
        [SwaggerResponse(401)]
        [Authorize("Bearer")]
        [TypeFilter(typeof(HyperMediaFilter))]
        public IActionResult Get(long id)
        {
            var marca = _marcainstrumentoBusiness.FindById(id);
            if (marca == null) return NotFound();
            return new OkObjectResult(marca);
        }

        // Configura o Swagger para a operação
        // http://localhost:{porta}/api/
        // [SwaggerResponse((202), Type = typeof(Book))]
        // determina o objeto de retorno em caso de sucesso Book
        // O [SwaggerResponse(XYZ)] define os códigos de retorno 400 e 401
        [HttpPost]
        [SwaggerResponse((201), Type = typeof(MarcaInstrumentoVO))]
        [SwaggerResponse(400)]
        [SwaggerResponse(401)]
        [Authorize("Bearer")]
        [TypeFilter(typeof(HyperMediaFilter))]
        public IActionResult Post([FromBody]MarcaInstrumentoVO marcaVO)
        {
            if (marcaVO == null) return BadRequest();
            return new OkObjectResult(_marcainstrumentoBusiness.Create(marcaVO));
        }

        // Configura o Swagger para a operação
        // http://localhost:{porta}/api/books/v1/
        // determina o objeto de retorno em caso de sucesso Book
        // O [SwaggerResponse(XYZ)] define os códigos de retorno 400 e 401
        [HttpPut]
        [SwaggerResponse((202), Type = typeof(MarcaInstrumentoVO))]
        [SwaggerResponse(400)]
        [SwaggerResponse(401)]
        [Authorize("Bearer")]
        [TypeFilter(typeof(HyperMediaFilter))]
        public IActionResult Put([FromBody]MarcaInstrumentoVO marcaVO)
        {
            if (marcaVO == null) return BadRequest();
            var updatedMarca = _marcainstrumentoBusiness.Update(marcaVO);
            if (updatedMarca == null) return BadRequest();
            return new OkObjectResult(updatedMarca);
        }

        // Configura o Swagger para a operação
        // http://localhost:{porta}/api/books/v1/{id}
        // O [SwaggerResponse(XYZ)] define os códigos de retorno 400 e 401
        [HttpDelete("{id}")]
        [SwaggerResponse(204)]
        [SwaggerResponse(400)]
        [SwaggerResponse(401)]
        [Authorize("Bearer")]
        [TypeFilter(typeof(HyperMediaFilter))]
        public IActionResult Delete(int id)
        {
            _marcainstrumentoBusiness.Delete(id);
            return NoContent();
        }
        [HttpDelete ("deletar-por-marca")]
        [SwaggerResponse ((200), Type = typeof (List<MarcaInstrumentoVO>))]
        [SwaggerResponse (204)]
        [SwaggerResponse (400)]
        [SwaggerResponse (401)]
        [Authorize ("Bearer")]
        [TypeFilter (typeof (HyperMediaFilter))]
        public IActionResult DeleteMarca ([FromQuery] string marca) {
            _marcainstrumentoBusiness.DeleteMarca(marca);
            return NoContent ();
        }
    }
}