using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using SIGADM.Business.Interfaces.Instrumento_data;
using SIGADM.Data.VO.Patrimonio.Instrumento_data;
using Swashbuckle.AspNetCore.SwaggerGen;
using Tapioca.HATEOAS;

namespace SIGADM.Controllers.Patrimonio.Instrumento_data {
    [ApiVersion ("1")]
    [Route ("api/[controller]/v{version:apiVersion}")]
    public class NomeInstrumentoController : Controller {
        private INomeInstrumentoBusiness _nomeinstrumentoBusiness;
        public NomeInstrumentoController (INomeInstrumentoBusiness nomeinstrumentoBusiness) {
            _nomeinstrumentoBusiness = nomeinstrumentoBusiness;
        }

        [HttpGet]
        [SwaggerResponse ((200), Type = typeof (List<NomeInstrumentoVO>))]
        [SwaggerResponse (204)]
        [SwaggerResponse (400)]
        [SwaggerResponse (401)]
        [Authorize ("Bearer")]
        [TypeFilter (typeof (HyperMediaFilter))]
        public IActionResult Get () {
            return new OkObjectResult (_nomeinstrumentoBusiness.FindAll ());
        }

        // Configura o Swagger para a operação
        // http://localhost:{porta}/api/books/v1/{id}
        // [SwaggerResponse((202), Type = typeof(Book))]
        // determina o objeto de retorno em caso de sucesso Book
        // O [SwaggerResponse(XYZ)] define os códigos de retorno 204, 400 e 401
        [HttpGet ("{id}")]
        [SwaggerResponse ((200), Type = typeof (NomeInstrumentoVO))]
        [SwaggerResponse (204)]
        [SwaggerResponse (400)]
        [SwaggerResponse (401)]
        [Authorize ("Bearer")]
        [TypeFilter (typeof (HyperMediaFilter))]
        public IActionResult Get (long id) {
            var nome = _nomeinstrumentoBusiness.FindById (id);
            if (nome == null) return NotFound ();
            return new OkObjectResult (nome);
        }

        // Configura o Swagger para a operação
        // http://localhost:{porta}/api/
        // [SwaggerResponse((202), Type = typeof(Book))]
        // determina o objeto de retorno em caso de sucesso Book
        // O [SwaggerResponse(XYZ)] define os códigos de retorno 400 e 401
        [HttpPost]
        [SwaggerResponse ((201), Type = typeof (NomeInstrumentoVO))]
        [SwaggerResponse (400)]
        [SwaggerResponse (401)]
        [Authorize ("Bearer")]
        [TypeFilter (typeof (HyperMediaFilter))]
        public IActionResult Post ([FromBody] NomeInstrumentoVO instrumentoVO) {
            if (instrumentoVO == null) return BadRequest ();
            return new OkObjectResult (_nomeinstrumentoBusiness.Create (instrumentoVO));
        }

        // Configura o Swagger para a operação
        // http://localhost:{porta}/api/books/v1/
        // determina o objeto de retorno em caso de sucesso Book
        // O [SwaggerResponse(XYZ)] define os códigos de retorno 400 e 401
        [HttpPut]
        [SwaggerResponse ((202), Type = typeof (NomeInstrumentoVO))]
        [SwaggerResponse (400)]
        [SwaggerResponse (401)]
        [Authorize ("Bearer")]
        [TypeFilter (typeof (HyperMediaFilter))]
        public IActionResult Put ([FromBody] NomeInstrumentoVO instrumentoVO) {
            if (instrumentoVO == null) return BadRequest ();
            var updatedNome = _nomeinstrumentoBusiness.Update (instrumentoVO);
            if (updatedNome == null) return BadRequest ();
            return new OkObjectResult (updatedNome);
        }

        // Configura o Swagger para a operação
        // http://localhost:{porta}/api/books/v1/{id}
        // O [SwaggerResponse(XYZ)] define os códigos de retorno 400 e 401
        [HttpDelete ("{id}")]
        [SwaggerResponse (204)]
        [SwaggerResponse (400)]
        [SwaggerResponse (401)]
        [Authorize ("Bearer")]
        [TypeFilter (typeof (HyperMediaFilter))]
        public IActionResult Delete (int id) {
            _nomeinstrumentoBusiness.Delete (id);
            return NoContent ();
        }

        [HttpDelete ("deletar-por-nome")]
        [SwaggerResponse ((200), Type = typeof (List<CaracteristicaInstrumentoVO>))]
        [SwaggerResponse (204)]
        [SwaggerResponse (400)]
        [SwaggerResponse (401)]
        [Authorize ("Bearer")]
        [TypeFilter (typeof (HyperMediaFilter))]
        public IActionResult DeleteNome ([FromQuery] string nome) {
            _nomeinstrumentoBusiness.DeleteNome (nome);
            return NoContent ();
        }

    }
}