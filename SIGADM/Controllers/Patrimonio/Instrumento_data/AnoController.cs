using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using SIGADM.Business.Interfaces.Instrumento_data;
using SIGADM.Data.VO.Patrimonio.Instrumento_data;
using Swashbuckle.AspNetCore.SwaggerGen;
using Tapioca.HATEOAS;


namespace SIGADM.Controllers.Patrimonio.Instrumento_data
{   
    [ApiVersion("1")]
    [Route("api/[controller]/v{version:apiVersion}")]
    public class AnoController : Controller
    {
        private IAnoBusiness _anoinstrumentoBusiness;
        public AnoController(IAnoBusiness anoinstrumentoBusiness) 
        {
            _anoinstrumentoBusiness = anoinstrumentoBusiness;
        }

        [HttpGet]
        [SwaggerResponse((200), Type = typeof(List<AnoVO>))]
        [SwaggerResponse(204)]
        [SwaggerResponse(400)]
        [SwaggerResponse(401)]
        [Authorize("Bearer")]
        [TypeFilter(typeof(HyperMediaFilter))]
        public IActionResult Get()
        {
            return new OkObjectResult(_anoinstrumentoBusiness.FindAll());
        }

        // Configura o Swagger para a operação
        // http://localhost:{porta}/api/books/v1/{id}
        // [SwaggerResponse((202), Type = typeof(Book))]
        // determina o objeto de retorno em caso de sucesso Book
        // O [SwaggerResponse(XYZ)] define os códigos de retorno 204, 400 e 401
        [HttpGet("{id}")]
        [SwaggerResponse((200), Type = typeof(AnoVO))]
        [SwaggerResponse(204)]
        [SwaggerResponse(400)]
        [SwaggerResponse(401)]
        [Authorize("Bearer")]
        [TypeFilter(typeof(HyperMediaFilter))]
        public IActionResult Get(long id)
        {
            var ano = _anoinstrumentoBusiness.FindById(id);
            if (ano == null) return NotFound();
            return new OkObjectResult(ano);
        }

        // Configura o Swagger para a operação
        // http://localhost:{porta}/api/
        // [SwaggerResponse((202), Type = typeof(Book))]
        // determina o objeto de retorno em caso de sucesso Book
        // O [SwaggerResponse(XYZ)] define os códigos de retorno 400 e 401
        [HttpPost]
        [SwaggerResponse((201), Type = typeof(AnoVO))]
        [SwaggerResponse(400)]
        [SwaggerResponse(401)]
        [Authorize("Bearer")]
        [TypeFilter(typeof(HyperMediaFilter))]
        public IActionResult Post([FromBody]AnoVO anoVO)
        {
            if (anoVO == null) return BadRequest();
            return new OkObjectResult(_anoinstrumentoBusiness.Create(anoVO));
        }

        // Configura o Swagger para a operação
        // http://localhost:{porta}/api/books/v1/
        // determina o objeto de retorno em caso de sucesso Book
        // O [SwaggerResponse(XYZ)] define os códigos de retorno 400 e 401
        [HttpPut]
        [SwaggerResponse((202), Type = typeof(AnoVO))]
        [SwaggerResponse(400)]
        [SwaggerResponse(401)]
        [Authorize("Bearer")]
        [TypeFilter(typeof(HyperMediaFilter))]
        public IActionResult Put([FromBody]AnoVO anoVO)
        {
            if (anoVO == null) return BadRequest();
            var updatedAno = _anoinstrumentoBusiness.Update(anoVO);
            if (updatedAno == null) return BadRequest();
            return new OkObjectResult(updatedAno);
        }

        // Configura o Swagger para a operação
        // http://localhost:{porta}/api/books/v1/{id}
        // O [SwaggerResponse(XYZ)] define os códigos de retorno 400 e 401
        [HttpDelete("{id}")]
        [SwaggerResponse(204)]
        [SwaggerResponse(400)]
        [SwaggerResponse(401)]
        [Authorize("Bearer")]
        [TypeFilter(typeof(HyperMediaFilter))]
        public IActionResult Delete(int id)
        {
            _anoinstrumentoBusiness.Delete(id);
            return NoContent();
        }
        [HttpDelete ("deletar-por-ano")]
        [SwaggerResponse ((200), Type = typeof (List<AnoVO>))]
        [SwaggerResponse (204)]
        [SwaggerResponse (400)]
        [SwaggerResponse (401)]
        [Authorize ("Bearer")]
        [TypeFilter (typeof (HyperMediaFilter))]
        public IActionResult DeleteNome ([FromQuery] string ano) {
            _anoinstrumentoBusiness.DeleteAno(ano);
            return NoContent ();
        }
    }
}