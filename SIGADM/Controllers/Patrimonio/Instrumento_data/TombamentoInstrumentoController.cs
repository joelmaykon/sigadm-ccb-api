using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using SIGADM.Business.Interfaces.Instrumento_data;
using SIGADM.Data.VO.Patrimonio.Instrumento_data;
using Swashbuckle.AspNetCore.SwaggerGen;
using Tapioca.HATEOAS;


namespace SIGADM.Controllers.Patrimonio.Instrumento_data
{   
    [ApiVersion("1")]
    [Route("api/[controller]/v{version:apiVersion}")]
    public class TombamentoInstrumentoController : Controller
    {
        private ITombamentoInstrumentoBusiness _tombamentoinstrumentoBusiness;
        public TombamentoInstrumentoController(ITombamentoInstrumentoBusiness tombamentoinstrumentoBusiness) 
        {
            _tombamentoinstrumentoBusiness = tombamentoinstrumentoBusiness;
        }

        [HttpGet]
        [SwaggerResponse((200), Type = typeof(List<TombamentoInstrumentoVO>))]
        [SwaggerResponse(204)]
        [SwaggerResponse(400)]
        [SwaggerResponse(401)]
        [Authorize("Bearer")]
        [TypeFilter(typeof(HyperMediaFilter))]
        public IActionResult Get()
        {
            return new OkObjectResult(_tombamentoinstrumentoBusiness.FindAll());
        }

        // Configura o Swagger para a operação
        // http://localhost:{porta}/api/books/v1/{id}
        // [SwaggerResponse((202), Type = typeof(Book))]
        // determina o objeto de retorno em caso de sucesso Book
        // O [SwaggerResponse(XYZ)] define os códigos de retorno 204, 400 e 401
        [HttpGet("{id}")]
        [SwaggerResponse((200), Type = typeof(TombamentoInstrumentoVO))]
        [SwaggerResponse(204)]
        [SwaggerResponse(400)]
        [SwaggerResponse(401)]
        [Authorize("Bearer")]
        [TypeFilter(typeof(HyperMediaFilter))]
        public IActionResult Get(long id)
        {
            var tombo = _tombamentoinstrumentoBusiness.FindById(id);
            if (tombo == null) return NotFound();
            return new OkObjectResult(tombo);
        }
        [HttpGet("buscar-tombamento")]
        [SwaggerResponse((200), Type = typeof(TombamentoInstrumentoVO))]
        [SwaggerResponse(204)]
        [SwaggerResponse(400)]
        [SwaggerResponse(401)]
        [Authorize("Bearer")]
        [TypeFilter(typeof(HyperMediaFilter))]
        public IActionResult GetTombamento(string tombamento)
        {
            var tombo = _tombamentoinstrumentoBusiness.FindByTombamento(tombamento);
            if (tombo == null) return NotFound();
            return new OkObjectResult(tombo);
        }

        // Configura o Swagger para a operação
        // http://localhost:{porta}/api/
        // [SwaggerResponse((202), Type = typeof(Book))]
        // determina o objeto de retorno em caso de sucesso Book
        // O [SwaggerResponse(XYZ)] define os códigos de retorno 400 e 401
        [HttpPost]
        [SwaggerResponse((201), Type = typeof(TombamentoInstrumentoVO))]
        [SwaggerResponse(400)]
        [SwaggerResponse(401)]
        [Authorize("Bearer")]
        [TypeFilter(typeof(HyperMediaFilter))]
        public IActionResult Post([FromBody]TombamentoInstrumentoVO instrumentoVO)
        {
            if (instrumentoVO == null) return BadRequest();
            return new OkObjectResult(_tombamentoinstrumentoBusiness.Create(instrumentoVO));
        }

        // Configura o Swagger para a operação
        // http://localhost:{porta}/api/books/v1/
        // determina o objeto de retorno em caso de sucesso Book
        // O [SwaggerResponse(XYZ)] define os códigos de retorno 400 e 401
        [HttpPut]
        [SwaggerResponse((202), Type = typeof(TombamentoInstrumentoVO))]
        [SwaggerResponse(400)]
        [SwaggerResponse(401)]
        [Authorize("Bearer")]
        [TypeFilter(typeof(HyperMediaFilter))]
        public IActionResult Put([FromBody]TombamentoInstrumentoVO tombamentoVO)
        {
            if (tombamentoVO == null) return BadRequest();
            var updatedTombo = _tombamentoinstrumentoBusiness.Update(tombamentoVO);
            if (updatedTombo == null) return BadRequest();
            return new OkObjectResult(updatedTombo);
        }
        [HttpPut("selecionar-tombamento")]
        [SwaggerResponse((202), Type = typeof(TombamentoInstrumentoVO))]
        [SwaggerResponse(400)]
        [SwaggerResponse(401)]
        [Authorize("Bearer")]
        [TypeFilter(typeof(HyperMediaFilter))]
        public IActionResult PutTombamento([FromQuery] string tombamento)
        {
            if (tombamento == null) return BadRequest();
            var updatedTombo = _tombamentoinstrumentoBusiness.SelecionarTombamento(tombamento);
            if (updatedTombo == null) return BadRequest();
            return new OkObjectResult(updatedTombo);
        }
        
        // Configura o Swagger para a operação
        // http://localhost:{porta}/api/books/v1/{id}
        // O [SwaggerResponse(XYZ)] define os códigos de retorno 400 e 401
        [HttpDelete("{id}")]
        [SwaggerResponse(204)]
        [SwaggerResponse(400)]
        [SwaggerResponse(401)]
        [Authorize("Bearer")]
        [TypeFilter(typeof(HyperMediaFilter))]
        public IActionResult Delete(int id)
        {
            _tombamentoinstrumentoBusiness.Delete(id);
            return NoContent();
        }
    }
}