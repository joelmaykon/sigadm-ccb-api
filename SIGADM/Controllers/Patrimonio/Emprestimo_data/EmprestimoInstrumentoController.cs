using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http.Headers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using SIGADM.Business.Interfaces.Emprestimo_data;
using SIGADM.Data.VO.Patrimonio.Emprestimo_data;
using Swashbuckle.AspNetCore.SwaggerGen;
using Tapioca.HATEOAS;


namespace SIGADM.Controllers.Patrimonio.Emprestimo_data
{   
    [ApiVersion("1")]
    [Route("api/[controller]/v{version:apiVersion}")]
    public class EmprestimoInstrumentoController : Controller
    {
        private IEmprestimoInstrumentoBusiness _instrumentoBusiness;
        public EmprestimoInstrumentoController(IEmprestimoInstrumentoBusiness instrumentoBusiness) 
        {
            _instrumentoBusiness = instrumentoBusiness;
        }
        [HttpPost ("salvar-responsabilidade-emprestimo"), DisableRequestSizeLimit]
        [SwaggerResponse ((201))]
        [SwaggerResponse (400)]
        [SwaggerResponse (401)]
        [Authorize ("Bearer")]
        [TypeFilter (typeof (HyperMediaFilter))]
        public IActionResult ResponsabilidadeUpload () {
            try {
                var file = Request.Form.Files[0];
                var folderName = Path.Combine ("Resources","Emprestimo","TermoResponsabilidade");
                var pathToSave = Path.Combine (Directory.GetCurrentDirectory (), folderName);

                if (file.Length > 0) {
                    var fileName = ContentDispositionHeaderValue.Parse (file.ContentDisposition).FileName.Trim ('"');
                    var fullPath = Path.Combine (pathToSave, fileName);
                    var dbPath = Path.Combine (folderName, fileName);

                    using (var stream = new FileStream (fullPath, FileMode.Create)) {
                        file.CopyTo (stream);
                    }

                    return Ok (new { dbPath });
                } else {
                    return BadRequest ();
                }
            } catch (Exception) {
                return StatusCode (500, "Internal server error");
            }
        }
        [HttpPost ("salvar-ciencia-emprestimo"), DisableRequestSizeLimit]
        [SwaggerResponse ((201))]
        [SwaggerResponse (400)]
        [SwaggerResponse (401)]
        [Authorize ("Bearer")]
        [TypeFilter (typeof (HyperMediaFilter))]
        public IActionResult CienciaUpload () {
            try {
                var file = Request.Form.Files[0];
                var folderName = Path.Combine ("Resources","Emprestimo","TermoCiencia");
                var pathToSave = Path.Combine (Directory.GetCurrentDirectory (), folderName);

                if (file.Length > 0) {
                    var fileName = ContentDispositionHeaderValue.Parse (file.ContentDisposition).FileName.Trim ('"');
                    var fullPath = Path.Combine (pathToSave, fileName);
                    var dbPath = Path.Combine (folderName, fileName);

                    using (var stream = new FileStream (fullPath, FileMode.Create)) {
                        file.CopyTo (stream);
                    }

                    return Ok (new { dbPath });
                } else {
                    return BadRequest ();
                }
            } catch (Exception) {
                return StatusCode (500, "Internal server error");
            }
        }

        [HttpGet]
        [SwaggerResponse((200), Type = typeof(List<EmprestimoInstrumentoVO>))]
        [SwaggerResponse(204)]
        [SwaggerResponse(400)]
        [SwaggerResponse(401)]
        [Authorize("Bearer")]
        [TypeFilter(typeof(HyperMediaFilter))]
        public IActionResult Get()
        {
            return new OkObjectResult(_instrumentoBusiness.FindAll());
        }
        [HttpGet("buscar-emprestimo")]
        [SwaggerResponse((200), Type = typeof(List<EmprestimoInstrumentoVO>))]
        [SwaggerResponse(204)]
        [SwaggerResponse(400)]
        [SwaggerResponse(401)]
        [Authorize("Bearer")]
        [TypeFilter(typeof(HyperMediaFilter))]
        public IActionResult GetDoacao([FromQuery] string tombamento)
        {
            return new OkObjectResult(_instrumentoBusiness.FindEmprestimo(tombamento));
        }
        // Configura o Swagger para a operação
        // http://localhost:{porta}/api/books/v1/{id}
        // [SwaggerResponse((202), Type = typeof(Book))]
        // determina o objeto de retorno em caso de sucesso Book
        // O [SwaggerResponse(XYZ)] define os códigos de retorno 204, 400 e 401
        [HttpGet("{id}")]
        [SwaggerResponse((200), Type = typeof(EmprestimoInstrumentoVO))]
        [SwaggerResponse(204)]
        [SwaggerResponse(400)]
        [SwaggerResponse(401)]
        [Authorize("Bearer")]
        [TypeFilter(typeof(HyperMediaFilter))]
        public IActionResult Get(long id)
        {
            var instrumento = _instrumentoBusiness.FindById(id);
            if (instrumento == null) return NotFound();
            return new OkObjectResult(instrumento);
        }

        // Configura o Swagger para a operação
        // http://localhost:{porta}/api/
        // [SwaggerResponse((202), Type = typeof(Book))]
        // determina o objeto de retorno em caso de sucesso Book
        // O [SwaggerResponse(XYZ)] define os códigos de retorno 400 e 401
        [HttpPost]
        [SwaggerResponse((201), Type = typeof(EmprestimoInstrumentoVO))]
        [SwaggerResponse(400)]
        [SwaggerResponse(401)]
        [Authorize("Bearer")]
        [TypeFilter(typeof(HyperMediaFilter))]
        public IActionResult Post([FromBody]EmprestimoInstrumentoVO instrumentoVO)
        {
            if (instrumentoVO == null) return BadRequest();
            return new OkObjectResult(_instrumentoBusiness.Create(instrumentoVO));
        }

        // Configura o Swagger para a operação
        // http://localhost:{porta}/api/books/v1/
        // determina o objeto de retorno em caso de sucesso Book
        // O [SwaggerResponse(XYZ)] define os códigos de retorno 400 e 401
        [HttpPut]
        [SwaggerResponse((202), Type = typeof(EmprestimoInstrumentoVO))]
        [SwaggerResponse(400)]
        [SwaggerResponse(401)]
        [Authorize("Bearer")]
        [TypeFilter(typeof(HyperMediaFilter))]
        public IActionResult Put([FromBody]EmprestimoInstrumentoVO instrumentoVO)
        {
            if (instrumentoVO == null) return BadRequest();
            var updatedInstrumento = _instrumentoBusiness.Update(instrumentoVO);
            if (updatedInstrumento == null) return BadRequest();
            return new OkObjectResult(updatedInstrumento);
        }

        // Configura o Swagger para a operação
        // http://localhost:{porta}/api/books/v1/{id}
        // O [SwaggerResponse(XYZ)] define os códigos de retorno 400 e 401
        [HttpDelete("{id}")]
        [SwaggerResponse(204)]
        [SwaggerResponse(400)]
        [SwaggerResponse(401)]
        [Authorize("Bearer")]
        [TypeFilter(typeof(HyperMediaFilter))]
        public IActionResult Delete(int id)
        {
            _instrumentoBusiness.Delete(id);
            return NoContent();
        }
    }
}