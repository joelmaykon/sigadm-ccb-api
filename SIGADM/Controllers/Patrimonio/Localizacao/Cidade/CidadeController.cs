using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using SIGADM.Business.Interfaces.Localizacao.Cidade;
using SIGADM.Data.VO.Patrimonio.Localizacao.Cidade;
using Swashbuckle.AspNetCore.SwaggerGen;
using Tapioca.HATEOAS;

namespace SIGADM.Controllers.Patrimonio.Localizacao.Cidade {
    [ApiVersion ("1")]
    [Route ("api/[controller]/v{version:apiVersion}")]
    public class CidadeController : Controller {
        private ICidadeBusiness _cidadeBusiness;
        public CidadeController (ICidadeBusiness cidadeBusiness) {
            _cidadeBusiness = cidadeBusiness;
        }

        [HttpGet]
        [SwaggerResponse ((200), Type = typeof (List<CidadeVO>))]
        [SwaggerResponse (204)]
        [SwaggerResponse (400)]
        [SwaggerResponse (401)]
        [Authorize ("Bearer")]
        [TypeFilter (typeof (HyperMediaFilter))]
        public IActionResult Get () {
            return new OkObjectResult (_cidadeBusiness.FindAll ());
        }

        // Configura o Swagger para a operação
        // http://localhost:{porta}/api/books/v1/{id}
        // [SwaggerResponse((202), Type = typeof(Book))]
        // determina o objeto de retorno em caso de sucesso Book
        // O [SwaggerResponse(XYZ)] define os códigos de retorno 204, 400 e 401
        [HttpGet ("{id}")]
        [SwaggerResponse ((200), Type = typeof (CidadeVO))]
        [SwaggerResponse (204)]
        [SwaggerResponse (400)]
        [SwaggerResponse (401)]
        [Authorize ("Bearer")]
        [TypeFilter (typeof (HyperMediaFilter))]
        public IActionResult Get (long id) {
            var cidade = _cidadeBusiness.FindById (id);
            if (cidade == null) return NotFound ();
            return new OkObjectResult (cidade);
        }

        // Configura o Swagger para a operação
        // http://localhost:{porta}/api/
        // [SwaggerResponse((202), Type = typeof(Book))]
        // determina o objeto de retorno em caso de sucesso Book
        // O [SwaggerResponse(XYZ)] define os códigos de retorno 400 e 401
        [HttpPost]
        [SwaggerResponse ((201), Type = typeof (CidadeVO))]
        [SwaggerResponse (400)]
        [SwaggerResponse (401)]
        [Authorize ("Bearer")]
        [TypeFilter (typeof (HyperMediaFilter))]
        public IActionResult Post ([FromBody] CidadeVO cidadeVO) {
            if (cidadeVO == null) return BadRequest ();
            return new OkObjectResult (_cidadeBusiness.Create (cidadeVO));
        }

        // Configura o Swagger para a operação
        // http://localhost:{porta}/api/books/v1/
        // determina o objeto de retorno em caso de sucesso Book
        // O [SwaggerResponse(XYZ)] define os códigos de retorno 400 e 401
        [HttpPut]
        [SwaggerResponse ((202), Type = typeof (CidadeVO))]
        [SwaggerResponse (400)]
        [SwaggerResponse (401)]
        [Authorize ("Bearer")]
        [TypeFilter (typeof (HyperMediaFilter))]
        public IActionResult Put ([FromBody] CidadeVO cidadeVO) {
            if (cidadeVO == null) return BadRequest ();
            var updatedCidade = _cidadeBusiness.Update (cidadeVO);
            if (updatedCidade == null) return BadRequest ();
            return new OkObjectResult (updatedCidade);
        }

        // Configura o Swagger para a operação
        // http://localhost:{porta}/api/books/v1/{id}
        // O [SwaggerResponse(XYZ)] define os códigos de retorno 400 e 401
        [HttpDelete ("{id}")]
        [SwaggerResponse (204)]
        [SwaggerResponse (400)]
        [SwaggerResponse (401)]
        [Authorize ("Bearer")]
        [TypeFilter (typeof (HyperMediaFilter))]
        public IActionResult Delete (int id) {
            _cidadeBusiness.Delete (id);
            return NoContent ();
        }

       

    }
}