using System;
using System.ComponentModel.DataAnnotations.Schema;
using SIGADM.Model.Base;

namespace SIGADM.Model.Patrimonio.SolicitacaoEmprestimo_data
{
    [Table("DeliberacaoEmprestimo")]
    public class DeliberacaoEmprestimo : BaseEntity
    {
        [Column("Tipo")]
        public string Tipo { get; set; }        
        
        [Column("DataCadastro")]
        public DateTime DataCadastro { get; set; }
        [Column("LaunchDate")]
        public DateTime LaunchDate { get; set; }
    }
}