using System;
using System.ComponentModel.DataAnnotations.Schema;
using SIGADM.Model.Base;

namespace SIGADM.Model.Patrimonio.SolicitacaoEmprestimo_data
{
    [Table("SolicitacaoEmprestimo")]
    public class SolicitacaoEmprestimo : BaseEntity
    {
        [Column("Nome")]
        public string Nome { get; set; }
        [Column("Irmao")]
        public int Irmao {get;set;}
        [Column("ComumCongregacao")]
        public string ComumCongregacao { get; set; }
        [Column("PrimeiroInstrumento")]
        public string PrimeiroInstrumento { get; set; }
        [Column("PrimeiraCategoria")]
        public string PrimeiraCategoria { get; set; }
        [Column("SegundoInstrumento")]
        public string SegundoInstrumento { get; set; }
        [Column("SegundaCategoria")]
        public string SegundaCategoria { get; set; }
        [Column("DataCadastro")]
        public DateTime DataCadastro { get; set; }
        [Column("Status")]
        public string Status { get; set; }
        [Column("LaunchDate")]
        public DateTime LaunchDate { get; set; }
    }
}