using System;
using System.ComponentModel.DataAnnotations.Schema;
using SIGADM.Model.Base;

namespace SIGADM.Model.Patrimonio.Empresa_data {
    [Table ("Empresa")]
    public class Empresa : BaseEntity {
        [Column ("Nome")]
        public string Nome { get; set; }
       

        [Column ("LaunchDate")]
        public DateTime LaunchDate { get; set; }
    }
}