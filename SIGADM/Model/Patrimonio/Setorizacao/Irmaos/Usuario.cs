using System;
using System.ComponentModel.DataAnnotations.Schema;
using SIGADM.Model.Base;

namespace SIGADM.Model.Patrimonio.Setorizacao.Irmaos {
    [Table ("Usuario")]
    public class Usuario : BaseEntity {

        [Column ("CPF")]
        public string CPF { get; set; }

        [Column ("Nascimento")]
        public DateTime Nascimento { get; set; }

        [Column ("Nome")]
        public string Nome { get; set; }

        [Column ("Cep")]
        public string Cep { get; set; }

        [Column ("Cidade")]
        public string Cidade { get; set; }

        [Column ("Bairro")]
        public string Bairro { get; set; }

        [Column ("Rua")]
        public string Rua { get; set; }

        [Column ("Numero")]
        public string Numero { get; set; }

        [Column ("Complemento")]
        public string Complemento { get; set; }

        [Column ("DDD")]
        public string DDD { get; set; }

        [Column ("Telefone")]
        public string Telefone { get; set; }

        [Column ("Cargo")]
        public string Cargo { get; set; }

        [Column ("LaunchDate")]
        public DateTime LaunchDate { get; set; }
    }
}