using System;
using System.ComponentModel.DataAnnotations.Schema;
using SIGADM.Model.Base;

namespace SIGADM.Model.Patrimonio.Setorizacao.Irmaos {
    [Table ("UsuarioIgreja")]
    public class UsuarioIgreja : BaseEntity {

        [Column ("IdIgreja")]
        public int IdIgreja { get; set; }

        [Column ("IdUsuario")]
        public int IdUsuario { get; set; }        

        [Column ("LaunchDate")]
        public DateTime LaunchDate { get; set; }
    }
}