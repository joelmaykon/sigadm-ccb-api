using System;
using System.ComponentModel.DataAnnotations.Schema;
using SIGADM.Model.Base;

namespace SIGADM.Model.Patrimonio.Setorizacao.Igrejas {
    [Table ("Igreja")]
    public class Igreja : BaseEntity {
        [Column ("Codigo")]
        public string Codigo { get; set; }
        [Column ("Cidade")]
        public string Cidade { get; set; } 
        [Column ("Bairro")]
        public string Bairro { get; set; } 
        [Column ("Endereco")]
        public string Endereco { get; set; }
        [Column ("Numero")]
        public string Numero { get; set; }
        [Column ("NomeRelatorio")]
        public string NomeRelatorio { get; set; }    
        [Column ("CEP")]
        public string CEP { get; set; }
        [Column ("EncRegional")]
        public string EncRegional { get; set; }
        [Column ("EncLocal")]
        public string EncLocal { get; set; }
        [Column ("LaunchDate")]
        public DateTime LaunchDate { get; set; }
    }
}