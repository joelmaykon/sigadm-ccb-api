using System;
using System.ComponentModel.DataAnnotations.Schema;
using SIGADM.Model.Base;
namespace SIGADM.Model.Patrimonio.Instrumento_data {
    [Table ("TombamentoInstrumento")]
    public class TombamentoInstrumento : BaseEntity {
        [Column ("Numero")]
        public string Numero { get; set; }
       [Column ("Status")]
        public string Status { get; set; }
        [Column ("LaunchDate")]
        public DateTime LaunchDate { get; set; }
    }
}