using System;
using System.ComponentModel.DataAnnotations.Schema;
using SIGADM.Model.Base;
namespace SIGADM.Model.Patrimonio.Instrumento_data {
    [Table ("ComponenteInstrumento")]
    public class ComponenteInstrumento : BaseEntity {
        [Column ("Componente")]
        public string Componente { get; set; }

        [Column ("LaunchDate")]
        public DateTime LaunchDate { get; set; }
    }
}