using System;
using System.ComponentModel.DataAnnotations.Schema;
using SIGADM.Model.Base;
namespace SIGADM.Model.Patrimonio.Instrumento_data {
    [Table ("MarcaInstrumento")]
    public class MarcaInstrumento : BaseEntity {
        [Column ("Marca")]
        public string Marca { get; set; }

        [Column ("LaunchDate")]
        public DateTime LaunchDate { get; set; }
    }
}