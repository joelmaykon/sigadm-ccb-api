using System;
using System.ComponentModel.DataAnnotations.Schema;
using SIGADM.Model.Base;

namespace SIGADM.Model.Patrimonio.Instrumento_data {
    [Table ("NomeInstrumento")]
    public class NomeInstrumento : BaseEntity {
        [Column ("Nome")]
        public string Nome { get; set; }

        [Column ("Naipe")]
        public string Naipe { get; set; }

        [Column ("LaunchDate")]
        public DateTime LaunchDate { get; set; }
    }
}