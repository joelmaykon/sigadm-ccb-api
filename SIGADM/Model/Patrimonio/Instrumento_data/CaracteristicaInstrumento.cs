using System;
using System.ComponentModel.DataAnnotations.Schema;
using SIGADM.Model.Base;
namespace SIGADM.Model.Patrimonio.Instrumento_data {
    [Table ("CaracteristicaInstrumento")]
    public class CaracteristicaInstrumento : BaseEntity {
        [Column ("Caracteristica")]
        public string Caracteristica { get; set; }
        [Column ("Naipe")]
        public string Naipe { get; set; }
        [Column ("LaunchDate")]
        public DateTime LaunchDate { get; set; }
    }
}