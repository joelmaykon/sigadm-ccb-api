using System;
using System.ComponentModel.DataAnnotations.Schema;
using SIGADM.Model.Base;

namespace SIGADM.Model.Patrimonio.Instrumento_data {
    [Table ("Instrumento")]
    public class Instrumento : BaseEntity {
        [Column ("Nome")]
        public string Nome { get; set; }

        [Column ("Status")]
        public string Status { get; set; }
        [Column ("Aquisicao")]
        public string Aquisicao { get; set; }

        [Column ("Ano")]
        public string Ano { get; set; }

        [Column ("Tombamento")]
        public string Tombamento { get; set; }

        [Column ("Caracteristica")]
        public string Caracteristica { get; set; }

        [Column ("Marca")]
        public string Marca { get; set; }

        [Column ("Descricao")]
        public string Descricao { get; set; }

        [Column ("Componentes")]
        public string Componentes { get; set; }

        [Column ("LaunchDate")]
        public DateTime LaunchDate { get; set; }
    }
}