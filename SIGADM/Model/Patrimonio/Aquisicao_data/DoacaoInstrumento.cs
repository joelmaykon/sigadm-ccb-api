using System;
using System.ComponentModel.DataAnnotations.Schema;
using SIGADM.Model.Base;
namespace SIGADM.Model.Patrimonio.Aquisicao_data {
    [Table ("DoacaoInstrumento")]
    public class DoacaoInstrumento : BaseEntity {
        [Column ("Tombamento")]
        public string Tombamento { get; set; }

        [Column ("Origem")]
        public string Origem { get; set; }

        [Column ("Observacao")]
        public string Observacao { get; set; }

        [Column ("DataDoacao")]
        public DateTime DataDoacao { get; set; }

        [Column ("LaunchDate")]
        public DateTime LaunchDate { get; set; }
    }
}