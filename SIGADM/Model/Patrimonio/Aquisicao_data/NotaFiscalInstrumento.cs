using System;
using System.ComponentModel.DataAnnotations.Schema;
using SIGADM.Model.Base;

namespace SIGADM.Model.Patrimonio.Aquisicao_data {
    [Table ("NotaFiscalInstrumento")]
    public class NotaFiscalInstrumento : BaseEntity {
        [Column ("Tombamento")]        
        public string Tombamento { get; set; }
         [Column ("Caminho")]        
        public string Caminho { get; set; } 
        [Column ("LaunchDate")]
        public DateTime LaunchDate { get; set; }
    }
}