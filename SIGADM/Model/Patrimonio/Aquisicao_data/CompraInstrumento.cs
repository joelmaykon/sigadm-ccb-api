using System;
using System.ComponentModel.DataAnnotations.Schema;
using SIGADM.Model.Base;
namespace SIGADM.Model.Patrimonio.Aquisicao_data {
    [Table ("CompraInstrumento")]
    public class CompraInstrumento : BaseEntity {
        [Column ("Tombamento")]
        public string Tombamento { get; set; }

        [Column ("Valor")]
        public string Valor { get; set; }

        [Column ("NotaFiscal")]
        public string NotaFiscal { get; set; }

        [Column ("Empresa")]
        public string Empresa { get; set; }

        [Column ("DataCompra")]
        public DateTime DataCompra { get; set; }

        [Column ("LaunchDate")]
        public DateTime LaunchDate { get; set; }
    }
}