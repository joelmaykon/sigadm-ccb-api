using System;
using System.ComponentModel.DataAnnotations.Schema;
using SIGADM.Model.Base;

namespace SIGADM.Model.Patrimonio.Aquisicao_data {
    [Table ("OrigemInstrumento")]
    public class OrigemInstrumento : BaseEntity {
        [Column ("Doador")]
        public string Doador { get; set; }
        [Column ("Estado")]
        public string Estado { get; set; }
        [Column ("Cidade")]
        public string Cidade { get; set; }

        [Column ("LaunchDate")]
        public DateTime LaunchDate { get; set; }
    }
}