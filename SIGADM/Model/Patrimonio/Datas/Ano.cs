using System;
using System.ComponentModel.DataAnnotations.Schema;
using SIGADM.Model.Base;

namespace SIGADM.Model.Patrimonio.Datas
{
    [Table("Ano")]
    public class Ano : BaseEntity
    {
        [Column("NumeroAno")]
        public string NumeroAno { get; set; }
        

        [Column("LaunchDate")]
        public DateTime LaunchDate { get; set; }
    }
}