using System;
using System.ComponentModel.DataAnnotations.Schema;
using SIGADM.Model.Base;

namespace SIGADM.Model.Patrimonio.Emprestimo_data {
    [Table ("EmprestimoInstrumento")]
    public class EmprestimoInstrumento : BaseEntity {
        [Column ("IdInstrumento")]
        public long? IdInstrumento { get; set; }

        [Column ("Nome")]
        public string Nome { get; set; }

        [Column ("Congregacao")]
        public string Congregacao { get; set; }

        [Column ("Tombamento")]
        public string Tombamento { get; set; }

        [Column ("Status")]
        public string Status { get; set; }

        [Column ("Encarregado")]
        public string Encarregado { get; set; }

        [Column ("EncarregadoRegional")]
        public string EncarregadoRegional { get; set; }

        [Column ("DevolucaoDate")]
        public DateTime DevolucaoDate { get; set; }

        [Column ("EmprestimoDate")]
        public DateTime EmprestimoDate { get; set; }

        [Column ("LaunchDate")]
        public DateTime LaunchDate { get; set; }
    }
}