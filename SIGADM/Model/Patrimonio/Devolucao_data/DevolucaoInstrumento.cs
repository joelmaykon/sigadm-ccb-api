using System;
using System.ComponentModel.DataAnnotations.Schema;
using SIGADM.Model.Base;

namespace SIGADM.Model.Patrimonio.Devolucao_data {
    [Table ("DevolucaoInstrumento")]
    public class DevolucaoInstrumento : BaseEntity {
        [Column ("IdInstrumento")]
        public long? IdInstrumento { get; set; }
         [Column ("IdEmprestimo")]
        public long? IdEmprestimo { get; set; }
        [Column ("Tombamento")]
        public string Tombamento { get; set; }
        
        [Column ("Status")]
        public string Status { get; set; }
        [Column ("Observacao")]
        public string Observacao { get; set; }
        
        [Column ("DevolucaoDate")]
        public DateTime DevolucaoDate { get; set; }

        [Column ("LaunchDate")]
        public DateTime LaunchDate { get; set; }
    }
}