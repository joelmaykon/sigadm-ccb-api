using System;
using System.ComponentModel.DataAnnotations.Schema;
using SIGADM.Model.Base;

namespace SIGADM.Model.Patrimonio.Localizacao.Estado {
    [Table ("Estado")]
    public class Estado : BaseEntity {
        [Column ("Nome")]
        public string Nome { get; set; }
        

        [Column ("LaunchDate")]
        public DateTime LaunchDate { get; set; }
    }
}