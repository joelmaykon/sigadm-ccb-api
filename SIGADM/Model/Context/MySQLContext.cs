﻿using Microsoft.EntityFrameworkCore;
using SIGADM.Model.Patrimonio;
using SIGADM.Model.Patrimonio.Aquisicao_data;
using SIGADM.Model.Patrimonio.Datas;
using SIGADM.Model.Patrimonio.Devolucao_data;
using SIGADM.Model.Patrimonio.Empresa_data;
using SIGADM.Model.Patrimonio.Emprestimo_data;
using SIGADM.Model.Patrimonio.Instrumento_data;
using SIGADM.Model.Patrimonio.Localizacao.Cidade;
using SIGADM.Model.Patrimonio.Localizacao.Estado;
using SIGADM.Model.Patrimonio.Setorizacao.Igrejas;
using SIGADM.Model.Patrimonio.Setorizacao.Irmaos;
using SIGADM.Model.Patrimonio.SolicitacaoEmprestimo_data;

namespace SIGADM.Model.Context {
    public class MySQLContext : DbContext {
        public MySQLContext () {

        }

        public MySQLContext (DbContextOptions<MySQLContext> options) : base (options) { }

        public DbSet<Instrumento> Instrumentos { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<NomeInstrumento> NomesInstrumentos { get; set; }
        public DbSet<Ano> Anos { get; set; }
        public DbSet<TombamentoInstrumento> Tombamentos { get; set; }
        public DbSet<CaracteristicaInstrumento> Caracteristicas { get; set; }
        public DbSet<MarcaInstrumento> Marcas { get; set; }
        public DbSet<ComponenteInstrumento> Componentes { get; set; }
        public DbSet<CompraInstrumento> CompraInstrumentos { get; set; }
        public DbSet<NotaFiscalInstrumento> NotaFiscalInstrumentos { get; set; }
        public DbSet<DoacaoInstrumento> DoacaoInstrumentos { get; set; }

        public DbSet<EmprestimoInstrumento> EmprestimoInstrumentos { get; set; }
        public DbSet<DevolucaoInstrumento> DevolucaoInstrumentos { get; set; }
        public DbSet<Empresa> Empresas { get; set; }

        public DbSet<Cidade> Cidades { get; set; }
        public DbSet<Estado> Estados { get; set; }
        public DbSet<OrigemInstrumento> Origens { get; set; }
        public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<UsuarioIgreja> UsuariosIgrejas { get; set; }
        public DbSet<Igreja> Igrejas { get; set; }
        public DbSet<SolicitacaoEmprestimo> SolicitacoesEmprestimo { get; set; }
         public DbSet<DeliberacaoEmprestimo> DeliberacoesEmprestimo { get; set; }
      
    }
}