﻿using SIGADM.Model;
using System.Collections.Generic;

namespace SIGADM.Repository
{
    public interface IUserRepository
    {
        User FindByLogin(string login);
    }
}
