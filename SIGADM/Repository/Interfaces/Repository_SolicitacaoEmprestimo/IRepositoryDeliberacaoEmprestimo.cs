﻿using System.Collections.Generic;
using SIGADM.Model.Patrimonio.SolicitacaoEmprestimo_data;
using SIGADM.Repository.Generic;

namespace SIGADM.Repository.Repository_SolicitacaoEmprestimo
{
    public interface IRepositoryDeliberacaoEmprestimo : IRepository<DeliberacaoEmprestimo> { 
        
    }
}