﻿using SIGADM.Model.Patrimonio.Aquisicao_data;
using SIGADM.Repository.Generic;
using System.Collections.Generic;

namespace SIGADM.Repository.Interfaces
{
    public interface IRepositoryNotaFiscal : IRepository<NotaFiscalInstrumento>
    {
       
        
    }
}
