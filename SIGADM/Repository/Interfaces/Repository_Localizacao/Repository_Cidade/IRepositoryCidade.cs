﻿using System.Collections.Generic;
using SIGADM.Model.Patrimonio.Localizacao.Cidade;
using SIGADM.Repository.Generic;

namespace SIGADM.Repository.Repository_Localizacao.Repository_Cidade {
    public interface IRepositoryCidade : IRepository<Cidade> {


    }
}