﻿using System.Collections.Generic;
using SIGADM.Model.Patrimonio.Localizacao.Estado;
using SIGADM.Repository.Generic;

namespace SIGADM.Repository.Repository_Localizacao.Repository_Estado {
    public interface IRepositoryEstado : IRepository<Estado> {


    }
}