﻿using System.Collections.Generic;
using SIGADM.Model.Patrimonio.Datas;
using SIGADM.Model.Patrimonio.Instrumento_data;
using SIGADM.Repository.Generic;

namespace SIGADM.Repository.Interfaces.Repository_Instrumento {
    public interface IRepositoryCaracteristicaInstrumento : IRepository<CaracteristicaInstrumento> {

        void DeleteCaracteristica (string caracteristica);
        List<CaracteristicaInstrumento> FindCaracteristicaNaipe (string naipe);

    }
}