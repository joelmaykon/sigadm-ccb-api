﻿using System.Collections.Generic;
using SIGADM.Model.Patrimonio.Instrumento_data;
using SIGADM.Repository.Generic;

namespace SIGADM.Repository.Interfaces.Repository_Instrumento {
    public interface IRepositoryMarcaInstrumento : IRepository<MarcaInstrumento> {

         void DeleteMarca(string marca);

    }
}