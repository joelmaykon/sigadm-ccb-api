﻿using System.Collections.Generic;
using SIGADM.Model.Patrimonio.Instrumento_data;
using SIGADM.Repository.Generic;

namespace SIGADM.Repository.Interfaces.Repository_Instrumento {
    public interface IRepositoryNomeInstrumento : IRepository<NomeInstrumento> {

         void DeleteNome(string nome);

    }
}