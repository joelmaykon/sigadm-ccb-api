﻿using System.Collections.Generic;
using SIGADM.Model.Patrimonio.Datas;
using SIGADM.Repository.Generic;

namespace SIGADM.Repository.Interfaces {
    public interface IRepositoryAnoInstrumento : IRepository<Ano> {

         void DeleteAno(string ano);

    }
}