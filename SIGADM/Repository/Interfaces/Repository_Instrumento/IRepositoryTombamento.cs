﻿using System.Collections.Generic;
using SIGADM.Model.Patrimonio.Aquisicao_data;
using SIGADM.Model.Patrimonio.Instrumento_data;
using SIGADM.Repository.Generic;

namespace SIGADM.Repository.Interfaces.Repository_Instrumento {
    public interface IRepositoryTombamento : IRepository<TombamentoInstrumento> {

        TombamentoInstrumento SelecionarTombamento (string tombamento);
        TombamentoInstrumento FindByTombamento(string tombamento);
    }
}