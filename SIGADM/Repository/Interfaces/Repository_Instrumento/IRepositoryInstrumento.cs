using System;
using System.Collections.Generic;
using SIGADM.Model.Patrimonio.Instrumento_data;

namespace SIGADM.Repository.Interfaces.Repository_Instrumento
{
    public interface IRepositoryInstrumento
    {
        List<Instrumento> FindInstrumentosNome (string nome);
    }
}

