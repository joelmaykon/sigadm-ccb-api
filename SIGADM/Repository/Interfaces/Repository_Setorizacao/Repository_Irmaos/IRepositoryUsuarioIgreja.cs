﻿using SIGADM.Model.Patrimonio.Setorizacao.Irmaos;
using System.Collections.Generic;
using SIGADM.Repository.Generic;

namespace SIGADM.Repository.Interfaces.Repository_Irmaos
{
    public interface IRepositoryUsuarioIgreja : IRepository<UsuarioIgreja>
    {
       
        UsuarioIgreja ProcurarIdUsuario(int idUsuario);
        
    }
}
