﻿using SIGADM.Model.Patrimonio.Setorizacao.Irmaos;
using System.Collections.Generic;
using SIGADM.Repository.Generic;

namespace SIGADM.Repository.Interfaces.Repository_Irmaos
{
    public interface IRepositoryUsuario : IRepository<Usuario>
    {
       
        Usuario BuscarIrmaoCPF(string cpf);
        Usuario BuscarId(long id);
        
    }
}
