﻿using System.Collections.Generic;
using SIGADM.Model.Patrimonio.Setorizacao.Igrejas;
using SIGADM.Repository.Generic;

namespace SIGADM.Repository.Repository_Setorizacao.Repository_Igrejas {
    public interface IRepositoryIgreja : IRepository<Igreja> {


    }
}