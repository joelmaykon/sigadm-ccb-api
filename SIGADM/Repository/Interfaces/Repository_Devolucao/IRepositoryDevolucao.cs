﻿using SIGADM.Model.Patrimonio.Devolucao_data;
using System.Collections.Generic;
using SIGADM.Repository.Generic;

namespace SIGADM.Repository.Interfaces
{
    public interface IRepositoryDevolucao : IRepository<DevolucaoInstrumento>
    {
       
        List<DevolucaoInstrumento> FindDevolucao(string tombamento);
        
    }
}
