﻿using SIGADM.Model.Patrimonio.Emprestimo_data;
using System.Collections.Generic;
using SIGADM.Repository.Generic;

namespace SIGADM.Repository.Interfaces
{
    public interface IRepositoryEmprestimo : IRepository<EmprestimoInstrumento>
    {
       
        List<EmprestimoInstrumento> FindEmprestimo(string tombamento);
        
    }
}
