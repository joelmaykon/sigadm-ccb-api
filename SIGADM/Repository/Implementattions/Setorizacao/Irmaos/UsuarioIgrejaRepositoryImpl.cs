﻿using SIGADM.Model;
using SIGADM.Model.Context;
using SIGADM.Model.Patrimonio.Setorizacao.Irmaos;
using SIGADM.Repository.Generic;
using SIGADM.Repository.Interfaces.Repository_Irmaos;
using System.Collections.Generic;
using System.Linq;

namespace SIGADM.Repository.Implementattions
{
    public class UsuarioIgrejaRepositoryImpl : GenericRepository<UsuarioIgreja>,IRepositoryUsuarioIgreja 
    {

        public UsuarioIgrejaRepositoryImpl(MySQLContext context) : base(context){}    
       public UsuarioIgreja ProcurarIdUsuario(int idUsuario)
        {
            
           return _context.UsuariosIgrejas.SingleOrDefault(d => d.IdUsuario.Equals(idUsuario));

            
        }   
    }
}
