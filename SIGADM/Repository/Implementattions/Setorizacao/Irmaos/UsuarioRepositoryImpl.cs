﻿using SIGADM.Model;
using SIGADM.Model.Context;
using SIGADM.Model.Patrimonio.Setorizacao.Irmaos;
using SIGADM.Repository.Generic;
using SIGADM.Repository.Interfaces.Repository_Irmaos;
using System.Collections.Generic;
using System.Linq;

namespace SIGADM.Repository.Implementattions
{
    public class UsuarioRepositoryImpl : GenericRepository<Usuario>,IRepositoryUsuario 
    {

        public UsuarioRepositoryImpl(MySQLContext context) : base(context){} 
        public Usuario BuscarIrmaoCPF(string cpf)
        {
            return _context.Usuarios.FirstOrDefault(p => p.CPF.Equals(cpf));
        }   
        public Usuario BuscarId(long id)
        {
            return _context.Usuarios.FirstOrDefault(p => p.Id.Equals(id));
        } 
         
    }
}
