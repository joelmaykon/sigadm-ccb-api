﻿using System;
using System.Collections.Generic;
using System.Linq;
using SIGADM.Model;
using SIGADM.Model.Context;
using SIGADM.Model.Patrimonio.Setorizacao.Igrejas;
using SIGADM.Repository.Generic;
using SIGADM.Repository.Repository_Setorizacao.Repository_Igrejas;

namespace SIGADM.Repository.Implementattions.Setorizacao.Igrejas {
    public class IgrejaRepositoryImpl : GenericRepository<Igreja>, IRepositoryIgreja {

        public IgrejaRepositoryImpl (MySQLContext context) : base (context) { }

    }
}