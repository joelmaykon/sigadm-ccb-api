﻿using System;
using System.Collections.Generic;
using System.Linq;
using SIGADM.Model;
using SIGADM.Model.Context;
using SIGADM.Model.Patrimonio.Aquisicao_data;
using SIGADM.Model.Patrimonio.Instrumento_data;
using SIGADM.Repository.Generic;
using SIGADM.Repository.Interfaces.Repository_Instrumento;

namespace SIGADM.Repository.Implementattions {
    public class TombamentoRepositoryImpl : GenericRepository<TombamentoInstrumento>, IRepositoryTombamento {

        public TombamentoRepositoryImpl (MySQLContext context) : base (context) { }
        public TombamentoInstrumento SelecionarTombamento (string tombamento) {
            if (string.IsNullOrEmpty (tombamento)) return null;

            
            var tombo_verificado =  _context.Tombamentos.SingleOrDefault(p => p.Numero.Equals(tombamento));
            var result = _context.Tombamentos.SingleOrDefault (b => b.Numero == tombo_verificado.Numero);
            if (result != null) {
                try {
                    
                     tombo_verificado.Status = "2";
                    _context.Entry (result).CurrentValues.SetValues (tombo_verificado);
                    _context.SaveChanges ();
                } catch (Exception ex) {
                    throw ex;
                }
            }
            return result;
        }
        public TombamentoInstrumento FindByTombamento(string tombamento)
        {
            return _context.Tombamentos.SingleOrDefault(p => p.Numero.Equals(tombamento));
        }
       

    }
}