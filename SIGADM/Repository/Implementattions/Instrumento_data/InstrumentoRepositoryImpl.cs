using SIGADM.Model;
using SIGADM.Model.Context;
using SIGADM.Model.Patrimonio.Instrumento_data;
using SIGADM.Repository.Generic;
using SIGADM.Repository.Interfaces;
using SIGADM.Repository.Interfaces.Repository_Instrumento;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SIGADM.Repository.Implementattions
{
    public class InstrumentoRepositoryImpl:GenericRepository<Instrumento>, IRepositoryInstrumento 
    {
        public InstrumentoRepositoryImpl(MySQLContext context) : base(context){}
        public List<Instrumento> FindInstrumentosNome(string nome)
        {
            if(!string.IsNullOrEmpty(nome)){
                             return _context.Instrumentos
                             .Where(d => d.Nome.Equals(nome))
                             .Where(d => d.Status.Equals("1"))
                             .ToList();

            }else{
                List<Instrumento> lista_vazia = new List<Instrumento>();
                return lista_vazia;
            }
        }       
    }
}
