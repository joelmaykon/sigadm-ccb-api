﻿using SIGADM.Model;
using SIGADM.Model.Context;
using SIGADM.Model.Patrimonio.Datas;
using SIGADM.Repository.Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using SIGADM.Repository.Interfaces;


namespace SIGADM.Repository.Implementattions
{
    public class AnoInstrumentoRepositoryImpl : GenericRepository<Ano>,IRepositoryAnoInstrumento 
    {

        public AnoInstrumentoRepositoryImpl(MySQLContext context) : base(context){}

        

         public void DeleteAno(string ano)
        {
            var result = _context.Anos.SingleOrDefault(i => i.NumeroAno.Equals(ano));
            try
            {
                if (result != null)
                {
                    _context.Anos.Remove(result);
                    _context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }    
    }
}
