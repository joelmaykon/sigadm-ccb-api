﻿using SIGADM.Model;
using SIGADM.Model.Context;
using SIGADM.Model.Patrimonio.Instrumento_data;
using SIGADM.Repository.Generic;
using SIGADM.Repository.Interfaces.Repository_Instrumento;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SIGADM.Repository.Implementattions
{
    public class MarcaInstrumentoRepositoryImpl : GenericRepository<MarcaInstrumento>,IRepositoryMarcaInstrumento 
    {

        public MarcaInstrumentoRepositoryImpl(MySQLContext context) : base(context){}

        

         public void DeleteMarca(string marca)
        {
            var result = _context.Marcas.SingleOrDefault(i => i.Marca.Equals(marca));
            try
            {
                if (result != null)
                {
                    _context.Marcas.Remove(result);
                    _context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }    
    }
}
