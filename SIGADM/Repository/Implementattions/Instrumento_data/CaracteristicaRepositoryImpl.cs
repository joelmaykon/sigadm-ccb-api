﻿using SIGADM.Model;
using SIGADM.Model.Context;
using SIGADM.Model.Patrimonio.Instrumento_data;
using SIGADM.Repository.Generic;
using SIGADM.Repository.Interfaces;
using SIGADM.Repository.Interfaces.Repository_Instrumento;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SIGADM.Repository.Implementattions
{
    public class CaracteristicaRepositoryImpl : GenericRepository<CaracteristicaInstrumento>,IRepositoryCaracteristicaInstrumento 
    {

        public CaracteristicaRepositoryImpl(MySQLContext context) : base(context){}

        

        public List<CaracteristicaInstrumento> FindCaracteristicaNaipe(string naipe)
        {
            if(!string.IsNullOrEmpty(naipe)){
                             return _context.Caracteristicas.Where(d => d.Naipe.Equals(naipe)).ToList();

            }else{
                return _context.Caracteristicas.ToList();
            }
        }
        public void DeleteCaracteristica(string caracteristica)
        {
            var result = _context.Caracteristicas.SingleOrDefault(i => i.Caracteristica.Equals(caracteristica));
            try
            {
                if (result != null)
                {
                    _context.Caracteristicas.Remove(result);
                    _context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }           
    }
}
