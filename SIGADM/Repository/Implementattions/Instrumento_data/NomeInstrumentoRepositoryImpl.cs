﻿using SIGADM.Model;
using SIGADM.Model.Context;
using SIGADM.Model.Patrimonio.Instrumento_data;
using SIGADM.Repository.Generic;
using SIGADM.Repository.Interfaces.Repository_Instrumento;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SIGADM.Repository.Implementattions
{
    public class NomeInstrumentoRepositoryImpl : GenericRepository<NomeInstrumento>,IRepositoryNomeInstrumento 
    {

        public NomeInstrumentoRepositoryImpl(MySQLContext context) : base(context){}

        

         public void DeleteNome(string nome)
        {
            var result = _context.NomesInstrumentos.SingleOrDefault(i => i.Nome.Equals(nome));
            try
            {
                if (result != null)
                {
                    _context.NomesInstrumentos.Remove(result);
                    _context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }    
    }
}
