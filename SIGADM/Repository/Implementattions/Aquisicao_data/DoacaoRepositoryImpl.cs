﻿using SIGADM.Model;
using SIGADM.Model.Context;
using SIGADM.Model.Patrimonio.Aquisicao_data;
using SIGADM.Repository.Generic;
using SIGADM.Repository.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace SIGADM.Repository.Implementattions
{
    public class DoacaoRepositoryImpl : GenericRepository<DoacaoInstrumento>,IRepositoryDoacao 
    {

        public DoacaoRepositoryImpl(MySQLContext context) : base(context){}    
        public List<DoacaoInstrumento> FindDoacao(string tombamento)
        {
            if(!string.IsNullOrEmpty(tombamento)){
                             return _context.DoacaoInstrumentos.Where(d => d.Tombamento.Equals(tombamento)).ToList();

            }else{
                return _context.DoacaoInstrumentos.ToList();
            }
        }       
    }
}
