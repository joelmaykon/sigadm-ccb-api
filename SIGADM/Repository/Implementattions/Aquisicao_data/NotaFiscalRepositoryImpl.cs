﻿using SIGADM.Model;
using SIGADM.Model.Context;
using SIGADM.Model.Patrimonio.Aquisicao_data;
using SIGADM.Repository.Generic;
using SIGADM.Repository.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace SIGADM.Repository.Implementattions
{
    public class NotaFiscalRepositoryImpl : GenericRepository<NotaFiscalInstrumento>,IRepositoryNotaFiscal 
    {

        public NotaFiscalRepositoryImpl(MySQLContext context) : base(context){}    
       
    }
}
