﻿using SIGADM.Model;
using SIGADM.Model.Context;
using SIGADM.Model.Patrimonio.Aquisicao_data;
using SIGADM.Repository.Generic;
using SIGADM.Repository.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace SIGADM.Repository.Implementattions
{
    public class CompraRepositoryImpl : GenericRepository<CompraInstrumento>,IRepositoryCompra 
    {

        public CompraRepositoryImpl(MySQLContext context) : base(context){}    
        public List<CompraInstrumento> FindCompra(string tombamento)
        {
            if(!string.IsNullOrEmpty(tombamento)){
                             return _context.CompraInstrumentos.Where(d => d.Tombamento.Equals(tombamento)).ToList();

            }else{
                return _context.CompraInstrumentos.ToList();
            }
        }       
    }
}
