﻿using SIGADM.Model;
using SIGADM.Model.Context;
using SIGADM.Model.Patrimonio.Aquisicao_data;
using SIGADM.Repository.Generic;
using System.Collections.Generic;
using System.Linq;
using SIGADM.Repository.Interfaces;


namespace SIGADM.Repository.Implementattions
{
    public class OrigemRepositoryImpl : GenericRepository<OrigemInstrumento>,IRepositoryOrigem 
    {

        public OrigemRepositoryImpl(MySQLContext context) : base(context){}    
       
    }
}
