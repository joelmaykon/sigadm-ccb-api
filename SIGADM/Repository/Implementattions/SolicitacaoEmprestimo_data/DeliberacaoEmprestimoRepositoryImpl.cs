﻿using System;
using System.Collections.Generic;
using System.Linq;
using SIGADM.Model;
using SIGADM.Model.Context;
using SIGADM.Model.Patrimonio.SolicitacaoEmprestimo_data;
using SIGADM.Repository.Generic;
using SIGADM.Repository.Repository_SolicitacaoEmprestimo;

namespace SIGADM.Repository.Implementattions.SolicitacaoEmprestimo_data{
    public class DeliberacaoEmprestimoRepositoryImpl : GenericRepository<DeliberacaoEmprestimo>, IRepositoryDeliberacaoEmprestimo {

        public DeliberacaoEmprestimoRepositoryImpl (MySQLContext context) : base (context) { }

    }
}