﻿using SIGADM.Model;
using SIGADM.Model.Context;
using SIGADM.Model.Patrimonio.Emprestimo_data;
using SIGADM.Repository.Generic;
using System.Collections.Generic;
using SIGADM.Repository.Interfaces;
using System.Linq;

namespace SIGADM.Repository.Implementattions
{
    public class EmprestimoRepositoryImpl : GenericRepository<EmprestimoInstrumento>,IRepositoryEmprestimo 
    {

        public EmprestimoRepositoryImpl(MySQLContext context) : base(context){}    
        public List<EmprestimoInstrumento> FindEmprestimo(string tombamento)
        {
            if(!string.IsNullOrEmpty(tombamento)){
                             return _context.EmprestimoInstrumentos.Where(d => d.Tombamento.Equals(tombamento)).ToList();

            }else{
                return _context.EmprestimoInstrumentos.ToList();
            }
        }       
    }
}
