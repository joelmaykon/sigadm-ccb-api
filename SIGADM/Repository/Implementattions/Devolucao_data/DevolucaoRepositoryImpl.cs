﻿using SIGADM.Model;
using SIGADM.Model.Context;
using SIGADM.Model.Patrimonio.Devolucao_data;
using SIGADM.Repository.Generic;
using System.Collections.Generic;
using System.Linq;
using SIGADM.Repository.Interfaces;


namespace SIGADM.Repository.Implementattions
{
    public class DevolucaoRepositoryImpl : GenericRepository<DevolucaoInstrumento>,IRepositoryDevolucao 
    {

        public DevolucaoRepositoryImpl(MySQLContext context) : base(context){}

        

        public List<DevolucaoInstrumento> FindDevolucao(string tombamento)
        {
            if(!string.IsNullOrEmpty(tombamento)){
                             return _context.DevolucaoInstrumentos.Where(d => d.Tombamento.Equals(tombamento)).ToList();

            }else{
                return _context.DevolucaoInstrumentos.ToList();
            }
        }       
    }
}
