using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SIGADM.Data.VO.Patrimonio.Setorizacao.Irmaos;
using Tapioca.HATEOAS;

namespace SIGADM.HyperMedia.Patrimonio.Setorizacao.Irmaos
{
    public class UsuarioIgrejaEnricher : ObjectContentResponseEnricher<UsuarioIgrejaVO>
    {
        protected override Task EnrichModel(UsuarioIgrejaVO content, IUrlHelper urlHelper)
        {
            var path = "api/UsuarioIgreja/v1";
            var url = new { controller = path, id = content.Id };

            content.Links.Add(new HyperMediaLink()
            {
                Action = HttpActionVerb.GET,
                Href = urlHelper.Link("DefaultApi", url),
                Rel = RelationType.self,
                Type = ResponseTypeFormat.DefaultGet
            });
            content.Links.Add(new HyperMediaLink()
            {
                Action = HttpActionVerb.POST,
                Href = urlHelper.Link("DefaultApi", url),
                Rel = RelationType.self,
                Type = ResponseTypeFormat.DefaultPost
            });
            content.Links.Add(new HyperMediaLink()
            {
                Action = HttpActionVerb.PUT,
                Href = urlHelper.Link("DefaultApi", url),
                Rel = RelationType.self,
                Type = ResponseTypeFormat.DefaultPost
            });
            content.Links.Add(new HyperMediaLink()
            {
                Action = HttpActionVerb.DELETE,
                Href = urlHelper.Link("DefaultApi", url),
                Rel = RelationType.self,
                Type = "int"
            });   
            return null;
        }
    }
}