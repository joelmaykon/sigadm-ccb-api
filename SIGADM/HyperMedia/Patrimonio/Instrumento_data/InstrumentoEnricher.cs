using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SIGADM.Data.VO.Patrimonio.Instrumento_data;
using Tapioca.HATEOAS;

namespace SIGADM.HyperMedia.Patrimonio.Patrimonio.Instrumento_data
{
    public class InstrumentoEnricher : ObjectContentResponseEnricher<InstrumentoVO>
    {
        protected override Task EnrichModel(InstrumentoVO content, IUrlHelper urlHelper)
        {
            var path = "api/instrumento/v1";
            var url = new { controller = path, id = content.Id };

            content.Links.Add(new HyperMediaLink()
            {
                Action = HttpActionVerb.GET,
                Href = urlHelper.Link("DefaultApi", url),
                Rel = RelationType.self,
                Type = ResponseTypeFormat.DefaultGet
            });
            content.Links.Add(new HyperMediaLink()
            {
                Action = HttpActionVerb.POST,
                Href = urlHelper.Link("DefaultApi", url),
                Rel = RelationType.self,
                Type = ResponseTypeFormat.DefaultPost
            });
            content.Links.Add(new HyperMediaLink()
            {
                Action = HttpActionVerb.PUT,
                Href = urlHelper.Link("DefaultApi", url),
                Rel = RelationType.self,
                Type = ResponseTypeFormat.DefaultPost
            });
            content.Links.Add(new HyperMediaLink()
            {
                Action = HttpActionVerb.DELETE,
                Href = urlHelper.Link("DefaultApi", url),
                Rel = RelationType.self,
                Type = "int"
            });   
            return null;
        }
    }
}