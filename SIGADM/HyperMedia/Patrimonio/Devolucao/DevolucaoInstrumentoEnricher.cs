using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SIGADM.Data.VO.Patrimonio.Devolucao_data;
using Tapioca.HATEOAS;

namespace SIGADM.HyperMedia.Patrimonio.Patrimonio.Devolucao_data
{
    public class DevolucaoInstrumentoEnricher : ObjectContentResponseEnricher<DevolucaoInstrumentoVO>
    {
        protected override Task EnrichModel(DevolucaoInstrumentoVO content, IUrlHelper urlHelper)
        {
            var path = "api/devolucaoinstrumento/v1";
            var url = new { controller = path, id = content.Id };

            content.Links.Add(new HyperMediaLink()
            {
                Action = HttpActionVerb.GET,
                Href = urlHelper.Link("DefaultApi", url),
                Rel = RelationType.self,
                Type = ResponseTypeFormat.DefaultGet
            });
            content.Links.Add(new HyperMediaLink()
            {
                Action = HttpActionVerb.POST,
                Href = urlHelper.Link("DefaultApi", url),
                Rel = RelationType.self,
                Type = ResponseTypeFormat.DefaultPost
            });
            content.Links.Add(new HyperMediaLink()
            {
                Action = HttpActionVerb.PUT,
                Href = urlHelper.Link("DefaultApi", url),
                Rel = RelationType.self,
                Type = ResponseTypeFormat.DefaultPost
            });
            content.Links.Add(new HyperMediaLink()
            {
                Action = HttpActionVerb.DELETE,
                Href = urlHelper.Link("DefaultApi", url),
                Rel = RelationType.self,
                Type = "int"
            });   
            return null;
        }
    }
}