CREATE
	TRIGGER devolucao_instrumento_update BEFORE INSERT 
	ON DevolucaoInstrumento 
	FOR EACH ROW 
    BEGIN    
	    UPDATE Instrumento SET  Instrumento.Status = '1' Where Id =  New.IdInstrumento;
		UPDATE EmprestimoInstrumento
		 SET  EmprestimoInstrumento.Status = '3',
		  EmprestimoInstrumento.DevolucaoDate = New.DevolucaoDate
		 Where Id =  New.IdEmprestimo;
		
    END