CREATE TABLE `DeliberacaoEmprestimo` (
	`Id` INT(10) NOT NULL AUTO_INCREMENT,
	`Tipo` VARCHAR(50)  NOT NULL,
	`DataCadastro` datetime(6) NOT NULL,
	`LaunchDate` datetime(6) NOT NULL,
	PRIMARY KEY (`Id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB;