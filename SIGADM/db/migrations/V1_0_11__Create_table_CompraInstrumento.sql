CREATE TABLE `CompraInstrumento` (
	`Id` INT(10) NOT NULL AUTO_INCREMENT,
	`Valor` VARCHAR(50)  NOT NULL,
	`Tombamento` VARCHAR(50) NOT NULL,
	`NotaFiscal` VARCHAR(50)  NOT NULL,
	`Empresa` VARCHAR(50)  NOT NULL,
	`DataCompra` datetime(6) NOT NULL,
	`LaunchDate` datetime(6) NOT NULL,
	PRIMARY KEY (`Id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB;