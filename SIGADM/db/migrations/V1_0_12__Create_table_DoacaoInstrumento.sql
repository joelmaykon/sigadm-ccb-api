CREATE TABLE `DoacaoInstrumento` (
	`Id` INT(10) NOT NULL AUTO_INCREMENT,
	`Origem` VARCHAR(50)  NOT NULL,
	`Tombamento` VARCHAR(50) NOT NULL,
	`Observacao` VARCHAR(50)  NOT NULL,
	`DataDoacao` datetime(6) NOT NULL,
	`LaunchDate` datetime(6) NOT NULL,
	PRIMARY KEY (`Id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB;