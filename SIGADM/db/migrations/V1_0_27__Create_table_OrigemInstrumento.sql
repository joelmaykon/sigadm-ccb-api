CREATE TABLE `OrigemInstrumento` (
	`Id` INT(10) NOT NULL AUTO_INCREMENT,
	`Doador` VARCHAR(50)  NOT NULL,
	`Estado`  VARCHAR(50)  NOT NULL,
	`Cidade` VARCHAR(50) NOT NULL,
	`LaunchDate` datetime(6) NOT NULL,
	PRIMARY KEY (`Id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB;