CREATE TABLE `TombamentoInstrumento` (
	`Id` INT(10) NOT NULL AUTO_INCREMENT,
	`Numero` VARCHAR(50) UNIQUE NOT NULL,
	`Status`  VARCHAR(500) NULL DEFAULT NULL,
	`LaunchDate` datetime(6) NOT NULL,
	PRIMARY KEY (`Id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB;