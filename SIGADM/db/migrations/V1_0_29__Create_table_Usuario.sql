CREATE TABLE `Usuario` (
	`Id` INT(10) NOT NULL AUTO_INCREMENT,
	`Nome` VARCHAR(50)  NOT NULL,
	`Endereco`  VARCHAR(50)  NOT NULL,
	`Cidade`  VARCHAR(50)  NOT NULL,
	`Telefone`  VARCHAR(50)  NOT NULL,	
	`Cargo`  VARCHAR(50)  NOT NULL,
	`LaunchDate` datetime(6) NOT NULL,
	PRIMARY KEY (`Id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB;