ALTER TABLE `Usuario` ADD `Cep` VARCHAR(50) NOT NULL DEFAULT '';
ALTER TABLE `Usuario` ADD `Bairro` VARCHAR(50) NOT NULL DEFAULT '';
ALTER TABLE `Usuario` ADD `Rua` VARCHAR(50) NOT NULL DEFAULT '';
ALTER TABLE `Usuario` ADD `Numero` VARCHAR(50) NOT NULL DEFAULT '0';
ALTER TABLE `Usuario` ADD `Complemento` VARCHAR(50) NOT NULL DEFAULT '';
ALTER TABLE `Usuario` ADD `DDD` VARCHAR(50) NOT NULL DEFAULT '';