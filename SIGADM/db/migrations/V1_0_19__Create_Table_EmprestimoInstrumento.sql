﻿CREATE TABLE  `EmprestimoInstrumento`
(
  `Id` INT(10) NOT NULL AUTO_INCREMENT,
   `IdInstrumento` INT(10)  NULL DEFAULT NULL,
  `Nome`  VARCHAR
(500) NOT NULL,
 `Congregacao`  VARCHAR
(500) NOT NULL,
`Tombamento`  VARCHAR
(500) NOT NULL,
`Status`  VARCHAR
(500) NOT NULL,
  `Encarregado`  VARCHAR
(500) NOT NULL,
`EncarregadoRegional`  VARCHAR
(500) NOT NULL,
`DevolucaoDate` datetime
(6) NOT NULL,  
`EmprestimoDate` datetime
(6) NOT NULL, 
  `LaunchDate` datetime
(6) NOT NULL,
  PRIMARY KEY (`Id`)
)COLLATE='latin1_swedish_ci'
ENGINE=InnoDB;
