CREATE TABLE `UsuarioIgreja` (
	`Id` INT(10) NOT NULL AUTO_INCREMENT,
	`IdIgreja` INT(10)  NOT NULL,
	`IdUsuario` INT(10)  NOT NULL,		
	`LaunchDate` datetime(6) NOT NULL,
	PRIMARY KEY (`Id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB;