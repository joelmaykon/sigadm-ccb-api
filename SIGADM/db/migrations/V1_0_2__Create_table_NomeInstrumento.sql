CREATE TABLE `NomeInstrumento` (
	`Id` INT(10) NOT NULL AUTO_INCREMENT,
	`Nome` VARCHAR(50) UNIQUE NOT NULL,
	`Naipe` VARCHAR(50) NOT NULL,
	`LaunchDate` datetime(6) NOT NULL,
	PRIMARY KEY (`Id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB;