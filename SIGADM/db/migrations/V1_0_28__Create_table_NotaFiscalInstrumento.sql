CREATE TABLE `NotaFiscalInstrumento` (
	`Id` INT(10) NOT NULL AUTO_INCREMENT,
	`Tombamento` VARCHAR(50)  NOT NULL,
	`Caminho`  VARCHAR(50)  NOT NULL,
	`LaunchDate` datetime(6) NOT NULL,
	PRIMARY KEY (`Id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB;