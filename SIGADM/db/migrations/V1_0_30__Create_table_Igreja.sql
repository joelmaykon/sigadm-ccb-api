CREATE TABLE `Igreja` (
	`Id` INT(10) NOT NULL AUTO_INCREMENT,
	`Codigo` VARCHAR(50)  NOT NULL,
	`Cidade` VARCHAR(50)  NOT NULL,	
	`Bairro` VARCHAR(50)  NOT NULL,
	`Endereco` VARCHAR(50)  NOT NULL,
	`CEP` VARCHAR(50)  NOT NULL,
	`EncRegional` VARCHAR(50)  NOT NULL,
	`EncLocal` VARCHAR(50)  NOT NULL,
	`LaunchDate` datetime(6) NOT NULL,
	PRIMARY KEY (`Id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB;