CREATE TABLE `ComponenteInstrumento` (
	`Id` INT(10) NOT NULL AUTO_INCREMENT,
	`Componente` VARCHAR(50) UNIQUE NOT NULL,
	`LaunchDate` datetime(6) NOT NULL,
	PRIMARY KEY (`Id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB;