CREATE TABLE `CaracteristicaInstrumento` (
	`Id` INT(10) NOT NULL AUTO_INCREMENT,
	`Caracteristica` VARCHAR(50)  NOT NULL,
	`Naipe` VARCHAR(50) NOT NULL,
	`LaunchDate` datetime(6) NOT NULL,
	PRIMARY KEY (`Id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB;