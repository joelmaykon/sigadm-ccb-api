﻿CREATE TABLE  `DevolucaoInstrumento`
(
  `Id` INT(10) NOT NULL AUTO_INCREMENT,
   `IdInstrumento` INT(10)  NULL DEFAULT NULL,
   `IdEmprestimo` INT(10)  NULL DEFAULT NULL,  
`Tombamento`  VARCHAR
(500) NOT NULL,
`Status`  VARCHAR
(500) NOT NULL, 
`Observacao`  VARCHAR
(500) NOT NULL, 
`DevolucaoDate` datetime
(6) NOT NULL,  
  `LaunchDate` datetime
(6) NOT NULL,
  PRIMARY KEY (`Id`)
)COLLATE='latin1_swedish_ci'
ENGINE=InnoDB;
